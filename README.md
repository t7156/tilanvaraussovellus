# Tilanvaraussovellus

## Description

This project is built and maintained by a group of students as part of the training program Buutti Training Academy. The goal of the project is to create a platform for reserving and creating reservable spaces for the use of both private and public groups and persons. The spaces available on the platform can be used for different purposes ranging from hobbies such as band-training to working and holding meetings. The platform should enable users to find spaces at or near specified locations, reserve them for use at specified times and allow creation of spaces for reservation by users of the platform.

### Technologies

Application backend functionality is built with Node.js and Express. Database will use PostgresSQL. FrontEnd functionality will be built with React (v17) and those libraries deemed necessary to achieve desired style and content such as MaterialUI.

## Full release major features

- Allow users to create an account. 
- Allow users to reserve available spaces at determined times.
- Allow users to provide spaces for reservation at desired times and prices.
- Allow users to search for spaces with given location, price, size and/or category.
- Allow users to review spaces.
- Allow users to live-chat. 
- Allow users to view and select locations on a map.  

### Implemented major features as of 29.03.2022

- Users can create an account.
- Users can reserve spaces with the minimum reservable time being 1 hour.
- Users can create spaces available for reservation with given price, location and category.
- Users can search for spaces with given location, price, size and/or category.
- Users can review spaces. 

### Future planned major features

- Allow users to live-chat.
- Allow users to view and select locations on a map. 

#### MVP Functionalities

The users can
- {+ (Completed) +} create an account
- {+ (Completed) +} create new spaces for reservation
- {+ (Completed) +} view all spaces available in a list
- {+ (Completed) +} search for spaces based on name, location and category
- {+ (Completed) +} view details of each space and times when the space is available
- {+ (Completed) +} make reservations
- {+ (Completed) +} remove spaces which they have created
- {+ (Completed) +} cancel their own reservations

#### Advanced Functionalities

The users can
- edit their reservations
- {+ (Completed) +} edit details of their own spaces
- {+ (Completed) +} change user information or delete their account
- view spaces and search results on a map
- {+ (Completed) +} view available times for each space in a calendar
- {+ (Completed) +} give ratings to spaces
- mark spaces as their favorite
- {+ (Completed) +} add pictures of their own spaces


### Getting started with the application

Currently the application runs in a local environment and has no external dedicated server.

In order to start the application the following software dependencies should be present
on the computer used to run the application.

- Node.js
- NPM package manager
- PostgreSQL for database management

Once these dependencies are installed and the project repository is cloned from GitLab, 
the application can be started by going through the following steps:

1. Install necessary packages by running the CLI command `npm install` in 
the `/server` and `/client` directories.
2. Once packages have been installed, the server should be started by running 
the CLI command `npm start` in the server directory. 
3. After starting the server, the client can be started by running the CLI
command `npm start` in the client directory. 
4. The application will now start and open in a new window/tab in the user's 
default web browser. 

In addition, setting up the database can be done by executing the following steps:

1. Generate secrets for backend by running the CLI command `node generateKeyPair.js` 
in the `/server/src` folder.
2. Generate test data for backend by running the CLI command `node createInitialData.js`
in the `/server/src/initial-data` folder. 

### Endpoint wiki

https://gitlab.com/t7156/tilanvaraussovellus/-/wikis/Endpoint-documentation

## UI

![etusivu1.PNG](./images/etusivu1.PNG)
![etusivu2.PNG](./images/etusivu2.PNG)
Landing page

![tilat-sivu.PNG](./images/tilat-sivu.PNG)
Search results

![haku.PNG](./images/haku.PNG)
Search bar

![tilasivu.PNG](./images/tilasivu.PNG)
Space view

![kalenteri.PNG](./images/kalenteri.PNG)
Reservation calendar

![arvostelut.PNG](./images/arvostelut.PNG)
Reviews

![profiilisivu.PNG](./images/profiilisivu.PNG)
Profile page
