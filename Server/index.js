import express from 'express';
import cors from 'cors';
import authRouter from './src/routes/authRoutes.js';
import bodyParser from 'body-parser';
import spaceRouter from './src/routes/spaceRoutes.js';
import reservationRouter from './src/routes/reservationRoutes.js';
import accountRouter from './src/routes/accountRoutes.js';
import categoryRouter from './src/routes/categoryRoutes.js';
import fileUpload from 'express-fileupload';
import spaceCategoryRouter from './src/routes/spaceCategoryRoutes.js';
import PendingreservationRouter from './src/routes/pendingReservationRoutes.js';
import reviewRouter from './src/routes/reviewRoutes.js';
const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());
app.use(
	fileUpload({
		createParentPath: true,
	  })
);

app.use('/auth', authRouter);
app.use('/space', spaceRouter);
app.use('/reservation', reservationRouter);
app.use('/account', accountRouter);
app.use('/category', categoryRouter);
app.use('/space_category', spaceCategoryRouter);
app.use('/pending', PendingreservationRouter);
app.use('/review', reviewRouter);

app.listen(3001, () => {
	console.log('Server is listening on port 3001');
});
