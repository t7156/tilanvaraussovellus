import crypto from 'crypto';
import jsonwebtoken from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import { readFileSync } from 'fs';
import { join } from 'path';
import {
	createInternalServerErrorResponse,
	createUnauthorizedResponse
} from '../lib/responses.js';
import { getAccountById } from '../queries/accountQueries.js';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const pathToPrivKey = path.join(__dirname, '..', 'id_rsa_priv.pem');
const PRIV_KEY = fs.readFileSync(pathToPrivKey, 'utf8');
const pathToPubKey = join(__dirname, '..', 'id_rsa_pub.pem');
const PUB_KEY = readFileSync(pathToPubKey, 'utf8');

const emailConfirmationSecret = crypto.randomBytes(32).toString('base64');

export const getSaltHashFromPassword = (password) => {
	const salt = crypto.randomBytes(32).toString('hex');
	const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');

	return { salt, hash };
};

export const validatePassword = (password, hash, salt) => {
	const hashToVerify = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');

	return hash === hashToVerify;
};

export const issueJWT = (account) => {
	// expiresIn = 6 hours
	const expiresIn = 21600000;
	const payload = {
		sub: account.id,
		iat: Date.now(),
	};
	const signedToken = jsonwebtoken.sign(payload, PRIV_KEY, { expiresIn, algorithm: 'RS256' });

	return {
		token: `Bearer ${signedToken}`,
		expires: expiresIn,
	};
};

export const authorizeAdmin = async (req, res, next) => {
	const id = req.jwt_payload.sub;
	const account = await getAccountById(id);
	account
		? account.role === 'admin' ? next() : createUnauthorizedResponse(res, 'Unauthorized')
		: createInternalServerErrorResponse(res, 'Something went wrong');
};

export const authenticateJWT = async (req, res, next) => {
	if (!req.headers.authorization) createUnauthorizedResponse(res, 'Invalid token');
	else {
		const token = req.headers.authorization;
		const tokenParts = token.split(' ');
		if (tokenParts[0] === 'Bearer' && tokenParts[1].match(/\S+\.\S+\.\S+/)) {
			try {
				jsonwebtoken.verify(
					tokenParts[1],
					PUB_KEY,
					{ algorithms: ['RS256'] },
					(error, decoded) => {
						if (error || decoded.exp < Date.now())
							return createUnauthorizedResponse(res, 'Invalid token');
						req.jwt_payload = decoded;
						next();
					}
				);
			} catch (e) {
				createUnauthorizedResponse(res, 'Invalid token');
			}
		} else {
			createUnauthorizedResponse(res, 'Invalid token');
		}
	}
};

export const issueEmailConfirmationCode = (id) => {
	// expiresIn = 6 hours
	const expiresIn = 21600000;
	const payload = {
		sub: id,
		iat: Date.now(),
	};
	const emailConfirmationCode = jsonwebtoken.sign(payload, emailConfirmationSecret, { expiresIn, algorithm: 'HS256' });
	return emailConfirmationCode;
};

export default {
	getSaltHashFromPassword,
	validatePassword,
	issueJWT,
	authenticateJWT,
	authorizeAdmin,
};
