import {
	createNotFoundResponse,
	createStatusOKResponse
} from '../lib/responses.js';
import {
	addSpaceCategoryToDb,
	getSpaceCategories
} from '../queries/spaceCategoryQueries.js';

export const getSpaceCategoriesController = async (req, res) => {
	const spaceCategories = await getSpaceCategories();
	spaceCategories
		? createStatusOKResponse(res, 'Space categories:', spaceCategories)
		: createNotFoundResponse(res, 'Not found');
};

export const addSpaceCategoriesToDbController = (categories, id) => {
	return (categories.every(async (category) => {
		const isAdded = await addSpaceCategoryToDb(id, category);
		return (!isAdded.length)
			? false
			: true;
	})) ? true : false;
};
