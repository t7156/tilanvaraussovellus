import {
	getSaltHashFromPassword,
	validatePassword
} from '../auth/authUtils.js';
import {
	createBadRequestResponse,
	createConflictResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse,
	createStatusOKResponse,
	createUnauthorizedResponse,
} from '../lib/responses.js';
import {
	validateEmailFormat,
	validatePasswordFormat,
	validateUsernameFormat,
	validateIdFormat,
} from '../lib/validation.js';
import {
	changePassword,
	getAccountById,
	deleteAccount,
	editAccount,
} from '../queries/accountQueries.js';
import {
	checkIsEmailAvailable,
	checkIsUsernameAvailable
} from '../queries/authQueries.js';

export const getAccountController = async (req, res) => {
	const id = req.jwt_payload.sub;
	if (validateIdFormat(id)) {
		const account = await getAccountById(id);
		account
			? createStatusOKResponse(res, 'Account found', {
				id: account.id,
				username: account.username,
				email: account.email,
				role: account.role,
			  })
			: createNotFoundResponse(res, 'Not found');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const changePasswordController = async (req, res) => {
	const id = req.jwt_payload.sub;
	const { oldPassword, newPassword } = req.body;
	const account = await getAccountById(id);

	if (account) {
		const isValidPassword = validatePassword(oldPassword, account.hash, account.salt);
		if (isValidPassword) {
			if (validatePasswordFormat(newPassword)) {
				const { salt, hash } = getSaltHashFromPassword(newPassword);
				(await changePassword(salt, hash, id))
					? createStatusOKResponse(res, 'Password changed')
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else {
				createBadRequestResponse(res, 'Bad request');
			}
		} else {
			createUnauthorizedResponse(res, 'Wrong password');
		}
	} else {
		createNotFoundResponse(res, 'Not found');
	}
};

export const deleteAccountController = async (req, res) => {
	const tokenSubId = req.jwt_payload.sub;
	try {
		if (tokenSubId === Number(req.params.id)) {
			await deleteAccount(tokenSubId);
			createStatusOKResponse(res, 'Account deleted');
		} else {
			createUnauthorizedResponse(res, 'Unauthorized');
		}
	} catch (e) {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const editAccountController = async (req, res) => {
	const tokenSubId = req.jwt_payload.sub;
	const param_id = parseInt(req.params.id);
	const { username, email } = req.body;
	const isUserNameAvailable = await checkIsUsernameAvailable(username);
	const isEmailAvailable = await checkIsEmailAvailable(email);
	const account = await getAccountById(tokenSubId);
	
	if (validateUsernameFormat(username) && validateEmailFormat(email) && validateIdFormat(param_id)) {
		if (param_id === tokenSubId) {
			if (isUserNameAvailable || username === account.username) {
				if (isEmailAvailable || email === account.email) {
					const editedAccount = await editAccount(username, email, tokenSubId);
					editedAccount
						? createStatusOKResponse(res, 'Account edited', editedAccount)
						: createNotFoundResponse(res, 'Not found');
				} else createConflictResponse(res, 'Email taken');
			} else createConflictResponse(res, 'Username taken');
		} else createUnauthorizedResponse(res, 'Unauthorized');
	} else createBadRequestResponse(res, 'Bad request');
};
