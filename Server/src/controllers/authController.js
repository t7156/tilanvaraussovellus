import {
	validateUsernameFormat,
	validatePasswordFormat,
	validateEmailFormat,
	validateIdFormat,
} from '../lib/validation.js';
import {
	checkIsUsernameAvailable,
	checkIsEmailAvailable,
	addAccountToDb,
	getAccountByEmail,
	addEmailConfirmationCodeToDb,
	getAccountByConfirmationCode,
	changeConfirmationStatus,
} from '../queries/authQueries.js';
import {
	validatePassword,
	issueJWT,
	getSaltHashFromPassword,
	issueEmailConfirmationCode
} from '../auth/authUtils.js';
import {
	createBadRequestResponse,
	createConflictResponse,
	createStatusOKResponse,
	createCreatedResponse,
	createUnauthorizedResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse,
} from '../lib/responses.js';
import { getAccountById } from '../queries/accountQueries.js';
import { sendEmail } from '../lib/nodemailer.js';

const testEmail = 'raoul.abernathy0@ethereal.email';

export const createAccountController = async (req, res) => {
	const { username, email, password } = req.body;
	const isUserNameAvailable = await checkIsUsernameAvailable(username);
	const isEmailAvailable = await checkIsEmailAvailable(email);

	if (
		validateUsernameFormat(username) &&
		validatePasswordFormat(password) &&
		validateEmailFormat(email)
	) {
		if (isUserNameAvailable) {
			if (isEmailAvailable) {
				const { salt, hash } = getSaltHashFromPassword(password);
				const account = await addAccountToDb(username, email, salt, hash, 'basic', false);
				const emailConfirmationCode = issueEmailConfirmationCode(account.id);
				const addCode = await addEmailConfirmationCodeToDb(emailConfirmationCode, account.id);
				if (sendEmail(testEmail, 'Email verification', emailConfirmationCode)) {
					(account, emailConfirmationCode, addCode)
						? createCreatedResponse(res, 'Account created. Check your email for confirmation')
						: createInternalServerErrorResponse(res, 'Something went wrong');
				} else {
					createInternalServerErrorResponse(res, 'Email could not be sent');
				}
			} else {
				createConflictResponse(res, 'Email already taken');
			}
		} else {
			createConflictResponse(res, 'Username already taken');
		}
	} else createBadRequestResponse(res, 'Bad request');
};

export const loginController = async (req, res) => {
	const { email, password } = req.body;

	if (validateEmailFormat(email) && validatePasswordFormat(password)) {
		const account = await getAccountByEmail(email);

		if (account) {
			const isValidPassword = validatePassword(password, account.hash, account.salt);
			if (isValidPassword) {
				if(account.confirmed === true) {
					const jwt = issueJWT(account);
					createStatusOKResponse(res, 'Logged in', {
						id: account.id,
						username: account.username,
						email: account.email,
						role: account.role,
						token: jwt.token,
					});
				} else {
					createUnauthorizedResponse(res, 'Pending account. Confirm your email');
				}
			} else {
				createUnauthorizedResponse(res, 'Wrong username or password.');
			}
		} else {
			createUnauthorizedResponse(res, 'Wrong username or password.');
		}
	} else {
		createUnauthorizedResponse(res, 'Wrong username or password.');
	}
};

export const verifyPasswordController = async (req, res) => {
	const { password } = req.body;
	const tokenSubId = req.jwt_payload.sub;

	if (validateIdFormat(Number(tokenSubId)) && validatePasswordFormat(password)) {
		const account = await getAccountById(tokenSubId);
		const isValidPassword = validatePassword(password, account.hash, account.salt);
		isValidPassword
			? createStatusOKResponse(res, 'Correct password', true)
			: createUnauthorizedResponse(res, 'Wrong password', false);
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const verifyAuthenticationController = async (_req, res) => {
	createStatusOKResponse(res, 'Authentication verified');
};

export const emailConfirmationController = async (req, res) => {
	const emailConfirmationCode = req.params.confirmation_code;
	const account = await getAccountByConfirmationCode(emailConfirmationCode);
	if (account) {
		if (account.confirmed === true) {
			createConflictResponse(res, 'Account already confirmed');
		}
		else {
			if (emailConfirmationCode === account.confirmation_code) {
				const confirmed = await changeConfirmationStatus(account.id);
				confirmed
					? createStatusOKResponse(res, 'Email confirmed')
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else createUnauthorizedResponse(res, 'Can\'t verify');
		}
	} else createNotFoundResponse(res, 'Not found. Re-send confirmation email');	
};

export const reSendEmailConfirmationCodeController = async (req, res) => {
	const email = req.body.email;
	if (validateEmailFormat(email)) {
		const account = await getAccountByEmail(email);
		if (account) {
			try {
				const emailConfirmationCode = issueEmailConfirmationCode(account.id);
				const addCode = await addEmailConfirmationCodeToDb(emailConfirmationCode, account.id);
				if (addCode) {
					sendEmail(testEmail, 'Confirmation', emailConfirmationCode);
					createStatusOKResponse(res, 'Email sent');
				} else {
					createInternalServerErrorResponse(res, 'Something went wrong');
				}
			} catch (e) {
				createInternalServerErrorResponse(res, 'Something went wrong');
			}
		} else {
			createNotFoundResponse(res, 'Not found');
		}
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};