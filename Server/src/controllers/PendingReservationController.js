import { authorizeAdmin } from '../auth/authUtils.js';
import { 
	createBadRequestResponse, 
	createConflictResponse, 
	createCreatedResponse, 
	createInternalServerErrorResponse, 
	createNotFoundResponse, 
	createStatusOKResponse, 
	createUnauthorizedResponse
} from '../lib/responses.js';
import {
	validateDatetime,
	validateIdFormat
} from '../lib/validation.js';
import { 
	deletePendingReservation,
	editPendingReservation,
	getPendingReservationByID, 
	getPendingReservations, 
	getPendingReservationsBySpaceID, 
	isPendingTimeslotFree, 
	makePendingReservation
} from '../queries/PendingReservationQueries.js';

export const getPendingReservationsController = async (req, res) => {
	const pendingReservations = await getPendingReservations();
	pendingReservations
		? createStatusOKResponse(res, 'Pending reservations:', pendingReservations)
		: createNotFoundResponse(res, 'Not found');
};

export const getPendingReservationsBySpaceIdController = async (req, res) => {
	const space_id = req.params.id;
	if (!validateIdFormat(space_id)) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const PendingReservations = await getPendingReservationsBySpaceID(space_id);
		!PendingReservations.length
			? createNotFoundResponse(res, 'Not found')
			: createStatusOKResponse(res, 'Pending Reservations', PendingReservations);
	}
};

export const getPendingReservationByIDController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		const PendingReservation = await getPendingReservationByID(id);
		PendingReservation
			? createStatusOKResponse(res, 'Pending Reservation:', PendingReservation)
			: createNotFoundResponse(res, 'Not found');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const getPendingReservationByAccountIDController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		const pendingReservations = await getPendingReservationByID(id);
		pendingReservations
			? createStatusOKResponse(res, 'Pending Reservations:', pendingReservations)
			: createNotFoundResponse(res, 'Not found');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const makePendingReservationController = async (req, res) => {
	const { from_datetime, to_datetime, space_id } = req.body;
	const account_id = req.jwt_payload.sub;
	if (!validateIdFormat(space_id)
	|| !validateIdFormat(account_id)
	|| !from_datetime
	|| !to_datetime
	|| from_datetime >= to_datetime
	) {
		createBadRequestResponse(res, 'Bad request');
	} else if (!validateDatetime(from_datetime) || !validateDatetime(to_datetime)) {
		createBadRequestResponse(res, 'Bad date format');
	} else if (await isPendingTimeslotFree(from_datetime, to_datetime, space_id)) {
		const madePendingReservation = await makePendingReservation(space_id, account_id, from_datetime, to_datetime);
		madePendingReservation
			? createCreatedResponse(res, 'Pending Reservation made', madePendingReservation)
			: createInternalServerErrorResponse(res, 'Something went wrong');
	} else {
		createConflictResponse(res, 'Timeslot already reserved');
	}
};

export const deletePendingReservationController = async (req, res) => {
	const PendingReservation_id = req.params.id;
	const account_id = req.jwt_payload.sub;
	if (!PendingReservation_id || !account_id) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const Pendingreservation = await getPendingReservationByID(PendingReservation_id);
		if (Pendingreservation) {
			if (Pendingreservation.account_id === account_id || authorizeAdmin) {
				await deletePendingReservation(PendingReservation_id)
					? createStatusOKResponse(res, 'Pending Reservation deleted')
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else createUnauthorizedResponse(res, 'Unauthorized');
		} else createNotFoundResponse(res, 'Not found');
	}
};

export const editPendingReservationController = async (req, res) => {
	const { from_datetime, to_datetime, account_id } = req.body;
	const PendingReservation_id = req.params.id;
	if (
		!validateIdFormat(PendingReservation_id)
	|| !validateIdFormat(account_id)
	|| !from_datetime
	|| !to_datetime
	|| from_datetime >= to_datetime
	) {
		createBadRequestResponse(res, 'Bad request');
	} else if (!validateDatetime(from_datetime) || !validateDatetime(to_datetime)) {
		createBadRequestResponse(res, 'Bad date format');
	} else if (await isPendingTimeslotFree(from_datetime, to_datetime, PendingReservation_id)) {
		const editedPendingReservation = await editPendingReservation(PendingReservation_id, account_id, from_datetime, to_datetime);
		editedPendingReservation
			? createCreatedResponse(res, 'Pending Reservation edited', editedPendingReservation)
			: createNotFoundResponse(res, 'Not found');
	} else {
		createConflictResponse(res, 'Timeslot already reserved');
	}
};
