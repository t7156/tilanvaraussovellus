import {
	createBadRequestResponse,
	createConflictResponse,
	createForbiddenResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse,
	createStatusOKResponse,
} from '../lib/responses.js';
import { validateIdFormat, validateReviewReqBody } from '../lib/validation.js';
import { getAccountById } from '../queries/accountQueries.js';
import {
	getReviewByID,
	getReviews,
	getReviewsBySpaceID,
	getReviewsByAccountID,
	addReview,
	isReviewAccessible,
	editReview,
	reviewDoesExist,
	deleteReview,
	accountHasReviewedSpace,
	getSpaceReviewsStarsBySpaceID,
	getReviewBySpaceIDByAccountID,
} from '../queries/reviewQueries.js';
import { spaceDoesExist } from '../queries/spaceQueries.js';

export const getReviewsController = async (req, res) => {
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (validateIdFormat(page) && validateIdFormat(limit)) {
		const reviews = await getReviews(startIndex, limit);
		reviews.length
			? createStatusOKResponse(res, 'Reviews:', reviews)
			: createNotFoundResponse(res, 'No content');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};
export const getSpaceReviewsStarsAvgBySpaceIDController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		const stars = await getSpaceReviewsStarsBySpaceID(id);
		if (stars[0]) {
			const starsSum = stars.reduce((sum, spaceReview) => Number(spaceReview.stars) + sum, 0);
			const starsAvg = starsSum / stars.length;
			createStatusOKResponse(res, 'Stars Average:', starsAvg);
		} else {
			createStatusOKResponse(res, 'Stars Average:', 0);
		}
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};
export const getReviewBySpaceIDByAccountIDController = async (req, res) => {
	const account_id = req.jwt_payload.sub;
	const space_id = req.params.id;
	if (validateIdFormat(account_id) && validateIdFormat(space_id)) {
		const review = await getReviewBySpaceIDByAccountID(space_id, account_id);
		review
			? createStatusOKResponse(res, 'Review:', review)
			: createNotFoundResponse(res, 'No content');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const getReviewsBySpaceIDController = async (req, res) => {
	const id = req.params.id;
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (validateIdFormat(id) && validateIdFormat(page) && validateIdFormat(limit)) {
		const reviews = await getReviewsBySpaceID(id, startIndex, limit);
		reviews.length
			? createStatusOKResponse(res, 'Reviews:', reviews)
			: createNotFoundResponse(res, 'No content');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const getReviewByIDController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		const review = await getReviewByID(id);
		review
			? createStatusOKResponse(res, 'Review:', review)
			: createNotFoundResponse(res, 'Not found');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const getReviewsByAccountIDController = async (req, res) => {
	const id = req.params.id;
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (validateIdFormat(id) && validateIdFormat(page) && validateIdFormat(limit)) {
		const reviews = await getReviewsByAccountID(id, startIndex, limit);
		reviews.length
			? createStatusOKResponse(res, 'Reviews:', reviews)
			: createNotFoundResponse(res, 'No content');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const addReviewController = async (req, res) => {
	const { stars, comment, space_id } = req.body;
	const account_id = req.jwt_payload.sub;

	if (
		validateReviewReqBody(req.body) &&
		validateIdFormat(account_id) &&
		validateIdFormat(req.body.space_id)
	) {
		if (await spaceDoesExist(space_id)) {
			if (!(await accountHasReviewedSpace(space_id, account_id))) {
				const addedReview = await addReview(stars, comment, space_id, account_id);
				addedReview
					? createStatusOKResponse(res, 'Review added:', addedReview)
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else {
				createConflictResponse(res, 'Account already reviewed space');
			}
		} else {
			createNotFoundResponse(res, 'Not found');
		}
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const editReviewController = async (req, res) => {
	const { stars, comment } = req.body;
	const review_id = req.params.id;
	const account_id = req.jwt_payload.sub;
	const account = await getAccountById(account_id);

	if (
		validateReviewReqBody(req.body) &&
		validateIdFormat(account_id) &&
		validateIdFormat(review_id)
	) {
		if (await reviewDoesExist(review_id)) {
			if ((await isReviewAccessible(review_id, account_id)) || account.role === 'admin') {
				const editedReview = await editReview(stars, comment, review_id);
				editedReview
					? createStatusOKResponse(res, 'Review edited:', editedReview)
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else {
				createForbiddenResponse(res, 'Unauthorized');
			}
		} else {
			createNotFoundResponse(res, 'Not found');
		}
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const deleteReviewController = async (req, res) => {
	const review_id = req.params.id;
	const account_id = req.jwt_payload.sub;
	const account = await getAccountById(account_id);
	if (validateIdFormat(review_id) && validateIdFormat(account_id)) {
		if (await reviewDoesExist(review_id)) {
			if ((await isReviewAccessible(review_id, account_id)) || account.role === 'admin') {
				const deletedReview = await deleteReview(review_id);
				deletedReview
					? createStatusOKResponse(res, 'Review deleted', deletedReview)
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else {
				createForbiddenResponse(res, 'Unauthorized');
			}
		} else {
			createNotFoundResponse(res, 'Not found');
		}
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};
