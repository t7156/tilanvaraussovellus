import {
	createBadRequestResponse,
	createForbiddenResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse,
	createStatusOKResponse,
} from '../lib/responses.js';
import {
	validateIdFormat,
	validateImageFormat,
	validateSpaceReqBody,
	validateSpaceSearchParams,
} from '../lib/validation.js';
import { categoryDoesExist } from '../queries/categoryQueries.js';
import {
	getSpaceById,
	getSpaceByName,
	getSpacesByCategory,
	getSpaces,
	addSpaceToDb,
	editSpace,
	isSpaceAccessible,
	spaceDoesExist,
	deleteSpace,
	getSpacesByAccountID,
	searchSpaces,
	deleteSpaceImagesFromDB,
} from '../queries/spaceQueries.js';
import { addSpaceCategoriesToDbController } from './spaceCategoryController.js';
import { filterSpacesWithAvailableReservations } from '../utils/filterSpacesWithAvailableReservations.js';
import { convertImage, createSpaceImage, deleteSpaceImages } from '../utils/spaceImages.js';
import { deleteSpaceCategories } from '../queries/spaceCategoryQueries.js';

export const getSpacesController = async (req, res) => {
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (!validateIdFormat(page) || !validateIdFormat(limit)) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const spaces = await getSpaces(startIndex, limit);
		const spacesWithImages = spaces.map((space) => {
			space.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
			return space;
		});
		!spacesWithImages.length
			? createNotFoundResponse(res, 'No content')
			: createStatusOKResponse(res, 'Spaces', spacesWithImages);
	}
};

export const getSpacesSearchController = async (req, res) => {
	try {
		const { name, location, category, hourly_price, from_date, to_date, page, limit } =
			req.query;
		const startIndex = (page - 1) * limit;
		if (
			validateSpaceSearchParams({
				name,
				location,
				category,
				hourly_price,
				from_date,
				to_date,
			})
		) {
			const spaces = await searchSpaces(
				name ? name : '',
				location ? location : '',
				category ? category : '%',
				hourly_price ? Number(hourly_price) : 999999999,
				startIndex,
				limit
			);

			if (!!from_date && !!to_date) {
				const spacesWithAvailableReservations = await filterSpacesWithAvailableReservations(
					spaces,
					from_date,
					to_date
				);
				const spacesWithImages = spacesWithAvailableReservations.map((space) => {
					space.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
					return space;
				});
				createStatusOKResponse(res, 'Spaces:', spacesWithImages);
			} else {
				const spacesWithImages = spaces.map((space) => {
					space.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
					return space;
				});
				spacesWithImages[0]
					? createStatusOKResponse(res, 'Spaces:', spacesWithImages)
					: createNotFoundResponse(res, 'Not found');
			}
		} else {
			createBadRequestResponse(res, 'Bad request');
		}
	} catch (e) {
		console.log(e);
		createInternalServerErrorResponse(res, 'Something went wrong!');
	}
};

export const getSpaceByIdController = async (req, res) => {
	const id = req.params.id;
	if (!validateIdFormat(id)) {
		createBadRequestResponse(res, 'Bad Request');
	} else {
		const space = await getSpaceById(id);
		space?.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
		space
			? createStatusOKResponse(res, 'Space: ', space)
			: createNotFoundResponse(res, 'No content');
	}
};

export const getSpaceByNameController = async (req, res) => {
	const spaceName = req.params.name;
	if (!spaceName) createBadRequestResponse(res, 'Bad Request');
	const space = await getSpaceByName(spaceName);
	space.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
	!space
		? createNotFoundResponse(res, 'No content')
		: createStatusOKResponse(res, 'Space: ', space);
};

export const getSpacesByCategoryController = async (req, res) => {
	const spaceCategory_id = req.params.id;
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (
		!validateIdFormat(spaceCategory_id) ||
		!validateIdFormat(page) ||
		!validateIdFormat(limit)
	) {
		createBadRequestResponse(res, 'Bad Request');
	} else if (!(await categoryDoesExist(spaceCategory_id))) {
		createNotFoundResponse(res, 'Not found');
	} else {
		const spaces = await getSpacesByCategory(spaceCategory_id, startIndex, limit);
		const spacesWithImages = spaces.map((space) => {
			space.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
			return space;
		});
		!spacesWithImages.length
			? createNotFoundResponse(res, 'No content')
			: createStatusOKResponse(res, 'Spaces: ', spacesWithImages);
	}
};

export const getSpacesByAccountIDController = async (req, res) => {
	const id = req.params.id;
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (!validateIdFormat(id) || !validateIdFormat(page) || !validateIdFormat(limit)) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const spaces = await getSpacesByAccountID(id, startIndex, limit);
		const spacesWithImages = spaces.map((space) => {
			space.image[0] ? (space.image = convertImage(space.image[0].image_path)) : null;
			return space;
		});
		spacesWithImages.length
			? createStatusOKResponse(res, 'Spaces:', spacesWithImages)
			: createNotFoundResponse(res, 'No content');
	}
};

export const addSpaceToDbController = async (req, res) => {
	const { name, address, size, description, hourly_price, spaceCategories } = req.body;
	const image = req.files.image;
	const owner_id = req.jwt_payload.sub;
	const categories = spaceCategories.split(',');
	
	if (categories.every(validateIdFormat) &&
	validateSpaceReqBody(req.body) &&
	validateIdFormat(owner_id)
	) {
		const addedSpace = await addSpaceToDb(
			name,
			address,
			size,
			description,
			hourly_price,
			owner_id
		);
		await createSpaceImage(addedSpace.id, image);
		if (categories.length > 0) {
			if (addSpaceCategoriesToDbController(categories, addedSpace.id)) {
				addedSpace
					? createStatusOKResponse(res, 'Space added', addedSpace)
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else createInternalServerErrorResponse(res, 'Something went wrong');
		} else createBadRequestResponse(res, 'Bad Request');
	} else createBadRequestResponse(res, 'Bad Request');
};

export const editSpaceController = async (req, res) => {
	const { name, address, size, description, hourly_price, spaceCategories, deleteImage } = req.body;
	const owner_id = req.jwt_payload.sub;
	const space_id = req.params.id;
	const categories = spaceCategories.split(',');
	const image = req.files?.image;
	if (
		validateSpaceReqBody(req.body) &&
		categories.every(validateIdFormat) &&
		(await spaceDoesExist(space_id)) &&
		(!image || validateImageFormat(image))
	) {
		if (await isSpaceAccessible(space_id, owner_id)) {
			const editedSpace = await editSpace(
				name,
				address,
				size,
				description,
				hourly_price,
				space_id
			);
			if (deleteImage === 'true') {
				const deletedImages = await deleteSpaceImagesFromDB(space_id);
				deleteSpaceImages(deletedImages);
				editedSpace.image = undefined;
			} else if (image) {
				const deletedImages = await deleteSpaceImagesFromDB(space_id);
				deleteSpaceImages(deletedImages);
				await createSpaceImage(space_id, image);
			}
			if (editedSpace && await deleteSpaceCategories(editedSpace.id)) {
				addSpaceCategoriesToDbController(categories, editedSpace.id)
					? createStatusOKResponse(res, 'Space edited', editedSpace)
					: createInternalServerErrorResponse(res, 'Something went wrong');
			} else createNotFoundResponse(res, 'Not found');
		} else createForbiddenResponse(res, 'Forbidden');
	} else createBadRequestResponse(res, 'Bad request');
};

export const deleteSpaceController = async (req, res) => {
	const space_id = req.params.id;
	const account_id = req.jwt_payload.sub;
	if (
		!space_id ||
		!(await spaceDoesExist(space_id)) ||
		!validateIdFormat(space_id) ||
		!validateIdFormat(account_id)
	) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		if (await isSpaceAccessible(space_id, account_id)) {
			try {
				const deletedImages = await deleteSpace(space_id);
				deleteSpaceImages(deletedImages);
				createStatusOKResponse(res, 'Space deleted');
			} catch (e) {
				createInternalServerErrorResponse(res, 'Something went wrong');
			}
		} else {
			createForbiddenResponse(res, 'Forbidden');
		}
	}
};
