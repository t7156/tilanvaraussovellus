import { authorizeAdmin } from '../auth/authUtils.js';
import {
	createBadRequestResponse,
	createConflictResponse,
	createCreatedResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse,
	createStatusOKResponse,
	createUnauthorizedResponse,
} from '../lib/responses.js';
import { validateDate, validateDatetime, validateIdFormat } from '../lib/validation.js';
import { convertDateToUTC } from '../utils/convertDateToUTC.js';
import {
	deleteReservation,
	editReservation,
	getReservationByID,
	getReservations,
	getReservationsBySpaceID,
	isTimeslotFree,
	makeReservation,
	getReservationsByAccountID,
	getPastReservationsByAccountID,
	getFutureReservationsByAccountID,
} from '../queries/reservationQueries.js';

export const getReservationsController = async (req, res) => {
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	if (!validateIdFormat(page) || !validateIdFormat(limit)) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const reservations = await getReservations(startIndex, limit);
		reservations.length
			? createStatusOKResponse(res, 'Reservations:', reservations)
			: createNotFoundResponse(res, 'No content');
	}
};

export const getAvailableReservationsBySpaceIDByDateController = async (req, res) => {
	try {
		const { space_id, date } = req.params;
		if (validateIdFormat(space_id) && validateDate(date)) {
			const fromDatetime = convertDateToUTC(new Date(date));
			const toDatetime = convertDateToUTC(new Date(date));
			toDatetime.setHours(toDatetime.getHours() + 1);
			const availableReservationHours = [];
			for (const timeslot = { hour: 0 }; timeslot.hour < 24; timeslot.hour++) {
				const isAvailable = await isTimeslotFree(fromDatetime, toDatetime, space_id);
				availableReservationHours.push({ hour: timeslot.hour, available: isAvailable });
				fromDatetime.setHours(fromDatetime.getHours() + 1);
				toDatetime.setHours(toDatetime.getHours() + 1);
			}
			createStatusOKResponse(res, 'Available reservations: ', availableReservationHours);
		} else createBadRequestResponse(res, 'Bad Request!');
	} catch (e) {
		createInternalServerErrorResponse(res, 'Something went wrong!');
	}
};

export const getReservationsBySpaceIdController = async (req, res) => {
	const page = req.query.page;
	const limit = req.query.limit;
	const startIndex = (page - 1) * limit;
	const space_id = req.params.id;
	if (!validateIdFormat(space_id) || !validateIdFormat(page) || !validateIdFormat(limit)) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const reservations = await getReservationsBySpaceID(space_id, startIndex, limit);
		!reservations.length
			? createNotFoundResponse(res, 'No content')
			: createStatusOKResponse(res, 'Reservations', reservations);
	}
};

export const getReservationByIDController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		const reservation = await getReservationByID(id);
		reservation
			? createStatusOKResponse(res, 'Reservation:', reservation)
			: createNotFoundResponse(res, 'Not found');
	} else {
		createBadRequestResponse(res, 'Bad request');
	}
};

export const getReservationsByAccountIDController = async (req, res) => {
	const { page, limit, from } = req.query;

	const startIndex = (page - 1) * limit;
	const id = req.params.id;
	const date = new Date();

	if (!validateIdFormat(id) || !validateIdFormat(page) || !validateIdFormat(limit)) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const reservations = { reservations: [] };
		from === 'past'
			? (reservations.reservations = await getPastReservationsByAccountID(
				id,
				date,
				startIndex,
				limit
			  ))
			: from === 'future'
				? (reservations.reservations = await getFutureReservationsByAccountID(
					id,
					date,
					startIndex,
					limit
			  ))
				: (reservations.reservations = await getReservationsByAccountID(id, startIndex, limit));
		reservations.reservations.length
			? createStatusOKResponse(res, 'Reservations:', reservations.reservations)
			: createNotFoundResponse(res, 'No content');
	}
};

export const makeReservationController = async (req, res) => {
	const { from_datetime, to_datetime, space_id } = req.body;
	const account_id = req.jwt_payload.sub;
	if (
		!validateIdFormat(space_id) ||
		!validateIdFormat(account_id) ||
		!from_datetime ||
		!to_datetime ||
		new Date() > new Date(from_datetime) ||
		from_datetime >= to_datetime
	) {
		createBadRequestResponse(res, 'Bad request');
	} else if (!validateDatetime(from_datetime) || !validateDatetime(to_datetime)) {
		createBadRequestResponse(res, 'Bad date format');
	} else if (await isTimeslotFree(from_datetime, to_datetime, space_id)) {
		const madeReservation = await makeReservation(
			space_id,
			account_id,
			from_datetime,
			to_datetime
		);
		madeReservation
			? createCreatedResponse(res, 'Reservation made', madeReservation)
			: createInternalServerErrorResponse(res, 'Something went wrong');
	} else {
		createConflictResponse(res, 'Timeslot already reserved');
	}
};

export const deleteReservationController = async (req, res) => {
	const reservation_id = req.params.id;
	const account_id = req.jwt_payload.sub;
	if (!reservation_id || !account_id) {
		createBadRequestResponse(res, 'Bad request');
	} else {
		const reservation = await getReservationByID(reservation_id);
		if (reservation) {
			if (reservation.account_id === account_id || authorizeAdmin) {
				(await deleteReservation(reservation_id))
					? createStatusOKResponse(res, 'Reservation deleted')
					: createInternalServerErrorResponse(res, 'Something went wrnog');
			} else createUnauthorizedResponse(res, 'Unauthorized');
		} else createNotFoundResponse(res, 'Not found');
	}
};

export const editReservationController = async (req, res) => {
	const { from_datetime, to_datetime, account_id } = req.body;
	const reservation_id = req.params.id;
	if (
		!validateIdFormat(reservation_id) ||
		!validateIdFormat(account_id) ||
		!from_datetime ||
		!to_datetime ||
		from_datetime >= to_datetime
	) {
		createBadRequestResponse(res, 'Bad request');
	} else if (!validateDatetime(from_datetime) || !validateDatetime(to_datetime)) {
		createBadRequestResponse(res, 'Bad date format');
	} else if (await isTimeslotFree(from_datetime, to_datetime, reservation_id)) {
		const editedReservation = await editReservation(
			reservation_id,
			account_id,
			from_datetime,
			to_datetime
		);
		editedReservation
			? createCreatedResponse(res, 'Reservation edited', editedReservation)
			: createNotFoundResponse(res, 'Not found');
	} else {
		createConflictResponse(res, 'Timeslot already reserved');
	}
};
