import {
	createBadRequestResponse,
	createInternalServerErrorResponse,
	createNotFoundResponse,
	createStatusOKResponse 
} from '../lib/responses.js';
import {
	validateCategoryNameFormat,
	validateIdFormat
} from '../lib/validation.js';
import {
	categoryDoesExist,
	getCategories,
	getCategoryById,
	addCategoryToDb,
	editCategory,
	deleteCategory
} from '../queries/categoryQueries.js';

export const getCategoriesController = async (req, res) => {
	const categories = await getCategories();
	categories.length
		? createStatusOKResponse(res, 'Categories', categories)
		: createNotFoundResponse(res, 'Not found');
};

export const getCategoryByIdController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		if (await categoryDoesExist(id)) {
			const category = await getCategoryById(id);
			category
				? createStatusOKResponse(res, 'Category:', category)
				: createInternalServerErrorResponse(res, 'Something went wrong');
		} else createNotFoundResponse(res, 'Not found');
	} else createBadRequestResponse(res, 'Bad request');
};

export const addCategoryToDbController = async (req, res) => {
	const category_name = req.body.category_name;
	if (validateCategoryNameFormat(category_name)) {
		const category = await addCategoryToDb(category_name);
		category 
			? createStatusOKResponse(res, 'Category added', category)
			: createInternalServerErrorResponse(res, 'Something went wrong');
	} else createBadRequestResponse(res, 'Bad request');
};

export const editCategoryController = async (req, res) => {
	const id = req.params.id;
	const category_name = req.body.category_name;
	if (validateCategoryNameFormat(category_name) && validateIdFormat(id)) {
		const category = await editCategory(category_name, id);
		category 
			? createStatusOKResponse(res, 'Category edited', category)
			: createInternalServerErrorResponse(res, 'Something went wrong');
	} else createBadRequestResponse(res, 'Bad request');
};

export const deleteCategoryController = async (req, res) => {
	const id = req.params.id;
	if (validateIdFormat(id)) {
		if (await categoryDoesExist(id)){
			const deletedCategory = await deleteCategory(id);
			deletedCategory
				? createStatusOKResponse(res, 'Category deleted', deletedCategory)
				: createInternalServerErrorResponse(res, 'Something went wrong');
		} else createNotFoundResponse(res, 'Not found');
	} else createBadRequestResponse(res, 'Bad request');
};