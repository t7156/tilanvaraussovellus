import { convertDateToUTC } from '../utils/convertDateToUTC.js';
import { getReservationsBySpaceIdBetweenDates } from '../queries/reservationQueries.js';

export const filterSpacesWithAvailableReservations = async (spaces, from_date, to_date) => {
	// need to add a check later if space has time/date restrictions for reservations
	const dates = {
		fromDate: new Date(from_date),
		toDate: new Date(to_date),
	};
	dates.fromDate = convertDateToUTC(dates.fromDate);
	dates.toDate = convertDateToUTC(dates.toDate);

	dates.toDate.setDate(dates.toDate.getDate() + 1);
	const spacesWithAvailableReservations = [];
	for (const space of spaces) {
		const reservationsBetweenDates = await getReservationsBySpaceIdBetweenDates(
			space.id,
			dates.fromDate,
			dates.toDate
		);
		const searchRangeInMS = dates.toDate.getTime() - dates.fromDate.getTime();
		const reservationsTotalTime = { ms: 0 };
		for (const reservation of reservationsBetweenDates) {
			const reservationFromDatetime = new Date(reservation.from_datetime);
			const reservationToDatetime = new Date(reservation.to_datetime);
			const reservationFromResidueInMS =
				reservationFromDatetime.getTime() - dates.fromDate.getTime();
			const reservationToResidueInMS =
				dates.toDate.getTime() - reservationToDatetime.getTime();
			const reservationTime = {
				ms: reservationToDatetime.getTime() - reservationFromDatetime.getTime(),
			};
			if (reservationFromResidueInMS < 0) reservationTime.ms += reservationFromResidueInMS;
			if (reservationToResidueInMS < 0) reservationTime.ms += reservationToResidueInMS;
			reservationsTotalTime.ms += reservationTime.ms;
		}
		if (searchRangeInMS > reservationsTotalTime.ms || reservationsTotalTime.ms === 0)
			spacesWithAvailableReservations.push(space);
	}
	return spacesWithAvailableReservations;
};
