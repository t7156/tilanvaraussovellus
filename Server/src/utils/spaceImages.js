import {addSpaceImageToDB} from '../queries/spaceQueries.js';
import path from 'path';
import { nanoid } from 'nanoid';
import fs from 'fs';
import { validateImageFormat, validateImageSize } from '../lib/validation.js';

export const createSpaceImage = async (space_id, image) => {

	if (validateImageFormat(image) && validateImageSize(image)){
		try{
			const uploadFile = async (space_id, image) => {
				const extension = path.extname(image.name);
				const newName = path.parse(image.name).name.concat('_space', space_id, '_', nanoid(7),  extension);
				image.name = newName;
				const filePath = path.join('.', '/', 'src', '/', 'files', '/', 'uploads', image.name);

				image.mv(filePath, function(err) {
					if (err) {
						return (err); /* Todo: Better error handling */
					}
				});
				await addSpaceImageToDB(space_id, newName);
			};

			uploadFile(space_id, image).then(() => {
				return;
			});
		}catch(e) {
			console.log(e);
		}
	} else return;
};

export const convertImage = (image_path) => {
	const image = fs.readFileSync(`./src/files/uploads/${image_path}`);
	const imageBase64String = new Buffer.from(image).toString('base64');
	return imageBase64String;
};

export const deleteSpaceImages = (images) => {
	for(const image of images) {
		fs.unlinkSync(`./src/files/uploads/${image.image_path}`);
	}
};
