import db from '../config/db.js';

const createReviewTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Review (
		id SERIAL PRIMARY KEY,
		stars VARCHAR (1),
		comment VARCHAR (255),
		space_id int,
		account_id int,
			FOREIGN KEY (space_id)
				REFERENCES Space(id),
			FOREIGN KEY (account_id)
				REFERENCES Account(id)
	);`
		)
		.catch((error) => {
			console.log('createReviewTable', error);
		});
};

const createPendingReservationTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Pending_reservation (
		id SERIAL PRIMARY KEY,
		space_id int,
		account_id int,
		from_datetime TIMESTAMP NOT NULL,
		to_datetime TIMESTAMP NOT NULL,
			FOREIGN KEY (space_id)
				REFERENCES Space(id),
			FOREIGN KEY (account_id)
				REFERENCES Account (id)
	);`
		)
		.catch((error) => {
			console.log('createPendingReservation', error);
		});
};

const createReservationTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Reservation (
		id SERIAL PRIMARY KEY,
		creation_time TIMESTAMP NOT NULL,
		space_id int,
		account_id int,
		from_datetime TIMESTAMP NOT NULL,
		to_datetime TIMESTAMP NOT NULL,
			FOREIGN KEY (space_id)
				REFERENCES Space (id),
			FOREIGN KEY (account_id)
				REFERENCES Account (id)
	);`
		)
		.catch((error) => {
			console.log('createReservation', error);
		});
};

const createSpaceCategoryTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Space_category (
		space_id int,
		category_id int,
			FOREIGN KEY (space_id)
				REFERENCES Space (id),
			FOREIGN KEY (category_id)
				REFERENCES Category (id)
	);`
		)
		.catch((error) => {
			console.log('createSpaceGatergory', error);
		});
};

const createCategoryTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Category (
		id SERIAL PRIMARY KEY,
		category_name VARCHAR (100) NOT NULL
	);`
		)
		.catch((error) => {
			console.log('createCategoryTable', error);
		});
};

const createSpaceTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Space (
		id SERIAL PRIMARY KEY,
		name VARCHAR (255) NOT NULL,
		address VARCHAR (255) NOT NULL,
		size VARCHAR (50),
		description VARCHAR (999) NOT NULL,
		hourly_price VARCHAR (50),
		owner_id int,
		FOREIGN KEY (owner_id)
			REFERENCES Account (id)
	);`
		)
		.catch((error) => {
			console.log('createSpace', error);
		});
};

const createAccountTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Account (
		id SERIAL PRIMARY KEY,
		username VARCHAR (20) NOT NULL,
		email VARCHAR (50) NOT NULL,
		hash VARCHAR (255) NOT NULL,
		salt VARCHAR (255) NOT NULL,
		role VARCHAR (20) NOT NULL,
		confirmed BOOLEAN NOT NULL,
		confirmation_code VARCHAR (999)
	);`
		)
		.catch((error) => {
			console.log('createAccount', error);
		});
};

const createSpaceImageTable = async () => {
	return db
		.query(
			`CREATE TABLE IF NOT EXISTS Space_Image (
				id SERIAL PRIMARY KEY,
				space_id INT,
				image_path VARCHAR(999),
					FOREIGN KEY (space_id)
					REFERENCES Space(id)
				);`
		)
		.catch((error) => {
			console.log('createSpaceImage', error);
		});
};

const createTables = async () => {
	await createAccountTable();
	await createSpaceTable();
	await createCategoryTable();
	await createSpaceCategoryTable();
	await createReservationTable();
	await createPendingReservationTable();
	await createReviewTable();
	await createSpaceImageTable();
	console.log('Tables created.');
};

export default createTables;
