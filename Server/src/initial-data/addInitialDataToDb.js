import db from '../config/db.js';

export const addAccountToDb = async (username, email, salt, hash, role, confirmed, confirmation_code) => {
	await db
		.query(
			`INSERT INTO Account
	(username, email, salt, hash, role, confirmed, confirmation_code)
			VALUES ($1, $2, $3, $4, $5, $6, $7)`,
			[username, email, salt, hash, role, confirmed, confirmation_code]
		)
		.catch((err) => {
			console.log(err);
		});
};

export const addCategoryToDb = async (category_name) => {
	await db
		.query(
			`INSERT INTO Category
			(category_name)
			VALUES ($1)`,
			[category_name]
		)
		.catch((err) => {
			console.log(err);
		});
};

export const addSpaceCategoryToDb = async (space_id, category_id) => {
	await db
		.query(
			`INSERT INTO Space_category
			(space_id, category_id)
			VALUES ($1, $2)`,
			[space_id, category_id]
		)
		.catch((err) => {
			console.log(err);
		});
};

export const addSpaceToDb = async (name, address, size, description, hourly_price, owner_id) => {
	await db
		.query(
			`INSERT INTO Space
			(name, address, size, description, hourly_price, owner_id)
			VALUES ($1, $2, $3, $4, $5, $6)`,
			[name, address, size, description, hourly_price, owner_id]
		)
		.catch((err) => {
			console.log(err);
		});
};

export const addPendingReservationToDb = async (space_id, account_id, from_datetime, to_datetime) => {
	await db
		.query(
			`INSERT INTO Pending_reservation
			(space_id, account_id, from_datetime, to_datetime)
			VALUES ($1, $2, $3, $4)`,
			[space_id, account_id, from_datetime, to_datetime]
		)
		.catch((err) => {
			console.log(err);
		});
};

export const addReservationToDb = async (
	creation_time,
	space_id,
	account_id,
	from_datetime,
	to_datetime
) => {
	await db
		.query(
			`INSERT INTO Reservation
			(creation_time, space_id, account_id, from_datetime, to_datetime)
			VALUES ($1, $2, $3, $4, $5)`,
			[creation_time, space_id, account_id, from_datetime, to_datetime]
		)
		.catch((err) => {
			console.log(err);
		});
};

export const addReviewToDb = async (stars, comment, space_id, account_id) => {
	await db
		.query(
			`INSERT INTO Review
	(stars, comment, space_id, account_id)
			VALUES ($1, $2, $3, $4)`,
			[stars, comment, space_id, account_id]
		)
		.catch((err) => {
			console.log(err);
		});
};
