/* eslint-disable immutable/no-let */
import fs from 'fs';
import { getSaltHashFromPassword } from '../auth/authUtils.js';
import createTables from './createTables.js';
import {
	addAccountToDb,
	addSpaceToDb,
	addCategoryToDb,
	addSpaceCategoryToDb,
	addReservationToDb,
	addPendingReservationToDb,
	addReviewToDb,
} from './addInitialDataToDb.js';

const createInitialAccounts = async () => {
	const rawdata = fs.readFileSync('account.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		const { salt, hash } = getSaltHashFromPassword(data[i].password);
		await addAccountToDb(data[i].username, data[i].email, salt, hash, data[i].role, data[i].confirmed, data[i].confirmation_code);
	}
	console.log('Accounts added.');
};

const createInitialSpaces = async () => {
	const rawdata = fs.readFileSync('space.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		await addSpaceToDb(
			data[i].name,
			data[i].address,
			data[i].size,
			data[i].description,
			data[i].hourly_price,
			data[i].owner_id
		);
	}
	console.log('Spaces added.');
};

const createInitialCategories = async () => {
	const rawdata = fs.readFileSync('category.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		await addCategoryToDb(data[i].category_name);
	}
	console.log('Categories added.');
};

const createInitialSpaceCategories = async () => {
	const rawdata = fs.readFileSync('spaceCategory.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		await addSpaceCategoryToDb(data[i].space_id, data[i].category_id);
	}
	console.log('Space-Categories added.');
};

const createInitialReservations = async () => {
	const rawdata = fs.readFileSync('reservation.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		await addReservationToDb(
			data[i].creation_time,
			data[i].space_id,
			data[i].account_id,
			data[i].from_datetime,
			data[i].to_datetime
		);
	}
	console.log('Reservations added.');
};

const createInitialPendingReservations = async () => {
	const rawdata = fs.readFileSync('pendingReservation.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		await addPendingReservationToDb(
			data[i].space_id,
			data[i].account_id,
			data[i].from_datetime,
			data[i].to_datetime
		);
	}
	console.log('Pending reservations added.');
};

const createInitialReviews = async () => {
	const rawdata = fs.readFileSync('review.json');
	const data = JSON.parse(rawdata);
	for (let i = 0; i < data.length; i++) {
		await addReviewToDb(data[i].stars, data[i].comment, data[i].space_id, data[i].account_id);
	}
	console.log('Reviews added.');
};

const createInitialData = async () => {
	await createTables();
	await createInitialAccounts();
	await createInitialSpaces();
	await createInitialCategories();
	await createInitialSpaceCategories();
	await createInitialReservations();
	await createInitialPendingReservations();
	await createInitialReviews();
};

createInitialData();
