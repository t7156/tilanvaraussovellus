import path from 'path';

export const validatePasswordFormat = (password) => {
	return !password ? false : password.length < 8 || password.length > 254 ? false : true;
};

export const validateUsernameFormat = (username) => {
	return !username ? false : username.length < 5 || username.length > 100 ? false : true;
};

export const validateEmailFormat = (email) => {
	return !email ? false : email.length < 10 || email.length > 254 ? false : true;
};

export const validateIdFormat = (id) => {
	return isNaN(id) || !id || isNaN(parseInt(id)) ? false : true;
};

export const validateCategoryNameFormat = (category) => {
	return !category ? false : category.length < 2 || category.length > 30 ? false : true;
};

export const validateSpaceSearchParams = (obj) => {
	return (!obj.name || obj.name.length < 71) &&
		(!obj.location || obj.location.length < 71) &&
		(!obj.category || validateIdFormat(obj.category)) &&
		(!obj.hourly_price || obj.hourly_price < 999999999) &&
		((!obj.from_date && !obj.to_date) ||
			(validateDate(obj.from_date) &&
				validateDate(obj.to_date) &&
				new Date(obj.from_date) <= new Date(obj.to_date)))
		? true
		: false;
};

export const validateSpaceReqBody = (obj) => {
	if (obj.address) {
		const address = JSON.parse(obj.address);
		return (
			obj.name.length >= 3 &&
			obj.name.length <= 50 &&
			address.street.length >= 5 &&
			address.street.length <= 50 &&
			address.city.length >= 2 &&
			address.city.length <= 50 &&
			obj.size >= 1 &&
			obj.hourly_price >= 1 &&
			obj.description.length >= 10 &&
			obj.description.length <= 900 &&
			validateIdFormat(address.postal) ||
			validateIdFormat(obj.size) ||
			validateIdFormat(obj.hourly_price)
		) ? true : false;
	} else return false;
};

export const validateDatetime = (dateTime) => {
	const year = dateTime.slice(0, 4);
	const month = dateTime.slice(5, 7);
	const day = dateTime.slice(8, 10);
	const hour = dateTime.slice(11, 13);
	const minutes = dateTime.slice(14, 16);
	const seconds = dateTime.slice(17, 19);
	const matches = dateTime.match(/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/);
	const date = new Date(year, month, day, hour, minutes, seconds);
	return year < new Date().getFullYear() ||
		parseInt(date.getFullYear()) !== parseInt(year) ||
		parseInt(date.getMonth()) !== parseInt(month) ||
		parseInt(date.getDate()) !== parseInt(day) ||
		parseInt(date.getHours()) !== parseInt(hour) ||
		parseInt(date.getMinutes()) !== parseInt(minutes) ||
		parseInt(date.getSeconds()) !== parseInt(seconds) ||
		matches === null
		? false
		: true;
};
export const validateDate = (date) => {
	try {
		const year = date.slice(0, 4);
		const month = date.slice(5, 7);
		const day = date.slice(8, 10);
		const matches = date.match(/^(\d{4})-(\d{2})-(\d{2})$/);
		const newDate = new Date(year, month, day);
		return year < new Date().getFullYear() ||
			parseInt(newDate.getFullYear()) !== parseInt(year) ||
			parseInt(newDate.getMonth()) !== parseInt(month) ||
			parseInt(newDate.getDate()) !== parseInt(day) ||
			matches === null
			? false
			: true;
	} catch (e) {
		return false;
	}
};

export const validateImageFormat = (file) => {
	const allowedExtension = ['.png', '.jpg', '.jpeg', '.PNG', '.JPG', '.JPEG'];
	const extension = path.extname(file.name);
	return !allowedExtension.includes(extension) ? false : true;
};

export const validateImageSize = (file) => {
	return file.size > 1024 * 1024 ? false : true;
};

export const validateReviewReqBody = (review) => {
	return !review.stars ||
		!review.comment ||
		review.stars < 1 ||
		review.stars > 5 ||
		review.comment.length < 1 ||
		review.comment.length > 255
		? false
		: true;
};
