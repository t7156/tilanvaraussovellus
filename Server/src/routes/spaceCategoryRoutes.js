import express from 'express';
import {
	getSpaceCategoriesController
} from '../controllers/spaceCategoryController.js';

const spaceCategoryRouter = express.Router();

spaceCategoryRouter.get('/', getSpaceCategoriesController);

export default spaceCategoryRouter;