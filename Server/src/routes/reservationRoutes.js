import express from 'express';
import {
	authenticateJWT,
	authorizeAdmin
} from '../auth/authUtils.js';
import { 
	deleteReservationController, 
	editReservationController, 
	getReservationByIDController, 
	getReservationsByAccountIDController, 
	getReservationsBySpaceIdController, 
	getReservationsController, 
	makeReservationController,
	getAvailableReservationsBySpaceIDByDateController
} from '../controllers/reservationController.js';

const reservationRouter = express.Router();

reservationRouter.get('/all', getReservationsController);
reservationRouter.get('/space/:id', getReservationsBySpaceIdController);
reservationRouter.get('/:id', getReservationByIDController);
reservationRouter.get('/account/:id', getReservationsByAccountIDController);
reservationRouter.get('/available_on_date/:space_id&:date', getAvailableReservationsBySpaceIDByDateController);
reservationRouter.post('/', authenticateJWT, makeReservationController);
reservationRouter.delete('/:id', authenticateJWT, deleteReservationController);
reservationRouter.patch('/:id', authenticateJWT, authorizeAdmin, editReservationController);

export default reservationRouter;