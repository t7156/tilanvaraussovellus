import express from 'express';
import { authenticateJWT } from '../auth/authUtils.js';
import {
	createAccountController,
	loginController,
	verifyAuthenticationController,
	verifyPasswordController,
	emailConfirmationController,
	reSendEmailConfirmationCodeController
} from '../controllers/authController.js';

const authRouter = express.Router();

authRouter.post('/register', createAccountController);
authRouter.post('/login', loginController);
authRouter.post('/verify_password', authenticateJWT, verifyPasswordController);
authRouter.get('/verify_authentication', authenticateJWT, verifyAuthenticationController);
authRouter.patch('/verify/:confirmation_code', emailConfirmationController);
authRouter.patch('/confirmation', reSendEmailConfirmationCodeController);

export default authRouter;
