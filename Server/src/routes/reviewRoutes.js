import express from 'express';
import { authenticateJWT } from '../auth/authUtils.js';
import {
	addReviewController,
	deleteReviewController,
	editReviewController,
	getReviewByIDController,
	getReviewsByAccountIDController,
	getReviewsBySpaceIDController,
	getReviewsController,
	getSpaceReviewsStarsAvgBySpaceIDController,
	getReviewBySpaceIDByAccountIDController,
} from '../controllers/reviewController.js';

const reviewRouter = express.Router();

reviewRouter.get('/', getReviewsController);
reviewRouter.get('/stars_average/:id', getSpaceReviewsStarsAvgBySpaceIDController);
reviewRouter.get('/space/account/:id', authenticateJWT, getReviewBySpaceIDByAccountIDController);
reviewRouter.get('/:id', getReviewByIDController);
reviewRouter.get('/space/:id', getReviewsBySpaceIDController);
reviewRouter.get('/account/:id', getReviewsByAccountIDController);
reviewRouter.post('/', authenticateJWT, addReviewController);
reviewRouter.patch('/:id', authenticateJWT, editReviewController);
reviewRouter.delete('/:id', authenticateJWT, deleteReviewController);

export default reviewRouter;
