import express from 'express';
import {
	authenticateJWT,
	authorizeAdmin
} from '../auth/authUtils.js';
import {
	addCategoryToDbController,
	deleteCategoryController,
	editCategoryController,
	getCategoriesController,
	getCategoryByIdController
} from '../controllers/categoryController.js';

const categoryRouter = express.Router();

categoryRouter.get('/', getCategoriesController);
categoryRouter.get('/:id', getCategoryByIdController);
categoryRouter.post('/', authenticateJWT, authorizeAdmin, addCategoryToDbController);
categoryRouter.put('/:id', authenticateJWT, authorizeAdmin, editCategoryController);
categoryRouter.delete('/:id', authenticateJWT, authorizeAdmin, deleteCategoryController);

export default categoryRouter;