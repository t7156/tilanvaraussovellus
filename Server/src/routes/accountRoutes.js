import express from 'express';
import { authenticateJWT } from '../auth/authUtils.js';
import {
	changePasswordController,
	deleteAccountController,
	editAccountController,
	getAccountController,
} from '../controllers/accountController.js';

const accountRouter = express.Router();

accountRouter.get('/', authenticateJWT, getAccountController);
accountRouter.patch('/password', authenticateJWT, changePasswordController);
accountRouter.delete('/:id', authenticateJWT, deleteAccountController);
accountRouter.put('/:id', authenticateJWT, editAccountController);

export default accountRouter;
