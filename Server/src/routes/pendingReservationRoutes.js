import express from 'express';
import {
	authenticateJWT,
	authorizeAdmin
} from '../auth/authUtils.js';
import { 
	deletePendingReservationController, 
	editPendingReservationController, 
	getPendingReservationByAccountIDController, 
	getPendingReservationByIDController, 
	getPendingReservationsBySpaceIdController, 
	getPendingReservationsController, 
	makePendingReservationController 
} from '../controllers/PendingReservationController.js';

const pendingReservationRouter = express.Router();

pendingReservationRouter.get('/all', getPendingReservationsController);
pendingReservationRouter.get('/space/:id', getPendingReservationsBySpaceIdController);
pendingReservationRouter.get('/:id', getPendingReservationByIDController);
pendingReservationRouter.get('/account/:id', getPendingReservationByAccountIDController);
pendingReservationRouter.post('/', authenticateJWT, makePendingReservationController);
pendingReservationRouter.delete('/:id', authenticateJWT, deletePendingReservationController);
pendingReservationRouter.patch('/:id', authenticateJWT, authorizeAdmin, editPendingReservationController);

export default pendingReservationRouter;
