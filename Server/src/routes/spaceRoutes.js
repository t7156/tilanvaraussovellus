import express from 'express';
import {
	getSpacesController,
	getSpaceByNameController,
	getSpacesByCategoryController,
	addSpaceToDbController,
	editSpaceController,
	deleteSpaceController,
	getSpacesByAccountIDController,
	getSpaceByIdController,
	getSpacesSearchController
} from '../controllers/spaceController.js';
import { authenticateJWT } from '../auth/authUtils.js';

const spaceRouter = express.Router();

spaceRouter.get('/', getSpacesController);
spaceRouter.get('/search', getSpacesSearchController);
spaceRouter.get('/:id', getSpaceByIdController);
spaceRouter.get('/name/:name', getSpaceByNameController);
spaceRouter.get('/category/:id', getSpacesByCategoryController);
spaceRouter.get('/account/:id', authenticateJWT, getSpacesByAccountIDController);
spaceRouter.post('/', authenticateJWT, addSpaceToDbController);
spaceRouter.put('/:id', authenticateJWT, editSpaceController);
spaceRouter.delete('/:id', authenticateJWT, deleteSpaceController);

export default spaceRouter;
