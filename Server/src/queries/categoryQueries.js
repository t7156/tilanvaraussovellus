import db from '../config/db.js';

export const categoryDoesExist = async (id) => {
	const result = await db.query(`
	SELECT * FROM category
	WHERE id = $1`,
	[id]
	);
	return result.rows[0] ? true : false;
};

export const getCategories = async () => {
	const result = await db.query(
		`SELECT *
		FROM category`,
	);
	return result.rows;
};

export const getCategoriesBySpaceId = async (id) => {
	const result = await db.query(
		`SELECT 
		C.id AS id,
		C.category_name AS name
		FROM Category AS C
		LEFT JOIN Space_category AS Sc
		ON $1 = Sc.space_id
		WHERE C.id = Sc.category_id`,
		[id]
	);
	return result.rows;
};

export const getCategoryById = async (id) => {
	const result = await db.query(
		`SELECT *
		FROM category
		WHERE id = $1`,
		[id]
	);
	return result.rows[0];
};

export const addCategoryToDb = async (category_name) => {
	const result = await db.query(
		`INSERT INTO Category
		(category_name)
		VALUES ($1)
		RETURNING *`,
		[category_name]
	);
	return result.rows[0];
};

export const editCategory = async (category_name, id) => {
	const result = await db.query(
		`UPDATE category
		SET category_name = $1
		WHERE id = $2
		RETURNING *`,
		[category_name, id]
	);
	return result.rows[0];
};

export const deleteCategory = async (id) => {
	const result = await db.query(
		`DELETE FROM category
		WHERE id = $1
		RETURNING *`,
		[id]
	);
	return result.rows[0];
};