import db from '../config/db.js';

export const changePassword = async (salt, hash, id) => {
	const result = await db.query(
		`
		UPDATE account
		SET salt = $1,
		hash = $2
		WHERE id = $3
		RETURNING *`,
		[salt, hash, id]
	);
	return result.rows[0];
};

export const getAccountById = async (id) => {
	const result = await db.query(
		`SELECT * FROM Account
		WHERE id = $1`,
		[id]
	);
	return result.rows[0];
};

export const deleteAccount = async (id) => {
	await db
		.query('BEGIN')
		.then(() => {
			return db.query(
				`DELETE FROM Reservation
				WHERE account_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Pending_reservation
					WHERE account_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Review
				WHERE account_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Space_category
				WHERE space_id IN (SELECT id FROM Space WHERE owner_id = $1)`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Space
				WHERE owner_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Account
				WHERE id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query('commit');
		});
};

export const editAccount = async (username, email, id) => {
	const result = await db.query(`
		UPDATE account
		SET username = $1,
		email = $2
		WHERE id = $3
		RETURNING username, email`,
	[username, email, id]);
	return result.rows[0];
};