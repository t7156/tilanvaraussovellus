import db from '../config/db.js';

export const addAccountToDb = async (username, email, salt, hash, role, confirmed) => {
	const result = await db.query(
		`INSERT INTO Account
		(username, email, salt, hash, role, confirmed)
			VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
		[username, email, salt, hash, role, confirmed]
	);

	return result.rows[0];
};

export const checkIsEmailAvailable = async (email) => {
	const result = await db.query(
		`SELECT * FROM Account
		WHERE email = $1`,
		[email]
	);
	return result.rows[0] ? false : true;
};

export const checkIsUsernameAvailable = async (username) => {
	const result = await db.query(
		`SELECT * FROM Account
		WHERE username = $1`,
		[username]
	);
	return result.rows[0] ? false : true;
};

export const getAccountByEmail = async (email) => {
	const result = await db.query(
		`SELECT * FROM Account
		WHERE email = $1`,
		[email]
	);
	return result.rows[0];
};

export const addEmailConfirmationCodeToDb = async (code, id) => {
	const result = await db.query(
		`UPDATE account
		SET confirmation_code = $1
		WHERE id = $2
		RETURNING *`,
		[code, id]
	);
	return result.rows[0];
};

export const getAccountByConfirmationCode = async (code) => {
	const result = await db.query(
		`SELECT * FROM account
		WHERE confirmation_code = $1`,
		[code]
	);
	return result.rows[0];
};

export const changeConfirmationStatus = async (id) => {
	const result = await db.query(
		`UPDATE account
		SET confirmed = 'true'
		WHERE id = $1
		RETURNING *`,
		[id]
	);
	return result.rows[0];
};