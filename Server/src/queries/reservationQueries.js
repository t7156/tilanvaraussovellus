import db from '../config/db.js';

export const getReservations = async (startIndex, limit) => {
	const result = await db.query(
		`SELECT R.*,
		S.name AS space_name,
		A.username
		FROM Reservation AS R
		LEFT JOIN Space AS S ON S.id = R.space_id
		LEFT JOIN Account AS A ON R.account_id = A.id
		ORDER BY from_datetime
		offset $1
		limit $2`,
		[startIndex, limit]
	);
	return result.rows;
};

export const getReservationsBySpaceID = async (id, startIndex, limit) => {
	const result = await db.query(
		`SELECT R.*,
		S.name AS space_name,
		A.username
		FROM Reservation AS R
		LEFT JOIN Space AS S ON S.id = R.space_id
		LEFT JOIN Account AS A ON R.account_id = A.id
		WHERE R.space_id = $1
		ORDER BY from_datetime
		offset $2
		limit $3`,
		[id, startIndex, limit]
	);
	return result.rows;
};

export const getReservationByID = async (id) => {
	const result = await db.query(
		`SELECT R.*,
		S.name AS space_name,
		A.username
		FROM Reservation AS R
		LEFT JOIN Space AS S ON S.id = R.space_id
		LEFT JOIN Account AS A ON R.account_id = A.id
		WHERE R.id = $1`,
		[id]
	);
	return result.rows[0];
};

export const getReservationsByAccountID = async (id, startIndex, limit) => {
	const result = await db.query(
		`SELECT R.*,
		S.name AS space_name,
		A.username
		FROM reservation AS R
		LEFT JOIN Space AS S ON S.id = R.space_id
		LEFT JOIN Account AS A ON R.account_id = A.id
		WHERE R.account_id = $1
		ORDER BY from_datetime
		offset $2
		limit $3`,
		[id, startIndex, limit]
	);
	return result.rows;
};
export const getPastReservationsByAccountID = async (id, date, startIndex, limit) => {
	const result = await db.query(
		`SELECT R.*,
		S.name AS space_name
		FROM reservation AS R
		LEFT JOIN Space AS S ON S.id = R.space_id
		WHERE R.account_id = $1
		AND to_datetime < $2
		ORDER BY from_datetime
		offset $3
		limit $4`,
		[id, date, startIndex, limit]
	);
	return result.rows;
};

export const getFutureReservationsByAccountID = async (id, date, startIndex, limit) => {
	const result = await db.query(
		`SELECT R.*,
		S.name AS space_name
		FROM reservation AS R
		LEFT JOIN Space AS S ON S.id = R.space_id
		WHERE R.account_id = $1
		AND to_datetime > $2
		ORDER BY from_datetime
		offset $3
		limit $4`,
		[id, date, startIndex, limit]
	);
	return result.rows;
};

export const getReservationsBySpaceIdBetweenDates = async (id, from_date, to_date) => {
	const result = await db.query(
		`SELECT R.*, 
        A.username
        FROM reservation AS R
        LEFT JOIN Account AS A ON R.account_id = A.id
        WHERE R.space_id = $1
        AND ((R.from_datetime BETWEEN $2 AND $3)
        OR (R.to_datetime BETWEEN $2 AND $3))`
		,
		[id, from_date, to_date]
	);
	return result.rows;
};

export const makeReservation = async (space_id, account_id, from_datetime, to_datetime) => {
	const result = await db.query(
		`INSERT INTO reservation 
		(creation_time,
		space_id,
		account_id,
		from_datetime,
		to_datetime)
		VALUES (CURRENT_TIMESTAMP(0), $1, $2, $3, $4)
		RETURNING *`,
		[space_id, account_id, from_datetime, to_datetime]
	);
	return result.rows[0];
};

export const deleteReservation = async (id) => {
	const result = await db.query(
		`DELETE FROM reservation
		WHERE id = $1
		RETURNING *`,
		[id]
	);
	return result.rows[0];
};

export const editReservation = async (id, account_id, from_datetime, to_datetime) => {
	const result = await db.query(
		`UPDATE reservation 
		SET
		account_id = $1,
		from_datetime = $2,
		to_datetime = $3
		WHERE id = $4
		RETURNING *`,
		[account_id, from_datetime, to_datetime, id]
	);
	return result.rows[0];
};

export const isTimeslotFree = async (from_datetime, to_datetime, space_id) => {
	const result = await db.query(
		`SELECT * FROM reservation
		WHERE (from_datetime <= $1
			AND to_datetime > $1
			AND space_id = $3)
		OR (from_datetime < $2
			AND to_datetime >= $2
			AND space_id = $3)
		OR (to_datetime > $1
			AND to_datetime <= $2
			AND space_id = $3)
		OR (from_datetime < $2
			AND from_datetime >= $1
			AND space_id = $3)`,
		[from_datetime, to_datetime, space_id]
	);
	return result.rows[0] ? false : true;
};
