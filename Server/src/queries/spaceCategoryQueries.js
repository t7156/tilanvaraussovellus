import db from '../config/db.js';

export const getSpaceCategories = async () => {
	const result = await db.query(
		'SELECT * FROM space_category'
	);
	return result.rows;
};

export const addSpaceCategoryToDb = async (space_id, category_id) => {
	const result = await db.query(
		`INSERT INTO space_category
		(space_id, category_id)
		VALUES ($1, $2)
		RETURNING *`,
		[space_id, category_id]
	);
	return result.rows[0];
};

export const deleteSpaceCategories = async (space_id) => {
	const result = await db.query(
		`DELETE FROM Space_category
		WHERE space_id = $1
		RETURNING *`,
		[space_id]
	);
	return result.rows[0];
};