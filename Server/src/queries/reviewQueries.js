import db from '../config/db.js';

export const getReviewsBySpaceID = async (id, startIndex, limit) => {
	const result = await db.query(
		`SELECT R.*,
		A.username
        FROM Review AS R
		LEFT JOIN Account AS A ON R.account_id = A.id
        WHERE space_id = $1
		ORDER BY id
		offset $2
		limit $3`,
		[id, startIndex, limit]
	);
	return result.rows;
};

export const getSpaceReviewsStarsBySpaceID = async (id) => {
	const result = await db.query(
		`SELECT R.stars
        FROM Review AS R
        WHERE space_id = $1`,
		[id]
	);
	return result.rows;
};

export const getReviewBySpaceIDByAccountID = async (space_id, account_id) => {
	const result = await db.query(
		`SELECT R.*
        FROM Review AS R
        WHERE space_id = $1
		AND account_id = $2`,
		[space_id, account_id]
	);
	return result.rows[0];
};

export const accountHasReviewedSpace = async (space_id, account_id) => {
	const result = await db.query(
		`SELECT * 
        FROM Review
        WHERE space_id = $1 
		AND account_id = $2`,
		[space_id, account_id]
	);
	return result.rows[0] ? true : false;
};

export const getReviewByID = async (id) => {
	const result = await db.query(
		`SELECT * 
        FROM Review
        WHERE id = $1`,
		[id]
	);
	return result.rows[0];
};

export const getReviews = async (startIndex, limit) => {
	const result = await db.query(
		`SELECT * FROM review
		ORDER BY id
		offset $1
		limit $2`,
		[startIndex, limit]
	);
	return result.rows;
};

export const getReviewsByAccountID = async (id, startIndex, limit) => {
	const result = await db.query(
		`SELECT * 
        FROM Review
        WHERE account_id = $1
		ORDER BY id
		offset $2
		limit $3`,
		[id, startIndex, limit]
	);
	return result.rows;
};

export const addReview = async (stars, comment, space_id, account_id) => {
	const result = await db.query(
		`INSERT INTO review 
		(stars, comment, space_id, account_id)
		VALUES ($1, $2, $3, $4)
		RETURNING *`,
		[stars, comment, space_id, account_id]
	);
	return result.rows[0];
};

export const editReview = async (stars, comment, review_id) => {
	const result = await db.query(
		`UPDATE review
		SET stars = $1,
		comment = $2
		WHERE id = $3
		RETURNING *`,
		[stars, comment, review_id]
	);
	return result.rows[0];
};

export const isReviewAccessible = async (review_id, account_id) => {
	const result = await db.query(
		`SELECT * FROM review
		WHERE id = $1
		AND account_id = $2`,
		[review_id, account_id]
	);
	return result.rows[0] ? true : false;
};

export const reviewDoesExist = async (id) => {
	const result = await db.query(
		`SELECT * FROM review
		WHERE id = $1`,
		[id]
	);
	return result.rows[0] ? true : false;
};

export const deleteReview = async (id) => {
	const result = await db.query(
		`DELETE FROM review
		WHERE id = $1
		RETURNING *`,
		[id]
	);
	return result.rows[0];
};
