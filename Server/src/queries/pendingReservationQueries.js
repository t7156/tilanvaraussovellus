import db from '../config/db.js';

export const getPendingReservations = async () => {
	const result = await db.query(
		'SELECT * FROM pending_reservation'
	);
	return result.rows;
};

export const getPendingReservationsBySpaceID = async (id) => {
	const result = await db.query(`
		SELECT *
		FROM pending_reservation
		WHERE space_id = $1`, [id],
	);
	return result.rows;
};

export const getPendingReservationByID = async (id) => {
	const result = await db.query(`
		SELECT *
		FROM pending_reservation
		WHERE id = $1`, [id],
	);
	return result.rows[0];
};

export const getPendingReservationByAccountID = async (id) => {
	const result = await db.query(
		`SELECT * FROM pending_reservation
		WHERE account_id = $1`,
		[id]
	);
	return result.rows;
};

export const makePendingReservation = async (space_id, account_id, from_datetime, to_datetime) => {
	const result = await db.query(`
		INSERT INTO pending_reservation 
		(space_id,
		account_id,
		from_datetime,
		to_datetime)
		VALUES ($1, $2, $3, $4)
		RETURNING *`,[space_id, account_id, from_datetime, to_datetime]
	);
	return result.rows[0];
};

export const deletePendingReservation = async (id) => {
	const result = await db.query(`
		DELETE FROM pending_reservation
		WHERE id = $1
		RETURNING *`,
	[id]
	);
	return result.rows[0];
};

export const editPendingReservation = async (id, account_id, from_datetime, to_datetime) => {
	const result = await db.query(
		`UPDATE pending_reservation 
		SET
		account_id = $1,
		from_datetime = $2,
		to_datetime = $3
		WHERE id = $4
		RETURNING *`,[account_id, from_datetime, to_datetime, id]
	);
	return result.rows[0];
};

export const isPendingTimeslotFree = async (from_datetime, to_datetime, space_id) => {
	const result = await db.query(`
		SELECT * FROM pending_reservation
		WHERE (from_datetime <= $1
			AND to_datetime > $1
			AND space_id = $3)
		OR (from_datetime < $2
			AND to_datetime >= $2
			AND space_id = $3)
		OR (to_datetime > $1
			AND to_datetime <= $2
			AND space_id = $3)
		OR (from_datetime < $2
			AND from_datetime >= $1
			AND space_id = $3)`,
	[from_datetime, to_datetime, space_id]
	);
	return result.rows[0] ? false : true;
};