import db from '../config/db.js';

export const getSpaces = async (startIndex, limit) => {
	const results = await db.query(
		`SELECT 
		S.id,
		S.name,
		S.address,
		S.size,
		S.description,
		S.hourly_price,
		array(
		SELECT json_build_object('id', C.id, 'category_name', category_name)
		FROM category AS C
		LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
		WHERE C.id = Sc.category_id
		) AS categories,
		array(
		SELECT json_build_object('id', R.id, 'stars', stars, 'comment', comment)
		FROM review AS R
		WHERE R.space_id = S.id
		) AS reviews,
		array(
			SELECT json_build_object('id', SI.id, 'space_id', space_id, 'image_path', image_path)
			FROM Space_image SI
			WHERE SI.space_id = S.id
		) AS image,
		A.username AS owner
		FROM space AS S
		LEFT JOIN account AS A ON A.id = S.owner_id
		ORDER BY s.id
		offset $1
		limit $2`,
		[startIndex, limit]
	);
	return results.rows;
};

export const searchSpaces = async (name, location, category, hourly_price, startIndex, limit) => {
	const result = await db.query(
		`SELECT
		S.id,
		S.name,
		S.address,
		S.size,
		S.description,
		S.hourly_price::int,
		array(
			SELECT json_build_object('id', C.id, 'category_name', category_name)
			FROM category AS C
			LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
			WHERE C.id = Sc.category_id
			) AS categories,
		array(
			SELECT json_build_object('id', R.id, 'stars', stars, 'comment', comment)
			FROM review AS R
			WHERE R.space_id = S.id
			) AS reviews,
		array(
			SELECT json_build_object('id', SI.id, 'space_id', space_id, 'image_path', image_path)
			FROM Space_image SI
			WHERE SI.space_id = S.id
			) AS image,	
		A.username AS owner
		FROM space AS S
		LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
		LEFT JOIN account AS A ON A.id = S.owner_id
		WHERE lower(S.name) LIKE lower($1)
		AND (SPLIT_PART(SPLIT_PART(S.address, ',', 1), ':', 2) LIKE $2 OR 
		SPLIT_PART(SPLIT_PART(S.address, ',', 2), ':', 2) LIKE $2 OR 
		SPLIT_PART(SPLIT_PART(S.address, ',', 3), ':', 2) LIKE $2)
		AND S.hourly_price::int <= $4
		AND Sc.category_id::text LIKE $3::text
		GROUP BY S.id, A.username
		ORDER BY S.id
		OFFSET $5
		LIMIT $6
		`,
		[`%${name}%`, `%${location}%`, category, `${hourly_price}`, startIndex, limit]
	);
	return result.rows;
};

export const getSpaceById = async (id) => {
	const result = await db.query(
		`SELECT 
		S.id,
		S.name,
		S.address,
		S.size,
		S.description,
		S.hourly_price,
		array(
		SELECT json_build_object('id', C.id, 'category_name', category_name)
		FROM category AS C
		LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
		WHERE C.id = Sc.category_id
		) AS categories,
		array(
		SELECT json_build_object('id', R.id, 'stars', stars, 'comment', comment)
		FROM review AS R
		WHERE R.space_id = S.id
		) AS reviews,
		array(
			SELECT json_build_object('id', SI.id, 'space_id', space_id, 'image_path', image_path)
			FROM Space_image SI
			WHERE SI.space_id = S.id
		) AS image,
		A.username AS owner
		FROM space AS S
		LEFT JOIN account AS A ON A.id = S.owner_id
		WHERE S.id = $1`,
		[id]
	);
	return result.rows[0];
};

export const getSpaceByName = async (spaceName) => {
	const result = await db.query(
		`SELECT 
		S.id,
		S.name,
		S.address,
		S.size,
		S.description,
		S.hourly_price,
		array(
		SELECT json_build_object('id', C.id, 'category_name', category_name)
		FROM category AS C
		LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
		WHERE C.id = Sc.category_id
		) AS categories,
		array(
		SELECT json_build_object('id', R.id, 'stars', stars, 'comment', comment)
		FROM review AS R
		WHERE R.space_id = S.id
		) AS reviews,
		array(
			SELECT json_build_object('id', SI.id, 'space_id', space_id, 'image_path', image_path)
			FROM Space_image SI
			WHERE SI.space_id = S.id
		) AS image,
		A.username AS owner
		FROM space AS S
		LEFT JOIN account AS A ON A.id = S.owner_id
		WHERE S.name = $1`,
		[spaceName]
	);
	return result.rows[0];
};

export const getSpacesByCategory = async (id, startIndex, limit) => {
	const result = await db.query(
		`SELECT 
		S.id,
		S.name,
		S.address,
		S.size,
		S.description,
		S.hourly_price,
		array(
		SELECT json_build_object('id', C.id, 'category_name', category_name)
		FROM category AS C
		LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
		WHERE C.id = Sc.category_id
		) AS categories,
		array(
		SELECT json_build_object('id', R.id, 'stars', stars, 'comment', comment)
		FROM review AS R
		WHERE R.space_id = S.id
		) AS reviews,
		array(
			SELECT json_build_object('id', SI.id, 'space_id', space_id, 'image_path', image_path)
			FROM Space_image SI
			WHERE SI.space_id = S.id
		) AS image,
		A.username AS owner
		FROM space AS S
		LEFT JOIN account AS A ON A.id = S.owner_id
		LEFT JOIN space_category AS Sc ON Sc.space_id = S.id  
		WHERE S.id = Sc.space_id AND Sc.category_id = $1
		ORDER BY s.id
		offset $2
		limit $3`,
		[id, startIndex, limit]
	);
	return result.rows;
};

export const getSpacesByAccountID = async (id, startIndex, limit) => {
	const result = await db.query(
		`SELECT 
		S.id,
		S.name,
		S.address,
		S.size,
		S.description,
		S.hourly_price,
		array(
			SELECT json_build_object('id', C.id, 'category_name', category_name)
			FROM category AS C
			LEFT JOIN space_category AS Sc ON Sc.space_id = S.id 
			WHERE C.id = Sc.category_id
		) AS categories,
		array(
			SELECT json_build_object('id', R.id, 'stars', stars, 'comment', comment)
			FROM review AS R
			WHERE R.space_id = S.id
		) AS reviews,
		array(
			SELECT json_build_object('id', SI.id, 'space_id', space_id, 'image_path', image_path)
			FROM Space_image SI
			WHERE SI.space_id = S.id
		) AS image,
		A.username AS owner
		FROM space AS S
		LEFT JOIN account AS A ON A.id = $1
		WHERE S.owner_id = $1
		ORDER BY s.id
		offset $2
		limit $3`,
		[id, startIndex, limit]
	);
	return result.rows;
};

export const addSpaceToDb = async (name, address, size, description, hourly_price, owner_id) => {
	const result = await db.query(
		`INSERT INTO space
		(name, address, size, description, hourly_price, owner_id) 
			VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
		[name, address, size, description, hourly_price, owner_id]
	);
	return result.rows[0];
};

export const editSpace = async (name, address, size, description, hourly_price, id) => {
	const result = await db.query(
		`UPDATE Space
		SET 
		name = $1,
		address = $2, 
		size = $3, 
		description = $4, 
		hourly_price = $5
		WHERE id = $6
		RETURNING *`,
		[name, address, size, description, hourly_price, id]
	);
	return result.rows[0];
};

export const deleteSpace = async (id) => {
	const deletedImages= { images: [] };
	await db
		.query('BEGIN')
		.then(() => {
			return db.query(
				`DELETE FROM Reservation
				WHERE space_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Pending_reservation
					WHERE space_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Review
				WHERE space_id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Space_category
				WHERE space_id = $1`,
				[id]
			);
		})
		.then(() => {
			const deleteImages = async (id) => {
				deletedImages.images = await db.query(
					`DELETE FROM Space_image
					WHERE space_id = $1
					RETURNING *`,
					[id]
				);
			};
			return deleteImages(id);
		})
		.then(() => {
			return db.query(
				`DELETE FROM Space
				WHERE id = $1`,
				[id]
			);
		})
		.then(() => {
			return db.query('COMMIT');
		});
	return deletedImages.images.rows;
};

export const isSpaceAccessible = async (space_id, account_id) => {
	const result = await db.query(
		`SELECT * FROM Space
		WHERE id = $1
		AND owner_id = $2`,
		[space_id, account_id]
	);
	return result.rows[0] ? true : false;
};

export const spaceDoesExist = async (id) => {
	const result = await db.query(
		`SELECT * FROM Space
		WHERE id = $1`,
		[id]
	);
	return result.rows[0] ? true : false;
};

export const addSpaceImageToDB = async (space_id, image) => {
	const result = await db.query(
		`INSERT INTO Space_Image
		(space_id, image_path)
		VALUES ($1, $2)
		RETURNING *`,
		[space_id, image]
	);
	return result.rows[0];
};
export const deleteSpaceImagesFromDB = async (space_id) => {
	const result = await db.query(
		`DELETE FROM Space_image
		WHERE space_id = $1
		RETURNING *`,
		[space_id]
	);
	return result.rows;
};
export const getSpaceImageBySpaceId = async (space_id) => {
	const result = await db.query(
		`SELECT * FROM Space_Image
		WHERE space_id = $1`,
		[space_id]
	);
	return result.rows[0];
};
