import axios from 'axios';

export const verifyAuthentication = (token) => {
	const headers = {
		authorization: `Bearer ${token}`,
	};
	return axios
		.get('http://localhost:3001/auth/verify_authentication', {
			headers,
		})
		.then(() => {
			return true;
		})
		.catch(() => {
			return false;
		});
};
