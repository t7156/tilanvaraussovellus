import axios from 'axios';

export const axiosAuth = axios.create({});

const addTokenToRequest = (config) => {
	const token = window.localStorage.getItem('token');
	if (token !== null) {
		config.headers['Authorization'] = `Bearer ${token}`;
	}
	return config;
};

export const setAxiosAuthInterceptors = (logout) => {
	axiosAuth.interceptors.request.use((config) => {
		return addTokenToRequest(config);
	},
	(error) => {
		return Promise.reject(error);
	});

	axiosAuth.interceptors.response.use(
		// Do not handle successful responses
		undefined,
		(error) => {
			if (error.response.status === 401 && error.response.data.msg === 'Invalid token') {
				logout();
			}
			return Promise.reject(error);
		}
	);
};
