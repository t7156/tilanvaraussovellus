import React, { useContext } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import UserContext from './UserContext';
import PropTypes from 'prop-types';

const RequireAuth = ({ children }) => {
	const { authState } = useContext(UserContext);
	const location = useLocation();

	return authState.isLoggedIn === true 
		? children 
		: <Navigate 
			to='/login' 
			replace 
			state={{ path: location.pathname }}
		/>;
};

RequireAuth.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.array, 
		PropTypes.object
	]),
};

export default RequireAuth;
