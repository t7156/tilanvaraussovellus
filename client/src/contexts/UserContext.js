import React from 'react';
import { createContext } from 'react';
import { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { verifyAuthentication } from '../utils/auth/verifyAuthentication';
import Loading from '../components/Loading';

const UserContext = createContext({
	authState: {},
	userState: {},
	setUserState: () => null,
	handleLogin: () => null,
	logout: () => null,
});

export const UserProvider = (props) => {
	const { children } = props;
	const [loading, setLoading] = useState(true);
	const [userState, setUserState] = useState({});
	const [authState, setAuthState] = useState({});
	const [isAuth, setIsAuth] = useState(localStorage.getItem('token') ? true : false);

	const logout = () => {
		localStorage.removeItem('token');
		setAuthState({});
		setUserState({});
	};

	const getAccount = async () => {
		const token = localStorage.getItem('token');
		const account = { info: null };
		if (token) {
			const headers = {
				authorization: `Bearer ${token}`,
			};
			const response = await axios
				.get('http://localhost:3001/account', { headers })
				.catch((e) => {
					console.log(e);
				});
			account.info = response.data.data;
		}
		return account.info;
	};

	useEffect(() => {
		const handleUserAndAuth = async () => {
			const token = localStorage.getItem('token');
			if (isAuth) {
				const isVerified = await verifyAuthentication(token);
				if (isVerified) {
					const account = await getAccount();
					setAuthState({
						isLoggedIn: !!token,
						role: account.role,
						token: `Bearer ${token}`,
					});
					setUserState({
						id: account.id,
						username: account.username,
						email: account.email,
					});
				} else {
					logout();
				}
			} else {
				logout();
			}
			setLoading(false);
		};
		handleUserAndAuth();
	}, []);

	const handleLogin = (responseData) => {
		const tokenFromLoginResponse = responseData.token;
		if (tokenFromLoginResponse) {
			localStorage.setItem('token', tokenFromLoginResponse.split(' ')[1]);
			try {
				setIsAuth(true);
				setAuthState({
					isLoggedIn: !!tokenFromLoginResponse,
					role: responseData.role,
					token: tokenFromLoginResponse,
				});
				setUserState({
					id: responseData.id,
					username: responseData.username,
					email: responseData.email,
				});
			} catch (e) {
				logout();
			}
		}
	};
	const valuesToProvide = useMemo(
		() => ({
			authState,
			userState,
			setUserState,
			handleLogin,
			logout,
		}),
		[authState, userState, setUserState, handleLogin, logout]
	);

	return (
		<UserContext.Provider value={valuesToProvide}>
			{loading ? <Loading /> : children}
		</UserContext.Provider>
	);
};

UserProvider.propTypes = {
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

export default UserContext;
