import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Box, Stack, TextField } from '@mui/material';
import ButtonSecondaryLarge from './ui/ButtonSecondaryLarge';

const register = async (username, email, password) => {
	return await axios.post('http://localhost:3001/auth/register', {
		username,
		email,
		password,
	});
};

const RegisterForm = () => {
	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');

	const navigate = useNavigate();

	const validateOnSubmit = () => {
		// TODO: same validation rules as on backend
		if (username && email && password && confirmPassword) {
			if (password === confirmPassword) {
				return true;
			} else {
				alert('Salasanat eivät täsmää.');
				return false;
			}
		} else {
			alert('Täytä kaikki kentät.');
			return false;
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateOnSubmit()) {
			register(username, email, password)
				.then((res) => {
					if (res.status === 201) {
						alert('Rekisteröityminen onnistui! Vahvista seuraavaksi sähköpostiosoitteesi.');
						navigate('/verify-email');
					}
				})
				.catch((e) => {
					if (
						e.response.status === 409 &&
						e.response.data.msg === 'Email already taken'
					) {
						alert('Tällä sähköpostiosoitteella on jo luotu käyttäjätili.');
					} else if (
						e.response.status === 409 &&
						e.response.data.msg === 'Username already taken'
					) {
						alert('Käyttäjänimi on jo käytössä. Valitse toinen käyttäjänimi.');
					} else {
						alert('Rekisteröityminen ei onnistunut. Yritä uudelleen.');
					}
				});
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<Stack spacing={3}>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="register-name"
					label="Nimi"
					value={username}
					onChange={(e) => setUsername(e.target.value)}
				/>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="register-email"
					label="Email"
					type="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="register-password"
					label="Salasana"
					type="password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="register-confirm-password"
					label="Vahvista salasana"
					type="password"
					value={confirmPassword}
					onChange={(e) => setConfirmPassword(e.target.value)}
				/>
				<Box pt={{ xs: 1, sm: 2, md: 3 }}>
					<ButtonSecondaryLarge
						type="submit"
						text="Luo tili"
					/>
				</Box>
			</Stack>
		</form>
	);
};
export default RegisterForm;
