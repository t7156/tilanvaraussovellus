import {
	Box,
	Container,
	OutlinedInput,
	Typography,
	FormControl,
	MenuItem,
	Grid,
	Card,
	Select,
	InputLabel,
	Radio,
	RadioGroup,
	FormControlLabel,
	Slider,
	Stack,
	TextField
} from '@mui/material';
import { useState, useEffect } from 'react';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import DateAdapter from '@mui/lab/AdapterDateFns';
import PropTypes from 'prop-types';
import ActionButton from './ui/ActionButton';
import axios from 'axios';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

function valuetext(price) {
	return `${price}°€`;
}

const SearchBar = ({ setAddress }) => {
	const currentDate = new Date();
	const [categories, setCategories] = useState('');
	const [selectedSearch, setSelectedSearch] = useState();
	const [category, setCategory] = useState('');
	const [searchCategory, setSearchCategory] = useState('');
	const [categoryId, setCategoryId] = useState('');
	const [price, setPrice] = useState(0);
	const [searchTerm, setSearchTerm] = useState('');
	const [startDate, setStartDate] = useState();
	const [endDate, setEndDate] = useState();

	useEffect(() => {
		const getData = async () => {
			await axios
				.get('http://localhost:3001/category')
				.then((res) => {
					setCategories(res.data.data);
				})
				.catch(() => {
					alert('Ongelma kategorioiden noudossa.');
				});
		};
		getData();
	}, []);

	const submitSearch = () => {
		const parameters = {
			name: selectedSearch === 'Nimi' ? searchTerm : '',
			location: selectedSearch === 'Sijainti' ? searchTerm : '',
			hourly_price: price,
			category: categoryId,
			from_date: startDate !== undefined ? startDate : '',
			to_date: endDate !== undefined ? endDate : '',
		};
		setAddress(
			`http://localhost:3001/space/search?name=${parameters.name}&location=${parameters.location}&hourly_price=${parameters.hourly_price}&category=${parameters.category}&from_date=${parameters.from_date}&to_date=${parameters.to_date}&`
		);
	};

	const handleSearchChange = (event) => {
		setSelectedSearch(event.target.value);
	};

	const handlePriceChange = (event, newValue) => {
		setPrice(newValue);
	};

	const handleChange = (event) => {
		setCategory(event.target.value);
	};

	const handleClick = (category) => {
		if (searchCategory === category.category_name) {
			setSearchCategory('');
			setCategoryId('');
		} else {
			setSearchCategory(category.category_name);
			setCategoryId(category.id);
		}
	};

	return (
		<Container
			fixed
			sx={{
				minWidth: '100%',
				minHeight: '200px',
				paddingTop: '20px',
				paddingBottom: '20px',
				display: 'inline-block',
				justifyContent: 'space-between',
			}}
		>
			<Box>
				<OutlinedInput
					placeholder="Hakutermi"
					value={searchTerm}
					onChange={(e) => setSearchTerm(e.target.value)}
					sx={{
						minWidth: '500px',
						marginTop: '10px',
						marginBottom: '10px',
					}}
				></OutlinedInput>
			</Box>
			<Box
				sx={{
					paddingTop: '10px',
					paddingBottom: '20px',
				}}
			>
				<RadioGroup row>
					<FormControlLabel
						value="Nimi"
						label="Nimi"
						control={
							<Radio
								checked={selectedSearch === 'Nimi'}
								onChange={handleSearchChange}
								value="Nimi"
							/>
						}
					/>
					<FormControlLabel
						value="Sijainti"
						label="Sijainti"
						control={
							<Radio
								checked={selectedSearch === 'Sijainti'}
								onChange={handleSearchChange}
								value="Sijainti"
							/>
						}
					/>
				</RadioGroup>
			</Box>
			<Typography
				variant="h5"
				sx={{
					fontSize: '20px',
				}}
			>
				Hinta
			</Typography>
			<Box
				sx={{
					width: 300,
					marginTop: '20px',
					paddingLeft: '10px',
				}}
			>
				<Slider
					value={price}
					max={200}
					onChange={handlePriceChange}
					valueLabelDisplay="auto"
					getAriaValueText={valuetext}
				/>
				<Card
					sx={{
						minWidth: '100px',
						maxWidth: '100px',
						minHeight: '30px',
						maxHeight: '30px',
						marginTop: '20px',
						marginBottom: '20px',
						marginLeft: '-10px',
					}}
				>
					<Typography
						variant="p"
						sx={{
							display: 'flex',
							justifyContent: 'center',
						}}
					>
						0€ - {price}€
					</Typography>
				</Card>
			</Box>
			<Box
				sx={{
					minWidth: '400px',
					maxWidth: '400px',
					marginTop: '20px',
					marginBottom: '20px',
				}}
			>
				<FormControl fullWidth>
					<InputLabel>Kategoria</InputLabel>
					<Select value={category} label="Kategoria" onChange={handleChange}>
						{categories &&
							categories.map((category) => (
								<MenuItem
									key={uuidv4()}
									value={category.category_name}
									onClick={() => handleClick(category)}
								>
									<Typography variant="p">{category.category_name}</Typography>
								</MenuItem>
							))}
					</Select>
					<Grid
						container
						spacing={3}
						sx={{
							marginTop: '10px',
							marginBottom: '20px',
						}}
					>
						{searchCategory !== '' ? (
							<Card
								sx={{
									maxHeight: '40px',
									minHeight: '40px',
									maxWidth: '100px',
									minWidth: '100px',
									marginTop: '20px',
									marginLeft: '25px',
									display: 'inline-flex',
									justifyContent: 'center',
								}}
							>
								<Typography
									variant="h5"
									sx={{
										marginTop: '10px',
										fontSize: '16px',
									}}
								>
									{searchCategory}
								</Typography>
							</Card>
						) : (
							<Typography
								variant="h5"
								sx={{
									fontSize: '20px',
									marginTop: '20px',
									marginBottom: '20px',
									marginLeft: '25px',
								}}
							>
								Ei kategorioita valittu.
							</Typography>
						)}
						<LocalizationProvider dateAdapter={DateAdapter}> 
							<Stack 
								direction='row' 
								spacing={3}
								sx={{
									marginTop: '20px',
									marginBottom: '20px',
									marginLeft: '25px'
								}}
							>
								<DesktopDatePicker
									label="Vapaana alkaen"
									value={startDate}
									minDate={currentDate}
									onChange={(newValue)=>
										setStartDate(`${newValue.getFullYear()}-${('0' + (newValue.getMonth() + 1)).slice(-2)}-${('0' + (newValue.getDate())).slice(-2)}`)}
									inputFormat="yyyy/MM/dd"
									mask="____/__/__"
									renderInput={(params) => <TextField {...params} />}
								/>
								<DesktopDatePicker
									label="Vapaana päättyen"
									value={endDate}
									minDate={currentDate}
									onChange={(newValue)=>
										setEndDate(`${newValue.getFullYear()}-${('0' + (newValue.getMonth() + 1)).slice(-2)}-${('0' + (newValue.getDate())).slice(-2)}`)}
									inputFormat="yyyy/MM/dd"
									mask="____/__/__"
									renderInput={(params) => <TextField {...params} />}
								/>
							</Stack>
						</LocalizationProvider>
					</Grid>
				</FormControl>
			</Box>
			<ActionButton
				text="Hae"
				onClickFunction={() => {
					submitSearch();
				}}
			/>
		</Container>
	);
};

SearchBar.propTypes = {
	setListedSpaces: PropTypes.func,
	setAddress: PropTypes.func,
};

export default SearchBar;