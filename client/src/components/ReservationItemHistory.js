import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@mui/material';

const formatTimeString = (ISOTime) => {
	const [date, time] = ISOTime.split('T');
	const [yyyy, MM, dd] = date.split('-');
	const [hh, mm] = time.split(':');
 
	return `${dd}.${MM}.${yyyy} ${hh}:${mm}`;
};

const ReservationItemHistory = ({ reservation }) => {
	return (
		<>
			<Grid item md={4}>
				<Typography>{reservation.space_name}</Typography>
			</Grid>
			<Grid item md={4}>
				<Typography>{formatTimeString(reservation.from_datetime)}</Typography>
			</Grid>
			<Grid item md={4}>
				<Typography>{formatTimeString(reservation.to_datetime)}</Typography>
			</Grid>
		</>
	);
};

ReservationItemHistory.propTypes = {
	reservation: PropTypes.object,
};

export default ReservationItemHistory;