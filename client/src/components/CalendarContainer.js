import React, { useEffect, useState, useRef, useContext } from 'react';
import { useTheme } from '@mui/system';
import { Box } from '@mui/material';
import FullCalendar from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import fiLocale from '@fullcalendar/core/locales/fi';
import axios from 'axios';
import PropTypes from 'prop-types';
import ReservationForm from './ReservationForm';
import UserContext from '../contexts/UserContext';
import { v4 as uuidv4 } from 'uuid';
import { validateInt } from '../utils/validation';
import { useNavigate } from 'react-router-dom';

const getReservations = (spaceId) => {
	return axios.get(`http://localhost:3001/reservation/space/${spaceId}?limit=9999999&page=1`);
};

const CalendarContainer = ({ space }) => {
	const [reservations, setReservations] = useState();
	const [reservationStart, setReservationStart] = useState();
	const [reservationEnd, setReservationEnd] = useState();
	const [selectionId, setSelectionId] = useState();
	const calendarRef = useRef();
	const theme = useTheme();
	const { userState } = useContext(UserContext);
	const navigate = useNavigate();

	const parseCalendarEvents = (reservations, userState, space) => {
		const events = reservations.map((reservation) => {
			return userState.username === reservation.username
				? {
					title: 'Oma varaus',
					start: reservation.from_datetime,
					end: reservation.to_datetime,
					color: theme.palette.calendar.light,
				  }
				: userState.username === space.owner
					? {
						title: reservation.username,
						start: reservation.from_datetime,
						end: reservation.to_datetime,
						color: theme.palette.calendar.main,
				  }
					: {
						title: 'Varattu',
						start: reservation.from_datetime,
						end: reservation.to_datetime,
						color: theme.palette.calendar.main,
				  };
		});
		return events;
	};

	useEffect(() => {
		if (validateInt(space.id)) {
			getReservations(space.id)
				.then((response) => {
					if (response.data.data) {
						setReservations(parseCalendarEvents(response.data.data, userState, space));
					} else {
						alert('Varaustietoja ei voitu hakea, päivitä sivu ja yritä uudelleen.');
					}
				})
				.catch((error) => {
					if (error.response.status === 404) {
						setReservations([]);
					} else {
						alert('Varaustietoja ei voitu hakea, päivitä sivu ja yritä uudelleen.');
					}
				});
		} else {
			navigate('/notfound');
		}
	}, []);

	const handleSelect = (start, end) => {
		setReservationStart(start);
		setReservationEnd(end);
		const calendarApi = calendarRef.current.getApi();
		const id = uuidv4();
		calendarApi.addEvent({
			id, 
			start, 
			end,
			display: 'background',
			backgroundColor: theme.palette.calendar.selection,
		});
		setSelectionId(id);
	};

	const unselect = () => {
		const calendarApi = calendarRef.current.getApi();
		const event = calendarApi.getEventById(selectionId);
		event.remove();
		setSelectionId();
	};

	const clearSelection = () => {
		setReservationStart();
		setReservationEnd();
	};

	return (
		<Box sx={{ backgroundColor : 'white', borderRadius : '10px' }} p={3}>
			<FullCalendar
				ref={calendarRef}
				height={'70vh'}
				locale={fiLocale}
				plugins={[ interactionPlugin, timeGridPlugin ]}
				initialView="timeGridWeek"
				selectable={true}
				selectOverlap={false}
				slotDuration='01:00:00'
				select={(info) => handleSelect(info.start, info.end)}
				unselect={unselect}
				validRange={{
					start: new Date(),
				}}
				headerToolbar={{
					start:'today prev,next',
					center: 'title',
					end: 'timeGridWeek,timeGridDay'
				}}
				events={reservations}/>
			<Box mt={2} display='flex' justifyContent='center'>
				<ReservationForm
					calendarRef={calendarRef}
					spaceName={space.name}
					spaceId={space.id} 
					hourlyPrice={Number(space.hourly_price)}
					reservationStart={reservationStart}
					reservationEnd={reservationEnd}
					clearSelection={clearSelection}
				/>
			</Box>
		</Box>
	);
};

CalendarContainer.propTypes = {
	space: PropTypes.object,
};

export default CalendarContainer;