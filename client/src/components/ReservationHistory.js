import React from 'react';
import { Typography } from '@mui/material';
import { Grid } from '@mui/material';
import { Divider } from '@mui/material';
import ReservationItemHistory from './ReservationItemHistory';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

const ReservationHistory = ({ reservations }) => {
	return (
		<Grid container spacing={2}>
			<Grid item md={12}>
				<Divider sx={{ paddingTop: '50px', marginBottom: '50px' }}>
					<Typography variant="h5">Käyttäjän vanhat varaukset</Typography>
				</Divider>
			</Grid>
			<Grid item md={2}></Grid>
			<Grid item md={8}>
				{reservations.length > 0 ? (
					<Grid container spacing={2} justifyContent="flex-end">
						<Grid item md={4}>
							<Typography>Tila</Typography>
						</Grid>
						<Grid item md={4}>
							<Typography>Alkaen</Typography>
						</Grid>
						<Grid item md={4}>
							<Typography>Päättyen</Typography>
						</Grid>
						 {reservations.map((reservation) => {
							return (
								<ReservationItemHistory key={uuidv4()} reservation={reservation} />
							);
						})} 
					</Grid>
				 ) : (
					<Typography>Ei varauksia.</Typography>
				)} 
			</Grid>
			<Grid item md={2}></Grid>
		</Grid>
	);
};

ReservationHistory.propTypes = {
	reservations: PropTypes.array,
};

export default ReservationHistory;
