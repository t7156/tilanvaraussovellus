import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Paper, Typography } from '@mui/material';
import { v4 as uuidv4 } from 'uuid';
import LocationOnIcon from '@mui/icons-material/LocationOn';

const SpaceDetails = ({ space }) => {
	const style = {
		display: 'inline-block',
		margin: 1,
		paddingTop: 0.5,
		paddingBottom: 0.5,
		paddingLeft: 1,
		paddingRight: 1,
	};
	const streetName = (JSON.parse(space.address).street);
	const postalNumber = (JSON.parse(space.address).postal);
	const cityName = (JSON.parse(space.address).city);
	
	return (
		<Grid container spacing={1}>
			<Grid item xs={12}>
				{space.categories.map(categ => {
					return (
						<Paper key={uuidv4()} sx={style}>
							<Typography>
								{categ.category_name}
							</Typography>
						</Paper>
					);
				})}
			</Grid>
			<Grid item xs={12}>
				<Paper sx={style}>
					<Typography>
						<strong>Hinta</strong> {space.hourly_price} €/h
					</Typography>
				</Paper>
				<Paper sx={style}>
					<Typography>
						<strong>Koko</strong> {space.size} m<sup>2</sup>
					</Typography>
				</Paper>
			</Grid>
			<Grid item xs={1}>
				<LocationOnIcon /> 
			</Grid>
			<Grid item xs={11}>
				<Typography>
					<strong>{streetName} <br />
						{postalNumber} {cityName}</strong>
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Typography m={1}>
					{space.description}
				</Typography>
			</Grid>
		</Grid>
	);
};

export default SpaceDetails;

SpaceDetails.propTypes = {
	space: PropTypes.object,
};