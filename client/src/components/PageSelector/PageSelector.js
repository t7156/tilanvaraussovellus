import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { Grid, Button, Typography } from '@mui/material';
import axios from 'axios';
import { axiosAuth } from '../../utils/axiosUtils';
import { validateInt } from '../../utils/validation';
import { useNavigate } from 'react-router-dom';

const PageSelector = ({ limit, setData, address, auth, update }) => {
	const [page, setPage] = useState(1);
	const addressRef = useRef();
	const navigate = useNavigate();

	const getRequest = (auth, address, limit, page) => {
		return (validateInt(limit) && validateInt(page)) 
			? auth
				? axiosAuth.get(`${address}limit=${limit}&page=${page}`)
				: axios.get(`${address}limit=${limit}&page=${page}`)
			: navigate('/notfound');
	};

	const handlePageChange = (value, page, setPage) => {
		const newPage = value < 0 && page === 1 ? page : page + value;
		page + value > 0
			? getRequest(auth, address, limit, newPage)
				.then((res) => {
					const data = { data: res.data.data };
					if (data.data[0]) {
						setPage(newPage);
						setData(data.data);
					} else alert('Tuloksia ei löytynyt');
				})
				.catch(() => {
					alert('Tuloksia ei löytynyt');
				})
			: null;
	};

	useEffect(() => {
		const getData = () => {
			const p = { number: page };
			if (addressRef.current !== address) {
				p.number = 1;
				addressRef.current = address;
				setPage(1);
			}
			getRequest(auth, address, limit, page)
				.then((res) => {
					const data = { data: res.data.data };
					setData(data.data);
				})
				.catch(() => {
					setData([]);
				});
		};
		getData();
	}, [address, update]);

	return (
		<Grid container my={'25px'} justifyContent={'center'} flexDirection={'row'}>
			<Button
				onClick={() => {
					handlePageChange(-1, page, setPage);
				}}
			>
				<ArrowBackIosNewIcon sx={{ fontSize: '30px' }} />
			</Button>
			<Typography sx={{ fontSize: '20px', marginTop: '5px' }}>{page}</Typography>
			<Button
				onClick={() => {
					handlePageChange(1, page, setPage);
				}}
			>
				<ArrowForwardIosIcon sx={{ fontSize: '30px' }} />
			</Button>
		</Grid>
	);
};

PageSelector.propTypes = {
	limit: PropTypes.number,
	setData: PropTypes.func,
	address: PropTypes.string,
	auth: PropTypes.bool,
	update: PropTypes.bool,
};

export default PageSelector;
