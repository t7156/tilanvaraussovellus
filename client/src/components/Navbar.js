import React, { useContext, useEffect } from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Box, Stack, Button, Container, Menu, MenuItem } from '@mui/material';
import NavigationLink from './ui/NavigationLink';
import UserContext from '../contexts/UserContext';
import { setAxiosAuthInterceptors } from '../utils/axiosUtils';

const Navbar = () => {
	const [opened, setOpened] = useState(null);
	const open = Boolean(opened);

	const { authState, logout } = useContext(UserContext);

	const handleOpen = (event) => {
		setOpened(event.currentTarget);
	};
	const handleClose = () => {
		setOpened(null);
	};

	useEffect(() => {
		setAxiosAuthInterceptors(logout);
	}, []);

	return (
		<Container
			fixed
			disableGutters={true}
			sx={{
				minWidth: '100%',
				maxWidth: '100%',
				minHeight: '75px',
				paddingTop: '15px',
				bgcolor: '#323232',
				display: 'inline-block',
				justifyContent: 'space-between',
			}}
		>
			<Container
				sx={{
					maxWidth: '100%',
					display: 'flex',
					justifyContent: 'space-between',
				}}
			>
				<Stack
					spacing={2}
					direction="row"
					sx={{
						marginLeft: '30px',
						maxWidth: '500px',
					}}
				>
					<NavigationLink
						to="/" 
					 	text="Etusivu"
					 />
					<NavigationLink
						to="/spaces" 
					 	text="Tilat"
					 />
					{ authState.isLoggedIn &&
						<NavigationLink
							to="/addnewspace" 
							text="Luo tila"
						/>}
				</Stack>
				<Stack
					direction="row-reverse"
					sx={{
						maxWidth: '100px',
					}}
				>
					<Button
						id="positioned-button"
						aria-controls={open ? 'positioned-menu' : undefined}
						aria-haspopup="true"
						aria-expanded={open ? 'true' : undefined}
						onClick={handleOpen}
					>
						<AccountCircleIcon
							sx={{
								color: 'white',
								fontSize: '42px',
							}}
						/>
					</Button>
					<Menu
						id="positioned-menu"
						aria-labelledby="positioned-button"
						anchorEl={opened}
						open={open}
						onClose={handleClose}
						anchorOrigin={{
							vertical: 'bottom',
							horizontal: 'left',
						}}
						transformOrigin={{
							vertical: 'top',
							horizontal: 'left',
						}}
					>
						{ authState.isLoggedIn 
							? <Box>
								<Link to="/profilepage">
									<MenuItem onClick={handleClose}>Profiili</MenuItem>
								</Link>
								<Link to="/login">
									<MenuItem onClick={logout}>Kirjaudu ulos</MenuItem>
								</Link>
							</Box>
							: <Box>
								<Link to="/login">
									<MenuItem onClick={handleClose}>Kirjaudu sisään</MenuItem>
								</Link>
												
								<Link to="/register">
									<MenuItem onClick={handleClose}>Rekisteröidy</MenuItem>
								</Link>
							</Box>}
					</Menu>
				</Stack>
			</Container>
		</Container>
	);
};

export default Navbar;
