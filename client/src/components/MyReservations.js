import React from 'react';
import { Typography } from '@mui/material';
import { Grid } from '@mui/material';
import { Divider } from '@mui/material';
import PropTypes from 'prop-types';
import ReservationItem from './ReservationItem';
import { v4 as uuidv4 } from 'uuid';
import { axiosAuth } from '../utils/axiosUtils';
import { validateInt } from '../utils/validation';
import { useNavigate } from 'react-router-dom';

const MyReservations = ({ reservations, setReservations }) => {
	const navigate = useNavigate();

	const deleteReservation = (reservationId) => {
		if (validateInt(reservationId)) {
			axiosAuth
				.delete(`http://localhost:3001/reservation/${reservationId}`)
				.then(() => {
					alert('Varauksen poistaminen onnistui.');
					setReservations(() => {
						return reservations?.filter((element) => element.id !== reservationId);
					});
				})
				.catch(() => {
					alert('Varauksen poistaminen ei onnistunut.');
				});
		} else {
			navigate('/notfound');
		}
	};
	return (
		<Grid container spacing={2}>
			<Grid item md={12}>
				<Divider sx={{ paddingTop: '50px', paddingBottom: '50px' }}>
					<Typography variant="h5">Käyttäjän tekemät varaukset</Typography>
				</Divider>
			</Grid>
			<Grid item md={2}></Grid>
			<Grid item md={8}>
				{reservations.length > 0 ? (
					<Grid container spacing={2} justifyContent="flex-end">
						<Grid item md={3}>
							<Typography>Tila</Typography>
						</Grid>
						<Grid item md={4}>
							<Typography>Alkaen</Typography>
						</Grid>
						<Grid item md={4}>
							<Typography>Päättyen</Typography>
						</Grid>
						<Grid item md={1}>
							{/* Tyhjä grid item, jotta delete nappi istuu nätisti alemmille riveille  */}
						</Grid>
						{reservations.map((reservation) => {
							return (
								<ReservationItem key={uuidv4()} reservation={reservation} deleteReservation={deleteReservation} />
							);
						})}
					</Grid>
				) : (
					<Typography>Ei varauksia.</Typography>
				)}
			</Grid>
			<Grid item md={2}></Grid>
		</Grid>
	);
};

MyReservations.propTypes = {
	reservations: PropTypes.array,
	setReservations: PropTypes.func
};

export default MyReservations;
