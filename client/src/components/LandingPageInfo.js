import { Card, Container, Stack, Typography, Divider, Paper} from '@mui/material';
import HouseIcon from '@mui/icons-material/House';
import React from 'react';

const LandingPageInfo = () => {
	return (
		<Container 
			sx={{ 
				marginTop: 5, 
				marginBottom: 20, 
				minHeight: '100%',
				justifyContent: 'center'
			}}>
			<Divider 
				sx={{ 
					paddingBottom: '40px' 
				}}>
				<Typography 
					variant="h5">
					Tilaa joka lähtöön
				</Typography>
			</Divider>
			<Container
				sx={{
					width: '60%',
					marginBottom: '30px'
				}}>
				<Typography 
					variant='subtitle1'
					sx={{
						display: 'flex',
						justifyContent: 'center',
						width: '100%',
						height: '100px',
						fontSize: '20px',
						textAlign: 'center'
					}}>
				Tule ja löydä todelliset tilojen ihmeet palvelustamme!
				Tiloja kaikenlaiseen käyttöön harrastuksista työntekoon.
				</Typography>
			</Container>
			<Container>
				<Stack
					direction='row'
					justifyContent='center'
					alignItems='center'
					spacing={8}>
					<Paper
						elevation={3}					
						sx={{ 
							height: 300, 
							width: 500, 
							padding: '15px' 
						}}>
						<Stack
							direction='row'
							justifyContent="space-between" 
							alignItems="center" 
							spacing={12}>
							<HouseIcon 
								sx={{
									display: 'inline-block',
									width: 'auto',
									height: '50px',
									fontSize: '40px' 
								}}
							/>
							<Typography 
								variant='h4'
								sx={{
									display: 'inline-block',
									width: 'auto',
									height: '50px',
									fontSize: '20px',
									lineHeight: '50px'
								}}>
							Vuokraa tilaa
							</Typography>
						</Stack>
						<Typography
							variant='body1'
							sx={{
								textAlign: 'justify',
								marginTop: '20px',
								marginLeft: '10px',
								marginRight: '10px'
							}}>
								Palvelussamme voit vuokrata käyttäjien
								tarjoamia tiloja ympäri Suomea. Olipa
								tarpeesi näppärälle harrastustilalle,
								rauhalliselle työtilalle tai Zenmäiselle
								mietiskelyhuoneelle, löydät ne täältä. 
						</Typography>	
					</Paper>
					<Paper
						elevation={3}
						sx={{ 
							height: 300, 
							width: 500, 
							padding: '15px'
						}}>
						<Stack
							direction='row'
							justifyContent="space-between" 
							alignItems="center" 
							spacing={12}>
							<HouseIcon 
								sx={{
									display: 'inline-block',
									width: 'auto',
									height: '50px',
									fontSize: '40px'
								}}
							/>
							<Typography 
								variant='h4'
								sx={{
									display: 'inline-block',
									width: 'auto',
									height: '50px',
									fontSize: '20px',
									lineHeight: '50px'
								}}>
							Aseta tilasi vuokralle
							</Typography>
						</Stack>
						<Typography
							variant='body1'
							sx={{
								textAlign: 'justify',
								marginTop: '20px',
								marginLeft: '10px',
								marginRight: '10px'
							}}>
								Onko sinulla tilaa vapaana? Palvelussamme
								voit myös tarjota tilasi vuokralle. Sinä
								määrittelet missä, milloin ja mihin hintaan
								käytettävää tilaasi haluat vuokrata. 
						</Typography>	
					</Paper>
				</Stack>
				<Divider 
					sx={{ 
						marginTop: '40px',
						marginBottom: '40px'  
					}}>
					<Typography 
						variant="h5">
					Viikon parhaat
					</Typography>
				</Divider>
			</Container>
			<Stack 
				direction="row" 
				justifyContent="center" 
				alignItems="center" 
				spacing={4}>
				<Card 
					sx={{ 
						height: 300, 
						width: 400, 
						padding: '20px' 
					}}>
					<Typography 
						variant='p'>
						Lorem ipsum lorem ipsum jada jada lorem ipsum
					</Typography>
				</Card>
				<Card 
					sx={{ 
						height: 300, 
						width: 400, 
						padding: '20px' 
					}}>
					<Typography 
						variant='p'>
						Lorem ipsum lorem ipsum jada jada lorem ipsum
					</Typography>
				</Card>
				<Card 
					sx={{ 
						height: 300, 
						width: 400, 
						padding: '20px' 
					}}>
					<Typography 
						variant='p'>
						Lorem ipsum lorem ipsum jada jada lorem ipsum
					</Typography>
				</Card>
			</Stack>
		</Container>
	);
};

export default LandingPageInfo;
