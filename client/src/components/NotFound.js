import { Typography, Box, Container } from '@mui/material';
import React from 'react';

const NotFound = () =>{
	return (
		<Container sx={{ width: '100%', bgcolor: '#F8EDEB', paddingBottom: '100px', paddingTop: '100px' }}>
			<Box mx={15}>
				<Typography variant="h3">404 page not found</Typography>
				<Typography sx={{paddingTop: '20px'}}>We are sorry but the page you are looking for does not exist.</Typography>
			</Box>
		</Container>
	);
};

export default NotFound;