
import React, {useState, useContext} from 'react';
import { Typography, TextField, Stack } from '@mui/material';
import { Grid } from '@mui/material';
import { Divider } from '@mui/material';
import UserContext from '../contexts/UserContext';
import { axiosAuth } from '../utils/axiosUtils';
import PropTypes from 'prop-types';
import ActionButton from './ui/ActionButton';
import CancelButton from './ui/CancelButton';
import DeleteAccount from './DeleteAccount';
import { validateInt } from '../utils/validation';
import { useNavigate } from 'react-router-dom';

const ChangeUserInfo = ({setEditVisibility}) => {
	const {userState, setUserState} = useContext(UserContext);
	const [username, setUsername] = useState(userState.username);
	const [email, setEmail] = useState(userState.email);
	const navigate = useNavigate();

	const handleEditUserInfo = () => {
		if (validateInt(userState.id)) {
			const data = {
				username,
				email
			};
			axiosAuth.put(`http://localhost:3001/account/${userState.id}`, data )
				.then((response) => {
					setUserState ({username: response.data.data.username, email: response.data.data.email, id: userState.id});
					alert('Tilin tiedot muokattu onnistuneesti.');
				})
				.catch(() => {
					alert('Tilin tietojen muokkaaminen epäonnistui');
					setUsername(userState.username);
					setEmail(userState.email);
				});
		} else {
			navigate('/notfound');
		}
		
	};

	return (
		<Grid container spacing={2}>
			<Grid item md={12}>
				<Divider sx={{ paddingTop: '50px', paddingBottom: '30px' }}>
					<Typography variant="h5">Käyttäjän tiedot</Typography>
				</Divider>
			</Grid>
			<Grid item md={3}></Grid>
			<Grid item md={6} container spacing={2} sx={{ alignContent: 'center', justifyContent: 'flex-end' }}>
				<Grid item md={12}>
					<TextField
						autoComplete="off"
						fullWidth
						label="Uusi käyttäjänimi"
						value={username}
						onChange={(event) => {setUsername(event.target.value);}}
					/>
				</Grid>
				<Grid item md={12}>
					<TextField
						autoComplete="off"
						fullWidth
						label="Uusi sähköposti"
						value={email}
						onChange={(event) => {setEmail(event.target.value);}}
					/>
				</Grid>
				<Grid item md={3}>
					{/* tyhjä grid item */}
				</Grid>
				<Grid item md={12}>
					<Stack spacing={2} direction="row">
						<ActionButton 
							text="Tallenna" 
							onClickFunction={() => {
								handleEditUserInfo();
								setEditVisibility(false);
							}} />
						<CancelButton 
							text="Peruuta" 
							onClickFunction={() => {
								setUsername(userState.username);
								setEmail(userState.email);
								setEditVisibility(false);
							}} />
						<DeleteAccount />
					</Stack>
				</Grid>
			</Grid>
			<Grid item md={3}>
				{/* tyhjä grid item */}
			</Grid>
		</Grid>
	);
};
ChangeUserInfo.propTypes = {
	setEditVisibility: PropTypes.func
};

export default ChangeUserInfo;
