
import React, { useContext, useState } from 'react';
import { Typography } from '@mui/material';
import { Grid, Box } from '@mui/material';
import { Divider } from '@mui/material';
import UserContext from '../contexts/UserContext';
import ChangeUserInfo from './ChangeUserInfo.js';
import ChangePasswordForm from '../components/ChangePasswordForm';
import ActionButton from './ui/ActionButton';

const UserInfo = () => {
	const {userState} = useContext(UserContext);
	const [editVisibility, setEditVisibility] = useState(false);
	return (
		<Box>{ editVisibility ?
			<ChangeUserInfo setEditVisibility={setEditVisibility} />
			:
			<Grid container spacing={2}>
				<Grid item md={12}>
					<Divider sx={{ paddingTop: '50px', paddingBottom: '30px' }}>
						<Typography variant="h5">Käyttäjän tiedot</Typography>
					</Divider>
				</Grid>
				<Grid item md={2}></Grid>
				<Grid item md={7} container spacing={2} justifyContent="flex-end">
					<Grid item md={3}><Typography>Käyttäjänimi:</Typography></Grid>
					<Grid item md={9}><Typography>{userState.username}</Typography></Grid>
					<Grid item md={3}><Typography>Sähköposti:</Typography></Grid>
					<Grid item md={9}><Typography>{userState.email}</Typography></Grid>
					<Grid item md={3}><Typography>Salasana:</Typography></Grid>
					<Grid item md={9}><Typography>*****</Typography></Grid>
				</Grid>
				<Grid item md={3} container spacing={2}>
					<Grid item md={12}>
						<ActionButton 
							text="Muokkaa" 
							onClickFunction={() => {
								setEditVisibility(!editVisibility);
							}} />
					</Grid>
					<Grid item md={12}><ChangePasswordForm /></Grid>
				</Grid>
			</Grid>
		}</Box>
	);
};

export default UserInfo;
