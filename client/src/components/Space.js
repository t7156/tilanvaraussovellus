import React from 'react';
import { Card, CardMedia, Typography, CardContent, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Space = ({ element }) => {
	return (
		<Card
			sx={{
				marginLeft: '20px',
				padding: '5px',
				height: '300px',
				width: '350px',
				textDecoration: 'none',
				justifyItems: 'c',
			}}
			component={Link}
			to={`/spaces/${element.id}`}
			state={{ data: element }}
		>
			<CardMedia
				height="50%"
				component="img"
				src={`data:image/*;base64,${element.image}`}>
			</CardMedia>
			<CardContent>
				<Typography gutterBottom variant="h5" component="div">
					{element.name}
				</Typography>
				<Grid
					container
					columns={{ xs: 12, sm: 12, md: 12, lg: 12 }}
					justifyContent="space-between"
					sx={{
						display: 'flex',
					}}
				>
					<Grid item justifyContent="flex-start" md={6} sm={6} xs={6}>
						<Typography
							variant="subtitle1"
							color="text.secondary"
							sx={{
								marginBottom: '5px',
								fontSize: '16px',
								display: 'flex',
							}}
						>
							{element.size} m2
						</Typography>
					</Grid>
					<Grid item justifyContent="flex-end" md={6} sm={6} xs={6}>
						<Typography
							variant="subtitle1"
							color="text.secondary"
							sx={{
								marginBottom: '5px',
								fontSize: '16px',
								display: 'flex',
							}}
						>
							{element.hourly_price} € / Tunti
						</Typography>
					</Grid>
					<Grid item justifyContent="flex-end" md={12} sm={12} xs={12}>
						<Typography
							variant="subtitle2"
							color="text.secondary"
							sx={{
								marginTop: '20px',
								marginBottom: '5px',
								fontSize: '16px',
								display: 'flex',
							}}
						>
							{element.owner}
						</Typography>
					</Grid>
				</Grid>
			</CardContent>
		</Card>
	);
};

Space.propTypes = {
	element: PropTypes.any,
};

export default Space;
