import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography, Button } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

const formatTimeString = (ISOTime) => {
	const [date, time] = ISOTime.split('T');
	const [yyyy, MM, dd] = date.split('-');
	const [hh, mm] = time.split(':');
 
	return `${dd}.${MM}.${yyyy} ${hh}:${mm}`;
};

const ReservationItem = ({ reservation, deleteReservation }) => {
	return (
		<>
			<Grid item md={3}>
				<Typography>{reservation.space_name}</Typography>
			</Grid>
			<Grid item md={4}>
				<Typography>{formatTimeString(reservation.from_datetime)}</Typography>
			</Grid>
			<Grid item md={4}>
				<Typography>{formatTimeString(reservation.to_datetime)}</Typography>
			</Grid>
			<Grid item md={1}>
				<Button
					onClick={() => deleteReservation(reservation.id)}>
					<DeleteIcon />
				</Button>
			</Grid>
		</>
	);
};

ReservationItem.propTypes = {
	reservation: PropTypes.object,
	deleteReservation: PropTypes.func,
};

export default ReservationItem;