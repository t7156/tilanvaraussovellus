import { Box } from '@mui/material';
import React, { useState, useContext } from 'react';
import { axiosAuth } from '../utils/axiosUtils';
import ConfirmationModal from '../components/ConfirmationModal';
import UserContext from '../contexts/UserContext.js';
import ErrorButton from './ui/ErrorButton';
import { validateInt } from '../utils/validation';
import { useNavigate } from 'react-router-dom';

const DeleteAccount = () => {
	const [modalVisibility, setModalVisibility] = useState(false);
	const { userState, logout } = useContext(UserContext);
	const navigate = useNavigate();

	const deleteAccount = () => {
		if (validateInt(userState.id)) {
			axiosAuth
				.delete(`http://localhost:3001/account/${userState.id}`)
				.then(( ) => {
					alert('Tilisi on poistettu.');
					logout();
				})
				.catch(() => {
					alert('Tilin poistossa tapahtui virhe.');
				});
		} else {
			alert('Tilin poistossa tapahtui virhe.');
			navigate('/notfound');
		}
	};

	return (
		<Box>
			<ConfirmationModal
				visibility={modalVisibility}
				setVisibility={setModalVisibility}
				functionToConfirm={deleteAccount}
				confirmTitle={'Poista tili'}
			/>

			<ErrorButton 
				text="Poista tili" 
				onClickFunction={() => {
					setModalVisibility(true);
				}} />
		</Box>
	);
};

export default DeleteAccount;
