import React from 'react';
import { Container, Typography, Box, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { v4 as uuidv4 } from 'uuid';

const SpaceReviewItem = ({ comment, stars, username }) => {
	const getStarIcons = (stars) => {
		const starIcons = [];
		for (const starIcon = { i: 1 }; starIcon.i <= 5; starIcon.i++) {
			const i = starIcon.i;
			starIcons.push(
				<StarBorderIcon
					key={uuidv4()}
					sx={
						i <= stars
							? { fontSize: '25px', margin: '2px', color: 'rgb(252, 232, 3)' }
							: { fontSize: '25px', margin: '2px', color: 'black' }
					}
				/>
			);
		}
		return starIcons.map((element) => element);
	};
	return (
		<Container>
			<Grid
				container
				sx={{
					flexDirection: 'row',
					alignContent: 'center',
				}}
			>
				<Box
					sx={{
						margin: '20px',
					}}
				>
					<AccountCircleIcon
						sx={{
							color: '#323232',
							fontSize: '42px',
						}}
					/>
					<Typography>{username}</Typography>
				</Box>
				<Box
					sx={{
						margin: '20px',
					}}
				>
					<Typography>{comment}</Typography>
					{getStarIcons(stars)}
				</Box>
			</Grid>
		</Container>
	);
};

SpaceReviewItem.propTypes = {
	comment: PropTypes.string,
	stars: PropTypes.number,
	username: PropTypes.string,
};

export default SpaceReviewItem;
