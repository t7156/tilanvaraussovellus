import { Box, Grid, Stack } from '@mui/material';
import React, { useContext, useState } from 'react';
import { axiosAuth } from '../../utils/axiosUtils';
import PropTypes from 'prop-types';
import SpaceReviewItem from './SpaceReviewItem';
import UserContext from '../../contexts/UserContext';
import ActionButton from '../ui/ActionButton';
import CancelButton from '../ui/CancelButton';
import DeleteSpaceReview from './DeleteSpaceReview';
import SpaceReviewInput from './SpaceReviewInput';
import { validateInt } from '../../utils/validation';
import { useNavigate } from 'react-router-dom';

const EditSpaceReview = ({
	userSpaceReview,
	spaceReviewsUpdateSwitch,
	setSpaceReviewsUpdateSwitch,
}) => {
	const { userState } = useContext(UserContext);
	const [editSpaceReviewVisibility, setEditSpaceReviewVisibility] = useState(false);
	const [comment, setComment] = useState(userSpaceReview.comment);
	const [stars, setStars] = useState(userSpaceReview.stars);
	const navigate = useNavigate();

	const handleEditSpaceReview = (review_id, comment, stars) => {
		if (validateInt(review_id)) {
			if (stars && comment.length > 3 && comment.length <= 255) {
				const data = { comment, stars };
				axiosAuth
					.patch(`http://localhost:3001/review/${review_id}`, data)
					.then(() => {
						alert('Tilan arvostelua muokattu.');
						setEditSpaceReviewVisibility(false);
						setSpaceReviewsUpdateSwitch(!spaceReviewsUpdateSwitch);
					})
					.catch(() => {
						alert(
							'Tilan arvostelun muokkaamisessa tuli ongelma. Yritä myöhemmin uudelleen!'
						);
					});
			} else {
				alert('Sinun pitää antaa arvosteluteksti (Pituus: 3-255) ja tähdet!');
			}
		} else {
			navigate('/notfound');
		}
	};

	return (
		<Box
			sx={{
				flexDirection: 'column',
				justifyContent: 'center',
				alignContent: 'center',
				margin: '20px',
			}}
		>
			{!editSpaceReviewVisibility ? (
				<Grid>
					<SpaceReviewItem
						comment={userSpaceReview.comment}
						stars={Number(userSpaceReview.stars)}
						username={userState.username}
					/>
					<Box sx={{ marginLeft: '40px' }}>
						<ActionButton
							text="Muokkaa"
							onClickFunction={() => {
								setEditSpaceReviewVisibility(true);
							}}
						/>
					</Box>
				</Grid>
			) : (
				<Grid>
					<SpaceReviewInput
						comment={comment}
						setComment={setComment}
						stars={Number(stars)}
						setStars={setStars}
					/>
					<Stack direction="row" spacing={2}>
						<ActionButton
							text="Tallenna"
							onClickFunction={() => {
								handleEditSpaceReview(userSpaceReview.id, comment, stars);
							}}
						/>
						<CancelButton
							text="Peruuta"
							onClickFunction={() => {
								setComment(userSpaceReview.comment);
								setStars(userSpaceReview.stars);
								setEditSpaceReviewVisibility(false);
							}}
						/>
						<DeleteSpaceReview
							userSpaceReview={userSpaceReview}
							setEditSpaceReviewVisibility={setEditSpaceReviewVisibility}
							spaceReviewsUpdateSwitch={spaceReviewsUpdateSwitch}
							setSpaceReviewsUpdateSwitch={setSpaceReviewsUpdateSwitch}
						/>
					</Stack>
				</Grid>
			)}
		</Box>
	);
};

EditSpaceReview.propTypes = {
	userSpaceReview: PropTypes.object,
	spaceReviewsUpdateSwitch: PropTypes.bool,
	setSpaceReviewsUpdateSwitch: PropTypes.func,
};

export default EditSpaceReview;
