import React from 'react';
import { Container } from '@mui/material';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import SpaceReviewItem from './SpaceReviewItem';

const SpaceReviewList = ({ spaceReviews }) => {
	const getSpaceReviews = (spaceReviews) => {
		return spaceReviews.map((review) => {
			return (
				<SpaceReviewItem
					key={uuidv4()}
					account_id={review.account_id}
					comment={review.comment}
					id={review.id}
					space_id={review.space_id}
					stars={Number(review.stars)}
					username={review.username}
				/>
			);
		});
	};

	return <Container>{getSpaceReviews(spaceReviews)}</Container>;
};

SpaceReviewList.propTypes = {
	spaceReviews: PropTypes.array,
};

export default SpaceReviewList;
