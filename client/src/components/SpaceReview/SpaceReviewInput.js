import React from 'react';
import { TextField, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import SpaceReviewStars from './SpaceReviewStars';

const SpaceReviewInput = ({ comment, setComment, stars, setStars }) => {
	return (
		<Grid
			container
			sx={{
				flexDirection: 'column',
				justifyContent: 'center',
				alignContent: 'center',
			}}
		>
			<TextField
				multiline={true}
				rows={4}
				label="Arvosteluteksti"
				autoComplete="off"
				value={comment}
				onChange={(event) => {
					setComment(event.target.value);
				}}
			/>
			<Grid
				container
				sx={{
					flexDirection: 'column',
					alignContent: 'center',
					justifyContent: 'center',
				}}
			>
				<Grid
					container
					sx={{
						padding: '10px',
						flexDirection: 'row',
						alignContent: 'center',
						justifyContent: 'center',
					}}
				>
					<SpaceReviewStars stars={Number(stars)} setStars={setStars} />
				</Grid>
			</Grid>
		</Grid>
	);
};

SpaceReviewInput.propTypes = {
	comment: PropTypes.string,
	setComment: PropTypes.func,
	stars: PropTypes.number,
	setStars: PropTypes.func,
};

export default SpaceReviewInput;
