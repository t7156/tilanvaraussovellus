import React, { useState } from 'react';
import { Container, Stack } from '@mui/material';
import PropTypes from 'prop-types';
import { axiosAuth } from '../../utils/axiosUtils';
import SpaceReviewInput from './SpaceReviewInput';
import ActionButton from '../ui/ActionButton';
import CancelButton from '../ui/CancelButton';

const ReviewSpace = ({ space_id, spaceReviewsUpdateSwitch, setSpaceReviewsUpdateSwitch }) => {
	const [stars, setStars] = useState(0);
	const [comment, setComment] = useState('');

	const handleReviewSpace = (stars, comment, space_id, setStars, setComment) => {
		if (stars && comment.length > 3 && comment.length <= 255) {
			axiosAuth
				.post('http://localhost:3001/review', {
					stars,
					comment,
					space_id,
				})
				.then(() => {
					alert('Arvostelu lisätty.');
					setStars(0);
					setComment('');
					setSpaceReviewsUpdateSwitch(!spaceReviewsUpdateSwitch);
				})
				.catch(() => {
					alert('Arvostelun lisäämisessä tuli ongelma, yritä uudelleen!');
				});
		} else {
			alert('Sinun pitää antaa arvosteluteksti (Pituus: 3-255) ja tähdet!');
		}
	};
	const handleClose = () => {
		setStars(0);
		setComment('');
	};

	return (
		<Container>
			<SpaceReviewInput
				comment={comment}
				setComment={setComment}
				stars={Number(stars)}
				setStars={setStars}
			/>
			<Stack spacing={2} direction="row">
				<ActionButton
					text="Arvostele"
					onClickFunction={() => {
						handleReviewSpace(stars, comment, space_id, setStars, setComment);
					}}
				/>
				<CancelButton 
					text="Peruuta"
					onClickFunction={handleClose}/>
			</Stack>
		</Container>
	);
};

ReviewSpace.propTypes = {
	space_id: PropTypes.string,
	spaceReviewsUpdateSwitch: PropTypes.bool,
	setSpaceReviewsUpdateSwitch: PropTypes.func,
};

export default ReviewSpace;
