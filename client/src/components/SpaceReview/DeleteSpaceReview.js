import React from 'react';
import { axiosAuth } from '../../utils/axiosUtils';
import { Box } from '@mui/material';
import ErrorButton from '../ui/ErrorButton';
import PropTypes from 'prop-types';
import { validateInt } from '../../utils/validation';
import { useNavigate } from 'react-router-dom';

const DeleteSpaceReview = ({
	userSpaceReview,
	spaceReviewsUpdateSwitch,
	setSpaceReviewsUpdateSwitch,
	setEditSpaceReviewVisibility,
}) => {
	const navigate = useNavigate();
	
	const handleDeleteSpaceReview = () => {
		if (validateInt(userSpaceReview.id)) {
			axiosAuth
				.delete(`http://localhost:3001/review/${userSpaceReview.id}`)
				.then(() => {
					alert('Tilan arvostelun poistaminen onnistui.');
					setEditSpaceReviewVisibility(false);
					setSpaceReviewsUpdateSwitch(!spaceReviewsUpdateSwitch);
				})
				.catch(() => {
					alert('Tilan arvostelun poistamisessa tuli ongelma, koita myöhemmin uudelleen!');
				});
		} else {
			navigate('/notfound');
		}
	};

	return (
		<Box>
			<ErrorButton
				text="Poista arvostelu"
				onClickFunction={() => {
					handleDeleteSpaceReview(userSpaceReview.id);
				}}
			/>
		</Box>
	);
};

DeleteSpaceReview.propTypes = {
	userSpaceReview: PropTypes.object,
	spaceReviewsUpdateSwitch: PropTypes.bool,
	setSpaceReviewsUpdateSwitch: PropTypes.func,
	setEditSpaceReviewVisibility: PropTypes.func,
};

export default DeleteSpaceReview;
