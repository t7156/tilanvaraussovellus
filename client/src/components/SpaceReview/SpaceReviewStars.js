import React from 'react';
import { Button, Box } from '@mui/material';
import { v4 as uuidv4 } from 'uuid';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import PropTypes from 'prop-types';

const SpaceReviewStars = ({ stars, setStars }) => {
	const getStarIcons = (stars, setStars) => {
		const starIcons = [];
		for (const starIcon = { i: 1 }; starIcon.i <= 5; starIcon.i++) {
			const i = starIcon.i;
			starIcons.push(
				<Button
					key={uuidv4()}
					sx={{ margin: 0 }}
					onClick={() => {
						setStars(i);
					}}
				>
					<StarBorderIcon
						sx={
							i <= stars
								? { fontSize: '30px', color: 'rgb(252, 232, 3)' }
								: { fontSize: '30px', color: 'black' }
						}
					/>
				</Button>
			);
		}
		return starIcons.map((element) => element);
	};
	return <Box>{getStarIcons(stars, setStars)}</Box>;
};
SpaceReviewStars.propTypes = {
	stars: PropTypes.number,
	setStars: PropTypes.func,
};
export default SpaceReviewStars;
