import React, { useContext, useEffect, useState } from 'react';
import ReviewSpace from './ReviewSpace';
import { Container, Divider, Typography, Box, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import UserContext from '../../contexts/UserContext';
import SpaceReviewList from './SpaceReviewList';
import EditSpaceReview from './EditSpaceReview';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { v4 as uuidv4 } from 'uuid';
import PageSelector from '../PageSelector/PageSelector';
import axios from 'axios';
import { axiosAuth } from '../../utils/axiosUtils';
import { validateInt } from '../../utils/validation';
import { useNavigate } from 'react-router-dom';

const SpaceReviewContainer = ({ space_id }) => {
	const { authState, userState } = useContext(UserContext);
	const [spaceReviews, setSpaceReviews] = useState([]);
	const [userSpaceReview, setUserSpaceReview] = useState({});
	const [spaceReviewsUpdateSwitch, setSpaceReviewsUpdateSwitch] = useState(false);
	const [spaceStarsAvg, setSpaceStarsAvg] = useState(0);
	const navigate = useNavigate();

	const setData = (data, setSpaceReviews) => {
		const uRev = data.find((review) => review.account_id === userState.id);
		const spaceReviewsWithoutURev = { reviews: data };
		if (uRev) {
			spaceReviewsWithoutURev.reviews = spaceReviewsWithoutURev.reviews.filter(
				(spaceReview) => spaceReview.id !== uRev.id
			);
		}
		setSpaceReviews(spaceReviewsWithoutURev.reviews);
	};

	useEffect(() => {
		const getSpaceStarsAvg = () => {
			axios
				.get(`http://localhost:3001/review/stars_average/${space_id}`)
				.then((res) => {
					setSpaceStarsAvg(res.data.data);
				})
				.catch(() => {
					setSpaceStarsAvg(0);
				});
		};
		const getUserSpaceReview = () => {
			axiosAuth
				.get(`http://localhost:3001/review/space/account/${space_id}`)
				.then((res) => {
					setUserSpaceReview(res.data.data);
				})
				.catch(() => {
					setUserSpaceReview({});
				});
		};
		
		if (validateInt(space_id)) {
			getSpaceStarsAvg();
			getUserSpaceReview();
		} else {
			navigate('/notfound');
		}
		
	}, [spaceReviewsUpdateSwitch]);

	const getSpaceAvgStarIcons = (stars) => {
		const starIcons = [];
		for (const starIcon = { i: 1 }; starIcon.i <= 5; starIcon.i++) {
			const i = starIcon.i;
			starIcons.push(
				<StarBorderIcon
					key={uuidv4()}
					sx={
						i <= stars + 0.5
							? { fontSize: '32px', margin: '2px', color: 'rgb(252, 232, 3)' }
							: { fontSize: '32px', margin: '2px', color: 'black' }
					}
				/>
			);
		}
		return starIcons.map((element) => element);
	};
	return (
		<Container>
			<Box>
				<Divider sx={{ paddingTop: '50px' }}>
					<Typography variant="h5">Arvostelut</Typography>
				</Divider>
				<Grid
					container
					sx={{
						paddingBottom: '50px',
						flexDirection: 'row',
						alignContent: 'center',
						justifyContent: 'center',
					}}
				>
					{getSpaceAvgStarIcons(spaceStarsAvg)}
				</Grid>
				{authState.isLoggedIn ? (
					userSpaceReview?.id ? (
						<EditSpaceReview
							userSpaceReview={userSpaceReview}
							spaceReviewsUpdateSwitch={spaceReviewsUpdateSwitch}
							setSpaceReviewsUpdateSwitch={setSpaceReviewsUpdateSwitch}
						/>
					) : (
						<ReviewSpace
							spaceReviewsUpdateSwitch={spaceReviewsUpdateSwitch}
							setSpaceReviewsUpdateSwitch={setSpaceReviewsUpdateSwitch}
							space_id={space_id}
						/>
					)
				) : null}
				<SpaceReviewList spaceReviews={spaceReviews} />
				<PageSelector
					limit={10}
					setData={(data) => {
						setData(data, setSpaceReviews);
					}}
					address={`http://localhost:3001/review/space/${space_id}/?`}
					update={spaceReviewsUpdateSwitch}
				/>
			</Box>
		</Container>
	);
};

SpaceReviewContainer.propTypes = {
	space_id: PropTypes.string,
};

export default SpaceReviewContainer;
