import React, { useState, useEffect, useContext } from 'react';
import { useTheme } from '@mui/system';
import { Box, Typography, TextField, Grid, Stack } from '@mui/material';
import { Link } from 'react-router-dom';
import ActionButton from './ui/ActionButton';
import CancelButton from './ui/CancelButton';
import ModalCentered from './ui/ModalCentered';
import PropTypes from 'prop-types';
import { axiosAuth } from '../utils/axiosUtils';
import format from 'date-fns/format';
import fiLocale from 'date-fns/locale/fi';
import UserContext from '../contexts/UserContext';

const makeReservation = async (data) => {
	return await axiosAuth.post('http://localhost:3001/reservation', data);
};

const formatTime = (dateObj) => {
	// Converts Date object to string with 'yyyy-MM-dd HH:mm:ss' format
	const formattedTime = `${dateObj.getFullYear()}-`
		+ `${('0' + `${(dateObj.getMonth() + 1)}`).slice(-2)}` + '-'
		+ `${('0' + `${dateObj.getDate()}`).slice(-2)} `
		+ `${('0' + `${dateObj.getHours()}`).slice(-2)}` + ':'
		+ `${('0' + `${dateObj.getMinutes()}`).slice(-2)}` + ':'
		+ `${('0' + `${dateObj.getSeconds()}`).slice(-2)}`;

	return formattedTime;
};

const ReservationForm = ({ 
	spaceName,
	spaceId, 
	hourlyPrice, 
	reservationStart,
	reservationEnd,
	calendarRef,
	clearSelection,
}) => {
	const [open, setOpen] = useState(false);
	const handleOpen = () => setOpen(true);
 	const handleClose = () => {
		setOpen(false);
		clearSelection();
	};
	const [totalPrice, setTotalPrice] = useState();
	const { authState } = useContext(UserContext);
	const theme = useTheme();
	
	useEffect(() => {
		const reservationLength = ((reservationEnd-reservationStart)/(1000 * 60 * 60));
		setTotalPrice(hourlyPrice * reservationLength);
	}, [reservationStart, reservationEnd]);

	const handleSubmit = (event) => {
		// TO DO: validation
		event.preventDefault();
		if (reservationStart && reservationEnd && (reservationStart < reservationEnd)) {
			const formattedStartTime = formatTime(reservationStart);
			const formattedEndTime = formatTime(reservationEnd);
			const reservationData = {
				space_id: spaceId,
				from_datetime: formattedStartTime,
				to_datetime: formattedEndTime,
			};
			makeReservation(reservationData)
				.then((response) => {
					handleClose();
					alert('Varauksen teko onnistui.');
					const calendarApi = calendarRef.current.getApi();
					calendarApi.addEvent({
						title: 'Oma varaus',
						start: response.data.data.from_datetime,
						end: response.data.data.to_datetime,
						color: theme.palette.calendar.light,
					  });
				})
				.catch((error) => {
					if (error.response.status === 409) {
						alert('Virhe: Tila on jo varattuna halutulle ajalle.');
					}
					else if (error.response.status === 400) {
						alert('Virhe: Tarkista varauksen tiedot.');
					}
					else {
						alert('Jokin meni vikaan, yritä uudelleen.');
					};
				});
		} else {
			alert('Valitse aika, jolle haluat tehdä varauksen.');
		}
	};

	return (
		<Box>
			{ authState.isLoggedIn  
				? <ActionButton 
					text="Tee varaus" 
					onClickFunction={handleOpen} />
				: <Typography>
					<Link to="/login">Kirjaudu sisään</Link>, jotta voit tehdä varauksen.
				</Typography>
			}
			<ModalCentered
				open={open}
				onClose={handleClose}
				width={400}
			>
				<Typography mb={2} id="modal-modal-title" variant="h6" component="h2">Varaa {spaceName}</Typography>
				<form onSubmit={handleSubmit}>
					<Grid container rowSpacing={1} mb={2}>
						<Grid item xs={4}>
							<Typography>Alkaen: </Typography>
						</Grid>
						<Grid item xs={8}>
							<Typography>
								{reservationStart ? format(reservationStart, 'cccc dd.MM.yyyy HH:mm', { locale: fiLocale }) : 'Valitse aika kalenterista'}
							</Typography>
						</Grid>
						<Grid item xs={4}>
							<Typography>Päättyen: </Typography>
						</Grid>
						<Grid item xs={8}>
							<Typography>
								{reservationEnd ? format(reservationEnd, 'cccc dd.MM.yyyy HH:mm', { locale: fiLocale }) : 'Valitse aika kalenterista'}
							</Typography>
						</Grid>
					</Grid>
					<TextField 
						id="outlined-multiline-static"
						multiline
          					rows={2}
						fullWidth
						label="Lisätietoa varauksesta"
						variant="outlined" />
					<br />
					<Typography sx={{ my: 2 }}>{`Varauksen hinta ${totalPrice ? totalPrice : 0} €`}</Typography>
					<Stack spacing={2} direction="row">
						<ActionButton 
							type="submit"
							text="Vahvista"/>
						<CancelButton 
							text="Peruuta"
							onClickFunction={handleClose}/>
					</Stack>
				</form>
			</ModalCentered>
		</Box>
	);
};

ReservationForm.propTypes = {
	spaceName: PropTypes.string,
	spaceId: PropTypes.number,
	hourlyPrice: PropTypes.number,
	reservationStart: PropTypes.instanceOf(Date),
	reservationEnd: PropTypes.instanceOf(Date),
	calendarRef: PropTypes.object,
	clearSelection: PropTypes.func,
};

export default ReservationForm;
