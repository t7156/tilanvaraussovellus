import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import { Box, TextField, Button, Typography } from '@mui/material';
import ButtonSecondaryLarge from './ui/ButtonSecondaryLarge';
import axios from 'axios';

const ReSendConfirmationEmail = ({ setReSendEmail }) => {
	const [email, setEmail] = useState('');
    
	const reSendConfirmationCode = async () => {
		return await axios.patch('http://localhost:3001/auth/confirmation', { email });
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		// TODO: validate email
		if (email) {
			reSendConfirmationCode()
				.then(() => {
					alert('Uusi vahvistuskoodi lähetetty.');
					setEmail('');
					setReSendEmail(false);
				})
				.catch(() => {
					alert('Jotain meni pieleen. Tarkista email-osoite ja yritä uudelleen.');
				});
		} 
	}; 

	return (
		<>
			<Typography mb={{ xs: 3, sm: 4, md: 5 }} variant="h4" textAlign="center">
					Lähetä vahvistusviesti uudelleen
			</Typography>
			<form onSubmit={handleSubmit}>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="register-email"
					label="Email"
					type="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}>
				</TextField>
				<Box pt={{ xs: 1, sm: 2, md: 3 }}>
					<ButtonSecondaryLarge 
						type="submit" 
						text="Ok"
					/>
				</Box>
			</form>
			<Box pt={{ xs: 1, sm: 2, md: 3 }}>
				<Button
					fullWidth
					size="large"
					onClick={() => setReSendEmail(false)}
				>
                    Takaisin
				</Button>
			</Box>
		</>
	);
};

export default ReSendConfirmationEmail;

ReSendConfirmationEmail.propTypes = {
	setReSendEmail: PropTypes.func
};