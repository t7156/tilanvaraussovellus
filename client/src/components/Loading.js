import React from 'react';
import { Box, CircularProgress, Typography, Stack } from '@mui/material';

const Loading = () => {
	return (
		<Box pt={20}
			display="flex"
			alignItems="center"
			justifyContent="center">
			<Stack spacing={2} direction="column">
				<CircularProgress size="60px" disableShrink />
				<Typography>Loading...</Typography>
			</Stack>
		</Box>
	);
};

export default Loading;