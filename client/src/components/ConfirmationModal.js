import { Input, Typography, Box, Stack } from '@mui/material';
import ModalCentered from './ui/ModalCentered';
import ActionButton from './ui/ActionButton';
import CancelButton from './ui/CancelButton';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { axiosAuth } from '../utils/axiosUtils';

const ConfirmationModal = ({ visibility, setVisibility, functionToConfirm, confirmTitle }) => {
	const [password, setPassword] = useState('');

	const confirmFunction = async () => {
		const data = {
			password,
		};

		axiosAuth
			.post('http://localhost:3001/auth/verify_password', data)
			.then(async (response) => {
				if (response?.data?.data === true) {
					await functionToConfirm();
				} 
			})
			.catch(() => {
				setPassword('');
				console.log('Wrong password.');
			});

	};

	const handleClose = () => {
		setPassword('');
		setVisibility(false);
	};

	return (
		<ModalCentered
			open={visibility}
			onClose={handleClose}
			width={280}
		>
			<Typography gutterBottom id="modal-modal-title" variant="h6" component="h2">
				{confirmTitle}
			</Typography>
			<Input
				type="password"
				placeholder="Salasana"
				onChange={(event) => {
					setPassword(event.target.value);
				}}
				value={password}
				fullWidth
			/>
			<Box mt={2}>
				<Stack spacing={2} direction="row">
					<ActionButton 
						onClickFunction={() => {
							confirmFunction();
						}}
						text="Vahvista"/>
					<CancelButton 
						text="Peruuta"
						onClickFunction={handleClose}/>
				</Stack>
			</Box>
		</ModalCentered>
	);
};

export default ConfirmationModal;

ConfirmationModal.propTypes = {
	visibility: PropTypes.bool,
	setVisibility: PropTypes.func,
	functionToConfirm: PropTypes.func,
	confirmTitle: PropTypes.string,
};
