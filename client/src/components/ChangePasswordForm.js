import { Box, OutlinedInput, InputAdornment, IconButton, Stack } from '@mui/material';
import ModalCentered from './ui/ModalCentered';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { useState } from 'react';
import { axiosAuth } from '../utils/axiosUtils';
import React from 'react';
import ActionButton from './ui/ActionButton';
import CancelButton from './ui/CancelButton';

const ChangePasswordForm = () => {
	const [ oldPassword, setOldPassword ] = useState('');
	const [ newPassword, setNewPassword ] = useState('');
	const [ confirmPassword, setConfirmPassword ] = useState('');
	const [ showOldPassword, setShowOldPassword] = useState(false);
	const [ showNewPassword, setShowNewPassword] = useState(false);
	const [ showConfirmPassword, setShowConfirmPassword] = useState(false);

	const submitPasswordChange = async () => {
		const data = {
			oldPassword,
			newPassword
		};

		return await axiosAuth.patch('http://localhost:3001/account/password', 
			data,
		);
	};

	const [opened, setOpened] = useState(false);
	const handleOpen = () => {
		setOpened(true);
	};
	const handleClose = () => {
		setOldPassword('');
		setNewPassword('');
		setConfirmPassword('');
		setOpened(false);
	};

	const clearFields = () => {
		setOldPassword('');
		setNewPassword('');
		setConfirmPassword('');
	};

	const handleShowoldPassword = () => {
		setShowOldPassword(!showOldPassword);
	};

	const handleShowNewPassword = () => {
		setShowNewPassword(!showNewPassword);
	};

	const handleShowConfirmPassword = () => {
		setShowConfirmPassword(!showConfirmPassword);
	};

	const validateChange = () => {
		if (oldPassword && newPassword && confirmPassword) {
			if (newPassword === confirmPassword) {
				return true;
			} else {
				alert('Salasanat eivät täsmää.');
				return false;
			}
		} else {
			alert('Täytä pakolliset kentät.');
			return false;
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateChange()) {
			submitPasswordChange()
				.then((response) => {
					if (response.status === 200) {
						alert('Salasana vaihdettu.');
						handleClose();
					} else {
						console.log('Server responded with status ', response.status);
					}
				})
				.catch(() => {
					alert('Salasanan vaihto ei onnistunut. Yritä uudelleen.');
					clearFields(); 
				});        

		}
	};

	return <Box>
		<ActionButton 
			text="Vaihda salasana"
			onClickFunction={handleOpen}/>
		<ModalCentered
			open={opened}
			onClose={handleClose}
			width={500}
		>
			<OutlinedInput
				sx={{
					marginTop: '20px',
					marginBottom: '20px',
				}} 
				fullWidth
				required
				type={showOldPassword ? 'text' : 'password'}
				value={oldPassword}
				label='Vanha salasana'
				placeholder='Vanha salasana'
				onChange={(e) => setOldPassword(e.target.value)}
				endAdornment={
					<InputAdornment position='end'>
						<IconButton
							onClick={handleShowoldPassword}
							edge='end'>
							{showOldPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					</InputAdornment>}
			/>
			<OutlinedInput
				sx={{ 
					marginTop: '20px',
					marginBottom: '20px'
				}}
				fullWidth  
				required
				type={showNewPassword ? 'text' : 'password'}
				value={newPassword}
				label='Uusi salasana'
				placeholder='Uusi salasana'
				onChange={(e) => setNewPassword(e.target.value)}
				endAdornment={
					<InputAdornment position='end'>
						<IconButton
							onClick={handleShowNewPassword}
							edge='end'>
							{showNewPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					</InputAdornment>}                   
			/>
			<OutlinedInput
				sx={{ 
					marginTop: '20px',
					marginBottom: '20px'
				}}  
				fullWidth
				required
				type={showConfirmPassword ? 'text' : 'password'}
				value={confirmPassword}
				label='Uusi salasana uudestaan'
				placeholder='Uusi salasana uudestaan'
				onChange={(e) => setConfirmPassword(e.target.value)}
				endAdornment={
					<InputAdornment position='end'>
						<IconButton
							onClick={handleShowConfirmPassword}
							edge='end'>
							{showConfirmPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					</InputAdornment>}
			/>
			<Stack 
				spacing={2}
				justifyContent="center"
				direction="row">
				<ActionButton 
					text="Vaihda salasana"
					onClickFunction={handleSubmit}/>
				<CancelButton 
					text="Peruuta"
					onClickFunction={handleClose}/>
			</Stack>   
		</ModalCentered>
	</Box>;
};

export default ChangePasswordForm;
