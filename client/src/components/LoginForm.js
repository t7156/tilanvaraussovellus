import React, { useContext, useState } from 'react';
import axios from 'axios';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Box, Stack, TextField, Typography } from '@mui/material';
import ButtonSecondaryLarge from './ui/ButtonSecondaryLarge';
import UserContext from '../contexts/UserContext';

const login = async (email, password) => {
	return await axios.post('http://localhost:3001/auth/login', {
		email,
		password,
	});
};

const LoginForm = () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const { handleLogin } = useContext(UserContext);
	const { state } = useLocation();
	const navigate = useNavigate();

	const resetForm = () => {
		setEmail('');
		setPassword('');
	};

	const validateLogin = () => {
		// TODO: validation rules
		return email && password;
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateLogin()) {
			login(email, password)
				.then((res) => {
					if (res.status === 200) {
						handleLogin(res.data.data);
						resetForm();
						navigate(state?.path || '/profilepage');
					}
				})
				.catch(() => {
					alert('Virheellinen email tai salasana.');
				});
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<Stack spacing={3}>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="login-email"
					label="Email"
					type="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="login-password"
					label="Salasana"
					type="password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>
				<Box pt={{ xs: 1, sm: 2, md: 3 }}>
					<ButtonSecondaryLarge
						type="submit"
						text="Kirjaudu"
					/>
				</Box>
				<Box pt={{ xs: 1, sm: 2, md: 3 }} textAlign="center">
					<Typography>
						Uusi käyttäjä? <Link to="/register">Luo tili</Link>
					</Typography>
				</Box>
			</Stack>
		</form>
	);
};

export default LoginForm;
