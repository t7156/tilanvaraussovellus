import React from 'react';
import { Button } from '@mui/material';
import PropTypes from 'prop-types';

const CancelButton = ({text, onClickFunction}) => {
	return (

		<Button
			onClick={onClickFunction}
			variant='outlined'
			sx={{
				minWidth: 100,
				maxHeight: 30,
				fontSize: '14px'
			}}>{text}</Button>
	);
};
CancelButton.propTypes = {
	onClickFunction: PropTypes.func,
	text: PropTypes.string
};

export default CancelButton;