import React from 'react';
import { OutlinedInput, InputLabel } from '@mui/material';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

const textStyle =  {
	minWidth: '500px',
	marginTop: '10px',
	marginBottom: '10px',
};

const numberStyle = {
	minWidth: '50px',
	maxWidth: '100px',
	marginTop: '10px',
	marginBottom: '10px',
};

const AddSpaceInput = ({ label, value, type, onChange }) => {
	const id = uuidv4();

	return (
		<>
			<InputLabel
				htmlFor={id}
				sx={{
					color: 'text.primary',
					fontSize: '20px',
					marginTop: '20px',
				}}>
				{label}
			</InputLabel>
			<OutlinedInput
				id={id}
				value={value}
				onChange={onChange}
				type={type}
				sx={ type === 'number' ? numberStyle : textStyle }
			/>
		</>
	);
};

AddSpaceInput.propTypes = {
	label: PropTypes.string,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]),
	type: PropTypes.string,
	onChange: PropTypes.func,
};

export default AddSpaceInput;