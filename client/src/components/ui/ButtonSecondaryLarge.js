import React from 'react';
import { Button } from '@mui/material';
import PropTypes from 'prop-types';

const ButtonSecondaryLarge = ({ type, text, onClickFunction }) => {
	return (
		<Button
			fullWidth
			type={type}
			variant="contained"
			color="secondary"
			size="large"
			onClick={onClickFunction}
		>
			{text}
		</Button>
	);
};

ButtonSecondaryLarge.propTypes = {
	type: PropTypes.string,
	text: PropTypes.string,
	onClickFunction: PropTypes.func,
};

export default ButtonSecondaryLarge;