import React from 'react';
import { Box, Modal } from '@mui/material';
import PropTypes from 'prop-types';

const ModalCentered = ({ open, onClose, width, children }) => {
	const style = {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width,
		bgcolor: 'background.paper',
		border: '2px solid #000',
		boxShadow: 24,
		p: 4,
	};

	return (
		<Modal
			open={open}
			onClose={onClose}>
			<Box 
				sx={style}>
				{children}
			</Box>
		</Modal>
	);
};

ModalCentered.propTypes = {
	open: PropTypes.bool,
	onClose: PropTypes.func,
	children: PropTypes.node,
	width: PropTypes.oneOfType([
		PropTypes.number,
		PropTypes.string,
	])
};

export default ModalCentered;