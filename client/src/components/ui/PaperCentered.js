import React from 'react';
import { Paper, Stack } from '@mui/material';
import PropTypes from 'prop-types';

const PaperCentered = ({ children }) => {
	return (
		<Paper
			component={Stack}
			elevation={3}
			width={{ xs: '100%', sm: '90%', md: '60%', lg: '50%' }}
			mx="auto"
			my={{ xs: 2, sm: 6, md: 8, lg: 10 }}
			py={{ xs: 4, sm: 6, md: 8 }}
			px={{ xs: 2, sm: 3, md: 4 }}
		>
			{children}
		</Paper>
	);
};

PaperCentered.propTypes = {
	children: PropTypes.node,
};

export default PaperCentered;