import React from 'react';
import { Button } from '@mui/material';
import PropTypes from 'prop-types';

const ErrorButton = ({text, onClickFunction}) => {
	return (

		<Button
			onClick={onClickFunction}
			variant='outlined'
			color='error'
			sx={{
				minWidth: 100,
				maxHeight: 30,
				fontSize: '14px'
			}}>{text}</Button>
	);
};
ErrorButton.propTypes = {
	onClickFunction: PropTypes.func,
	text: PropTypes.string
};

export default ErrorButton;