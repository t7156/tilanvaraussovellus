import React from 'react';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const NavigationLink = ({ to, text }) => {
	return (
		<Button
			disableElevation
			variant="contained"
			sx={{
				minWidth: 100,
				maxHeight: 50,
				fontSize: '24px'
			}}
			component={Link}
			to={to}
		>
			{text}
		</Button>
	);
};

NavigationLink.propTypes = {
	to: PropTypes.string,
	text: PropTypes.string,
};

export default NavigationLink;