import React from 'react';
import { Button } from '@mui/material';
import PropTypes from 'prop-types';

const ActionButton = ({text, onClickFunction, type}) => {
	return (

		<Button
			onClick={onClickFunction}
			variant='contained'
			type={type}
			sx={{
				minWidth: 100,
				maxHeight: 30,
				fontSize: '14px'
			}}>{text}</Button>
	);
};
ActionButton.propTypes = {
	onClickFunction: PropTypes.func,
	text: PropTypes.string,
	type: PropTypes.string
};

export default ActionButton;