import React, { useState } from 'react';
import { Typography } from '@mui/material';
import { Grid, Card } from '@mui/material';
import { Box } from '@mui/system';
import { axiosAuth } from '../../utils/axiosUtils';
import ConfirmationModal from '../ConfirmationModal.js';
import ActionButton from '../ui/ActionButton.js';
import PropTypes from 'prop-types';
import EditSpace from './EditSpace';
import { validateInt } from '../../utils/validation'; 
import { useNavigate } from 'react-router-dom';

const MySpace = ({ element, displayedSpaces, setDisplayedSpaces, update, setUpdate }) => {
	const [confirmationModalVisibility, setConfirmationModalVisibility] = useState(false);
	const navigate = useNavigate();

	const handleSpaceDelete = (
		id,
		displayedSpaces,
		setDisplayedSpaces,
		setConfirmationModalVisibility
	) => {
		if (validateInt(id)) {
			axiosAuth
				.delete(`http://localhost:3001/space/${id}`)
				.then(() => {
					setConfirmationModalVisibility(false);
					alert('Tilan poistaminen onnistui.');
					setDisplayedSpaces(() => {
						return displayedSpaces?.filter((element) => element.id !== id);
					});
				})
				.catch(() => {
					alert('Tilan poistaminen ei onnistunut.');
				});
		} else {
			alert('Tilan poistaminen ei onnistunut.');
			navigate('/notfound');
		}
	};

	return (
		<Grid
			sx={{
				flexGrow: 1,
			}}
			item
			md={4}
			container
			spacing={2}
		>
			<Card
				sx={{
					marginLeft: '20px',
					padding: '5px',
					display: 'flex',
					maxHeight: '300px',
					maxWidth: '400px',
				}}
			>
				<Grid
					container
					sx={{
						minHeight: '200px',
						maxHeight: '300px',
						minWidth: '300px',
						maxWidth: '400px',
					}}
				>
					<Box
						sx={{
							minHeight: '200px',
							maxHeight: '300px',
							minWidth: '300px',
							maxWidth: '400px',
						}}
					>
						<Typography
							variant="h5"
							sx={{
								fontSize: '16px',
							}}
						>
							{' '}
							<ConfirmationModal
								visibility={confirmationModalVisibility}
								setVisibility={setConfirmationModalVisibility}
								functionToConfirm={() => {
									handleSpaceDelete(
										element.id,
										displayedSpaces,
										setDisplayedSpaces,
										setConfirmationModalVisibility
									);
								}}
								confirmTitle={'Poista tila'}
							/>
							<ActionButton
								text="Poista tila"
								onClickFunction={() => {
									setConfirmationModalVisibility(true);
								}}
							/>
							<EditSpace element={element} update={update} setUpdate={setUpdate} />
							<Grid item md={12} sx={{ justifyContent: 'flex-end' }}>
								Tilan nimi: {element.name}
							</Grid>
							<Grid item md={12} sx={{ justifyContent: 'flex-end' }}>
								Tilan koko: {element.size}
							</Grid>
							<Grid item md={12} sx={{ justifyContent: 'flex-end' }}>
								Omistaja: {element.owner}
							</Grid>
							<Grid item md={12} sx={{ justifyContent: 'flex-end' }}>
								Tyyppi: {element?.categories?.map(element => element.category_name)}
							</Grid>
						</Typography>
					</Box>
				</Grid>
			</Card>
		</Grid>
	);
};

MySpace.propTypes = {
	update: PropTypes.bool,
	setUpdate: PropTypes.func,
	element: PropTypes.object,
	displayedSpaces: PropTypes.array,
	setDisplayedSpaces: PropTypes.func,
};

export default MySpace;
