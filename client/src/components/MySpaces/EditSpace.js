import React, { useState, useEffect } from 'react';
import ErrorButton from '../ui/ErrorButton';
import ActionButton from '../ui/ActionButton';
import CancelButton from '../ui/CancelButton';
import {
	Box,
	Typography,
	FormControl,
	MenuItem,
	Grid,
	Card,
	TextField,
	Select,
	InputLabel,
	Stack
} from '@mui/material';
import ModalCentered from '../ui/ModalCentered';
import CategoryIcon from '@mui/icons-material/Category';
import PropTypes from 'prop-types';
import { axiosAuth } from '../../utils/axiosUtils';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import SpaceImages from '../SpaceImages';
import { validateInt } from '../../utils/validation';
import { useNavigate } from 'react-router-dom';

const EditSpace = ({ element, update, setUpdate }) => {
	const [editModalVisibility, setEditModalVisibility] = useState(false);
	const [spaceName, setSpaceName] = useState(element.name);
	const [street, setStreet] = useState(JSON.parse(element.address).street);
	const [postal, setPostal] = useState(JSON.parse(element.address).postal);
	const [city, setCity] = useState(JSON.parse(element.address).city);
	const [size, setSize] = useState(element.size);
	const [hourly_price, setHourly_price] = useState(element.hourly_price);
	const [description, setDescription] = useState(element.description);
	const [categories, setCategories] = useState ('');
	const [category, setCategory] = useState('');
	const [spaceCategories, setSpaceCategories] = useState([]);
	const [categoryId, setCategoryId] = useState([]);
	const navigate = useNavigate();

	useEffect(() => {
		const getData = async () => {
			await axios
				.get('http://localhost:3001/category')
				.then((res) => {
					setCategories(res.data.data);
				})
				.catch(() => {
					alert('Ongelma kategorioiden noudossa.');
				});
		};
		getData();
	}, []);

	const handleChange = (event) => {
		setCategory(event.target.value);
	};

	const handleClick = (category) => {
		if (spaceCategories.includes(category.category_name)) {
			setSpaceCategories((spaceCategories) => {
				return spaceCategories?.filter((element) => element !== category.category_name);
			});
			setCategoryId((categoryId) => {
				return categoryId?.filter((element) => element !== category.id);
			});
		} else {
			setSpaceCategories((spaceCategories) => {
				return [...spaceCategories, category.category_name];
			});
			setCategoryId((categoryId) => {
				return [...categoryId, category.id];
			});
		}
	};
	const [deleteImage, setDeleteImage] = useState(false);
	const [imageFile, setImageFile] = useState();
	const [image, setImage] = useState(element.image);

	const convertFileToBase64 = async (file) => {
		return await new Promise((resolve, reject) => {
			const fileReader = new FileReader();
			fileReader.readAsDataURL(file);

			fileReader.onloadend = () => {
				const imgBase64 = fileReader.result.split(',')[1];
				resolve(setImage(imgBase64));
			};

			fileReader.onerror = (error) => {
				reject(error);
			};
		});
	};

	const handleSpaceEdit = (
		id,
		spaceName,
		street,
		postal,
		city,
		size,
		hourly_price,
		description,
		setEditModalVisibility,
		update,
		setUpdate,
		imageFile,
		deleteImage
	) => {
		const address = { street, postal, city };
		const spaceCategories = categoryId;
			
		const formData = new FormData();

		formData.append('image', imageFile);
		formData.append('name', spaceName);
		formData.append('address', JSON.stringify(address));
		formData.append('size', size);
		formData.append('description', description);
		formData.append('hourly_price', hourly_price);
		formData.append('spaceCategories', spaceCategories);
		formData.append('deleteImage', deleteImage);

		if (validateInt(id)) {
			axiosAuth
				.put(`http://localhost:3001/space/${id}`, formData, {
					headers: {
						'Content-Type': 'multipart/form-data;',
					},
				})
				.then(() => {
					alert('Tilan muokkaaminen onnistui.');
					setEditModalVisibility(false);
					setUpdate(!update);
				})
				.catch(() => {
					alert('Tilan muokkaaminen ei onnistunut.');
					setEditModalVisibility(false);
				});
		} else {
			navigate('/notfound');
		}
	};

	const validateSubmit = () => {
		// TODO: validate image and deleteImage
		return spaceName.length >= 3 &&
			spaceName.length <= 50 &&
			street.length >= 5 &&
			street.length <= 50 &&
			city.length >= 2 &&
			city.length <= 50 &&
			postal.length === 5 &&
			size >= 1 &&
			hourly_price >= 1 &&
			description.length >= 10 &&
			description.length <= 900 &&
			!isNaN(postal) &&
			!isNaN(size) && 
			!isNaN(hourly_price)
			? true
			: false;
	};
	return (
		<Box>
			<ActionButton
				text="Muokkaa"
				onClickFunction={() => {
					setEditModalVisibility(true);
				}}
			/>
			<ModalCentered
				open={editModalVisibility}
				onClose={() => {
					setEditModalVisibility(false);
				}}
				width={400}
			>
				<Box sx={{
					overflowY: 'scroll',
					height: '400px',
					paddingLeft: '10px', 
				}}>

					<Typography id="modal-modal-title" variant="h6" component="h2">
						Muokkaa tilaa
					</Typography>
					<TextField
						sx={{ marginTop: '10px' }}
						type="text"
						label="Tilan nimi"
						onChange={(event) => {
							setSpaceName(event.target.value);
						}}
						value={spaceName}
					/>
					<TextField
						sx={{ marginTop: '10px' }}
						type="text"
						label="Katuosoite"
						onChange={(event) => {
							setStreet(event.target.value);
						}}
						value={street}
					/>
					<TextField
						sx={{ marginTop: '10px' }}
						type="text"
						label="Postinumero"
						onChange={(event) => {
							setPostal(event.target.value);
						}}
						value={postal}
					/>
					<TextField
						sx={{ marginTop: '10px' }}
						type="text"
						label="Kaupunki"
						onChange={(event) => {
							setCity(event.target.value);
						}}
						value={city}
					/>
					<TextField
						sx={{ marginTop: '10px' }}
						type="text"
						label="Tilan koko (m2)"
						onChange={(event) => {
							setSize(event.target.value);
						}}
						value={size}
					/>
					<TextField
						sx={{ marginTop: '10px' }}
						type="text"
						label="Hinta €/h"
						onChange={(event) => {
							setHourly_price(event.target.value);
						}}
						value={hourly_price}
					/>
					<Box sx={{ minWidth: 120, marginTop: '10px' }}>
						<FormControl fullWidth> 
							<InputLabel>Kategoria</InputLabel>
							<Select 
								value={category} 
								label="Kategoria" 
								onChange={handleChange} 
								sx={{width: '227px'}}>
									
								{categories &&
										categories.map((category) => (
											<MenuItem 
												key={uuidv4()}
												value={category.category_name}
												onClick={() => handleClick(category)}
											>
												<Typography variant="p">
													{category.category_name}
												</Typography>
											</MenuItem>
										))}
							</Select>
							<Grid
								container
								spacing={3}
								sx={{
									display: 'flex', 
									flexDirection: 'column',
									marginTop: '10px',
									marginBottom: '20px',
								}}
							>
								{spaceCategories.length > 0 ? (
									spaceCategories.map((category, index) => (
										<Grid item md={4} key={uuidv4()} >
											<Card
												sx={{
													maxWidth: '140px',
													minWidth: '140px',
													paddingTop: '5px',
													paddingBottom: '5px',
													display: 'inline-flex',
													justifyContent: 'flex-end',
												}}
											>
												<CategoryIcon
													sx={{
														fontSize: '18px',
														marginRight: '15px',
														marginTop: '5px',
													}}
												/>
												<Typography
													variant="h5"
													sx={{
														fontSize: '19px',
														marginRight: '15px',
													}}
												>
													{spaceCategories[index]}
												</Typography>
											</Card>
										</Grid>
									))
								) : (
									<Typography
										variant="h5"
										sx={{
											marginTop: '20px',
											marginBottom: '20px',
											marginLeft: '20px',
										}}
									>
											Ei kategorioita valittu.
									</Typography>
								)}
							</Grid>
						</FormControl>
					</Box>
					<TextField
						sx={{ marginTop: '10px', marginBottom: '10px' }}
						multiline
						rows={3}
						type="text"
						label="Kuvaus"
						onChange={(event) => {
							setDescription(event.target.value);
						}}
						value={description}
					/>
					<SpaceImages image={image} width="100px" height="auto"/>
					<input
						type="file"
						name="image"
						accept="image/*"
						multiple
						onChange={(event) => {
							convertFileToBase64(event.target.files[0]);
							setImageFile(event.target.files[0]);
							setDeleteImage(false);
						}}
					/>
					<ErrorButton
						text="Poista kuva"
						onClickFunction={() => {
							setDeleteImage(true);
							setImageFile();
							setImage();
						}}
					/>
					<Stack spacing={2} direction="row">
						<ActionButton
							text="Vahvista"
							onClickFunction={() => {
								(validateSubmit()) 
									? handleSpaceEdit(
										element.id,
										spaceName,
										street,
										postal,
										city,
										size,
										hourly_price,
										description,
										setEditModalVisibility,
										update,
										setUpdate,
										imageFile,
										deleteImage
									) : alert('Tilan muokkaus epäonnistui. Tarkista tiedot');
							}}
						/>
						<CancelButton 
							text="Peruuta"
							onClickFunction={() => {
								setEditModalVisibility(false);
							}}
						/>
					</Stack>
				</Box>
			</ModalCentered>
		</Box>
	);
};

EditSpace.propTypes = {
	element: PropTypes.object,
	update: PropTypes.bool,
	setUpdate: PropTypes.func,
};
export default EditSpace;
