import React, { useState, useContext } from 'react';
import UserContext from '../../contexts/UserContext';
import { Typography } from '@mui/material';
import { Grid } from '@mui/material';
import { Divider } from '@mui/material';
import MySpace from './MySpace';
import { v4 as uuidv4 } from 'uuid';
import PageSelector from '../PageSelector/PageSelector';
import { useNavigate } from 'react-router-dom';
import { validateInt } from '../../utils/validation';

const MySpaces = () => {
	const [displayedSpaces, setDisplayedSpaces] = useState();
	const [update, setUpdate] = useState(false);
	const navigate = useNavigate();

	const { userState } = useContext(UserContext);
	const userId = userState.id;
	if (!validateInt(userId)) {
		navigate('/notfound');
	}

	return (
		<Grid container justifyContent="center" spacing={2}>
			<Grid item md={12}>
				<Divider
					sx={{
						paddingTop: '50px',
						paddingBottom: '50px',
					}}
				>
					<Typography variant="h5">Käyttäjän omat varattavissa olevat tilat</Typography>
				</Divider>
			</Grid>
			<Grid container item md={12} spacing={12}>
				{displayedSpaces ? (
					displayedSpaces.map((element) => (
						<MySpace
							key={uuidv4()}
							element={element}
							displayedSpaces={displayedSpaces}
							setDisplayedSpaces={setDisplayedSpaces}
							update={update}
							setUpdate={setUpdate}
						/>
					))
				) : (
					<Typography variant="p">Ei näytettäviä tiloja.</Typography>
				)}
			</Grid>
			<PageSelector
				limit={10}
				setData={setDisplayedSpaces}
				address={`http://localhost:3001/space/account/${userId}/?`}
				update={update}
				auth={true}
			/>
			<Grid item md={2}></Grid>
		</Grid>
	);
};

export default MySpaces;
