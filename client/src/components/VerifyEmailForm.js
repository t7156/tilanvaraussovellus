import React, { useState } from 'react';
import { Box, Button, TextField, Typography } from '@mui/material';
import ButtonSecondaryLarge from './ui/ButtonSecondaryLarge';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const verifyAccount = async (code) => {
	return await axios.patch(`http://localhost:3001/auth/verify/${code}`);
}; 

const VerifyEmailForm = ({ setReSendEmail }) => {
	const [verificationCode, setVerificationCode] = useState('');
	const navigate = useNavigate();

	const handleSubmit = (e) => {
		e.preventDefault();
		if (verificationCode) {
			verifyAccount(verificationCode)
				.then(() => {
					alert('Sähköposti vahvistettu. Voit nyt kirjautua sisään.');
					navigate('/login');
				})
				.catch((error) => {
					if (error.response.status === 409) {
						alert('Tili on jo vahvistettu.');
						navigate('/login');
					} else if (error.response.status === 500) {
						alert('Tapahtui virhe. Yritä uudelleen.');
					} else {
						alert('Virheellinen koodi. Yritä uudelleen.');
					}
				});
		}
	};

	return (
		<>
			<Typography mb={{ xs: 3, sm: 4, md: 5 }} variant="h4" textAlign="center">
					Vahvista sähköpostiosoite
			</Typography>
			<form onSubmit={handleSubmit}>
				<TextField
					required
					autoComplete="off"
					fullWidth
					id="verification-code"
					label="Kopioi tähän sähköpostilla saamasi koodi"
					type="text"
					value={verificationCode}
					onChange={(e) => setVerificationCode(e.target.value)}
				/>
				<Box pt={{ xs: 1, sm: 2, md: 3 }}>
					<ButtonSecondaryLarge
						type="submit"
						text="Ok"
					/>
				</Box>
			</form>
			<Box pt={{ xs: 1, sm: 2, md: 3 }}>
				<Button
					onClick={() => setReSendEmail(true)}
					fullWidth
					size="large">
                        Lähetä uusi vahvistuskoodi
				</Button>
			</Box>
		</>
        
	);
};

export default VerifyEmailForm;

VerifyEmailForm.propTypes = {
	setReSendEmail: PropTypes.func
};