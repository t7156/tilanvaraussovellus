import banner from '../assets/placeholder/banner2-sm.jpg';
import React from 'react';
import { Container, Paper, Typography } from '@mui/material';

const style = {
	banner: { 
		height: 'auto',
		width: '100%',
		bgcolor: 'primary.main'
	},
	bannerContainer: {
		minWidth: '100%',
		maxWidth: 'false',
		height: 'auto',
		bgcolor: 'primary.main',
		paddingBottom: '40px',
		overFlow: 'hidden'
	},
	bannerTextBox: {
		position: 'absolute',
		top: '150px',
		left: '100px',
		minWidth: '250px',
		minHeight: '150px',
		maxWidth: '350px',
		maxHeight: '200px',
		zIndex: '2000',
		opacity: '0.9',
		padding: '15px'
	}
};

const BannerContainer = () => {
	return (
		<Container
			disableGutters={true}
			sx={style.bannerContainer}>
			<Paper
				sx={style.banner} 
				component='img'
				src={banner} />
			<Paper
				sx={style.bannerTextBox}>
				<Typography 
					variant='button'
					sx={{
						display: 'flex',
						justifyContent: 'center',
						lineHeight: '30px',
						fontSize: '20px',
						textAlign: 'center',
						marginBottom: '20px'
					}}>
				Tilaihmeet
				</Typography>
				<Typography 
					variant='subtitle1'
					sx={{
						display: 'flex',
						justifyContent: 'center',
						width: '100%',
						lineHeight: '30px',
						fontSize: '18px',
						textAlign: 'justify'
					}}>
				Tilaihmeet tilapalvelussa haaveista 
				tulee totta! Voit myös löytää tiloja. 
				</Typography>
			</Paper>
		</Container>
	);
};

export default BannerContainer;
