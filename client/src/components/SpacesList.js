import React from 'react';
import { Container, Typography, Grid, Divider } from '@mui/material';
import { v4 as uuidv4 } from 'uuid';
import Space from './Space';

const SpacesList = ( listedSpaces ) => {

	/* TODO display space owner username & category once 
backend functionality for providing them is completed 
Listed Space into its own component
*/
	const { spaces } = listedSpaces;	

	return (
		<Container
			sx={{
				maxWidth: '90%',
			}}
		>
			<Grid 
				container 
				justifyContent="center"
				spacing={{ xs: 1, md: 2 }}
				columns={{ xs: 2, sm: 8, md: 8, lg: 12}}
			>
				<Grid item md={12}>
					<Divider
						sx={{
							paddingTop: '50px',
							paddingBottom: '50px',
						}}
					>
						<Typography variant="h5">Varattavissa olevat tilat</Typography>
					</Divider>
				</Grid>
				<Grid 
					container 
					justifyContent='center'
				>
					{spaces ? (
						spaces.map((element) => (
							<Grid
								key={uuidv4()}
								sx={{
									marginTop: '20px'
								}}
								item
								container
								md={4}
								spacing={2}
							>
								<Space
									element={element} />
							</Grid>
						))
					) : (
						<Typography variant="p">Ei näytettäviä tiloja.</Typography>
					)}
				</Grid>
			</Grid>
		</Container>
	);
};

export default SpacesList;
