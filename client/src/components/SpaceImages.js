import React from 'react';
import { Box } from '@mui/material';
import PropTypes from 'prop-types';

const SpaceImages = ({ image, width, height }) => {
	return (
		<Box width={width} height={height} m={1}>
			<img src={`data:image/*;base64,${image}`} 
				width="100%" 
				height="100%"
				object-fit="contain" />
		</Box>
	);
};

SpaceImages.propTypes = {
	image: PropTypes.any,
	width: PropTypes.string,
	height: PropTypes.string,
};

export default SpaceImages;
