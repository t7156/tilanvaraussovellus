import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import mainTheme from './themes/MainTheme';
import './index.css';
import App from './App';

ReactDOM.render(
	<React.StrictMode>
		<ThemeProvider theme={mainTheme}>
			<CssBaseline>
				<App />
			</CssBaseline>
		</ThemeProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
