import { Container, Typography } from '@mui/material';
import React from 'react';
import PaperCentered from '../components/ui/PaperCentered';
import LoginForm from '../components/LoginForm';

const Login = () => {
	return (
		<Container maxWidth="lg" fixed height="90vh">
			<PaperCentered>
				<Typography mb={{ xs: 3, sm: 4, md: 5 }} variant="h4" textAlign="center">
					Kirjaudu sisään
				</Typography>
				<LoginForm />
			</PaperCentered>
		</Container>
	);
};

export default Login;
