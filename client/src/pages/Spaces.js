import React, { useState } from 'react';
import { Container } from '@mui/material';
import SpacesList from '../components/SpacesList';
import SearchBar from '../components/SearchBar';
import PageSelector from '../components/PageSelector/PageSelector';
const Spaces = () => {
	const [listedSpaces, setListedSpaces] = useState();
	const [address, setAddress] = useState('http://localhost:3001/space?');
	return (
		<Container
			sx={{
				minWidth: '100%',
				bgcolor: '#F8EDEB',
			}}
		>
			<SearchBar setListedSpaces={setListedSpaces} setAddress={setAddress} />
			<SpacesList spaces={listedSpaces} />
			<PageSelector limit={10} setData={setListedSpaces} address={address} />
		</Container>
	);
};

export default Spaces;
