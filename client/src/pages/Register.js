import React from 'react';
import { Container, Typography } from '@mui/material';
import PaperCentered from '../components/ui/PaperCentered';
import RegisterForm from '../components/RegisterForm';

const Register = () => {
	return (
		<Container maxWidth="lg" fixed height="90vh">
			<PaperCentered>
				<Typography mb={{ xs: 3, sm: 4, md: 5 }} variant="h4" textAlign="center">
					Luo käyttäjätili
				</Typography>
				<RegisterForm />
			</PaperCentered>
		</Container>
	);
};

export default Register;
