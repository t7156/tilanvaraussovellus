import { Container, Box, Typography } from '@mui/material';
import MyReservations from '../components/MyReservations';
import UserInfo from '../components/UserInfo';
import MySpaces from '../components/MySpaces/MySpaces';
import ReservationHistory from '../components/ReservationHistory';
import React, { useContext, useState } from 'react';
import UserContext from '../contexts/UserContext';
import PageSelector from '../components/PageSelector/PageSelector';
import { useNavigate } from 'react-router-dom';
import { validateInt } from '../utils/validation';

const ProfilePage = () => {
	const { userState } = useContext(UserContext);
	const [futureReservations, setFutureReservations] = useState([]);
	const [pastReservations, setPastReservations] = useState([]);
	const navigate = useNavigate();

	if (!validateInt(userState.id)) {
		navigate('/notfound');
	}

	return (
		<Container sx={{ width: '100%', bgcolor: '#F8EDEB', paddingBottom: '100px' }}>
			<Box mx={15}>
				<Typography variant="h3">Tilit</Typography>
				<UserInfo />
				<MyReservations reservations={futureReservations} setReservations={setFutureReservations}/>
				<PageSelector
					limit={10}
					setData={(data) => {
						setFutureReservations(data);
					}}
					address={`http://localhost:3001/reservation/account/${userState.id}/?from=future&`}
				/>
				<MySpaces />
				<ReservationHistory reservations={pastReservations} />
				<PageSelector
					limit={10}
					setData={(data) => {
						setPastReservations(data);
					}}
					address={`http://localhost:3001/reservation/account/${userState.id}/?from=past&`}
				/>
			</Box>
		</Container>
	);
};

export default ProfilePage;
