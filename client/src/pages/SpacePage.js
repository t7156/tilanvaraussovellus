import React, { useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import SpaceDetails from '../components/SpaceDetails';
import SpaceImages from '../components/SpaceImages';
import CalendarContainer from '../components/CalendarContainer';
import Loading from '../components/Loading';
import {
	Box,
	Container,
	Grid,
	Typography,
} from '@mui/material';
import { useLocation, useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import SpaceReviewContainer from '../components/SpaceReview/SpaceReviewContainer';
import { validateInt } from '../utils/validation';

const getSpaceData = async (id) => {
	return await axios.get(`http://localhost:3001/space/${id}`);
};

const SpacePage = () => {
	const [space, setSpace] = useState();
	const [loading, setLoading] = useState(true);
	const location = useLocation();
	const { id } = useParams();
	const navigate = useNavigate();
	const isFirstRender = useRef(true);

	useEffect(() => {
		if (validateInt(id)) {
			if (location.state) {
				const { data } = location.state;
				setSpace(data);
			} else {
				getSpaceData(id)
					.then((response) => {
						setSpace(response.data.data);
					})
					.catch((error) => {
						if (error.response?.status !== 404) {
							alert('Tapahtui virhe. Päivitä sivu ja yritä uudelleen.');
						}
						setLoading(false);
					});
			}
		} else {
			navigate('/notfound');
		}
	}, []);

	useEffect(() => {
		if (isFirstRender.current) {
			isFirstRender.current = false;
			return;
		}
		setLoading(false);
	}, [space]);

	return loading ? (
		<Loading />
	) : (
		!space ? (
			<Container maxWidth="lg">
				<Box
					p={15}> 
					<Typography 
						mb={3}
						variant="h4">
							Hakemaasi tilaa ei löytynyt.
					</Typography>
					<Typography mb={3}>
						Jos sivun päivittäminen ei auta, etsimäsi tila on saatettu poistaa.
						<br/>
					</Typography>
					<Link to='/spaces'>
						Hakusivulle
					</Link>
				</Box>
			</Container>
		) : ( 
			<Container maxWidth="lg">
				<Grid container spacing={1} mt={3}>
					<Grid item xs={12}>
						<Typography variant="h4" m={1}>
							{space.name}
						</Typography>
					</Grid>
					<Grid item xs={12} md={6}>
						<SpaceDetails space={space}/>
					</Grid>
					<Grid item xs={12} md={6} 
						maxHeight='600px' 
						display='flex'
						justifyContent='center'>
						<SpaceImages image={space.image}/>
					</Grid>
					<Grid item xs={12} mt={2}>
						<CalendarContainer 
							space={space}/>
					</Grid>
				</Grid>
				<SpaceReviewContainer space_id={id} />
			</Container>
		)
	);
};

export default SpacePage;
