import React, { useEffect, useState } from 'react';
import {
	Box,
	Container,
	Typography,
	FormControl,
	MenuItem,
	Grid,
	Card,
	Checkbox,
	Tooltip,
	TextField,
	Button,
	Select,
	InputLabel,
} from '@mui/material';
import AddSpaceInput from '../components/ui/AddSpaceInput';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import CategoryIcon from '@mui/icons-material/Category';
import ControlPointIcon from '@mui/icons-material/ControlPoint';
import axios from 'axios';
import { axiosAuth } from '../utils/axiosUtils';
import { v4 as uuidv4 } from 'uuid';
/* TODO flesh out help text.  */

const helpText =
	'Kuvaile tilaa lyhyesti. Kerro mahdollisista rajoituksista, \
ympäristöstä tai muista erityisistä huomioitavista seikoista.';

const AddNewSpace = () => {
	const [categories, setCategories] = useState('');
	const [category, setCategory] = useState('');
	const [spaceCategories, setSpaceCategories] = useState([]);
	const [categoryId, setCategoryId] = useState([]);
	const [name, setName] = useState('');
	const [address, setAddress] = useState({
		street: '',
		postal: '',
		city: '',
	});
	const [size, setSize] = useState('');
	const [description, setDescription] = useState('');
	const [hourly_price, setHourlyPrice] = useState('');
	const [termsRead, setTermsRead] = useState(false);
	const [image, setImage] = useState();

	useEffect(() => {
		const getData = async () => {
			await axios
				.get('http://localhost:3001/category')
				.then((res) => {
					setCategories(res.data.data);
				})
				.catch(() => {
					alert('Ongelma kategorioiden noudossa.');
				});
		};
		getData();
	}, []);

	const handleChange = (event) => {
		setCategory(event.target.value);
	};

	const handleClick = (category) => {
		if (spaceCategories.includes(category.category_name)) {
			setSpaceCategories((spaceCategories) => {
				return spaceCategories?.filter((element) => element !== category.category_name);
			});
			setCategoryId((categoryId) => {
				return categoryId?.filter((element) => element !== category.id);
			});
		} else {
			setSpaceCategories((spaceCategories) => {
				return [...spaceCategories, category.category_name];
			});
			setCategoryId((categoryId) => {
				return [...categoryId, category.id];
			});
		}
	};

	const clearFields = () => {
		setName('');
		setAddress({
			street: '',
			postal: '',
			city: '',
		});
		setSize('');
		setHourlyPrice('');
		setDescription('');
		setCategory('');
		setSpaceCategories([]);
		setTermsRead(false);
		setImage();
	};

	const submitNewSpace = async () => {
		const spaceCategories = categoryId;

		const formData = new FormData();

		formData.append('image', image);
		formData.append('name', name);
		formData.append('address', JSON.stringify(address));
		formData.append('size', size);
		formData.append('description', description);
		formData.append('hourly_price', hourly_price);
		formData.append('spaceCategories', spaceCategories);

		const data = formData;
		const response = await axiosAuth.post('http://localhost:3001/space/', data, {headers: {
			'Content-Type': 'multipart/form-data;'
		}});
		return response;
	};

	const validateImageFormat = (image) => {
		return (!image.name.endsWith('.png') && 
			!image.name.endsWith('.jpg') &&
			!image.name.endsWith('.jpeg') && 
			!image.name.endsWith('.PNG') &&
			!image.name.endsWith('.JPG') && 
			!image.name.endsWith('.JPEG'))
			? false : true;
	};
	
	const validateImageSize = (image) => {
		return image.size > 1024 * 1024 ? false : true;
	};

	const validateSubmit = () => {
		return (
			name.length >= 3 &&
			name.length <= 50 &&
			address.street.length >= 5 &&
			address.street.length <= 50 &&
			address.city.length >= 2 &&
			address.city.length <= 50 &&
			address.postal.length === 5 &&
			size >= 1 &&
			hourly_price >= 1 &&
			spaceCategories.length > 0 &&
			description.length >= 10 &&
			description.length <= 900 &&
			termsRead === true &&
			!isNaN(address.postal) &&
			!isNaN(size) && 
			!isNaN(hourly_price)
		) ? true : false;
	};
	//  TODO: Jos käyttäjä ei lisää kuvaa, lisätäänkö default kuva?
	const postNewSpace = () => {
		if (validateSubmit()) {
			if (validateImageFormat(image)) {
				if (validateImageSize(image)) {
					submitNewSpace()
						.then(() => {
							clearFields();
							alert('Tilan lisäys onnistui.');
						})
						.catch(() => {
							alert('Tilaa ei voitu lisätä.');
						});
				} else {
					alert('Liian suuri tiedosto');
				}
			} else {
				alert('Väärä tiedostomuoto');
			}
		} else {
			alert('Tilan lisäys epäonnistui. Tarkista tiedot.');
		}
	};

	return (
		<Container
			sx={{
				width: '100%',
				height: '1800px',
				paddingTop: '75px',
				paddingBottom: '75px',
			}}
		>
			<Container
				sx={{
					width: '550px',
					marginLeft: '30px',
				}}
			>
				<Typography
					variant="h1"
					sx={{
						fontSize: '42px',
					}}
				>
					Luo uusi tila
				</Typography>
				<Box
					sx={{
						width: '600px',
					}}
				>
					<AddSpaceInput
						label="Tilan nimi"
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
					<AddSpaceInput
						label="Katuosoite"
						value={address.street}
						onChange={(e) => setAddress({ ...address, street: e.target.value })}
					/>
					<AddSpaceInput
						label="Postinumero"
						value={address.postal}
						onChange={(e) => setAddress({ ...address, postal: e.target.value })}
					/>
					<AddSpaceInput
						label="Kaupunki"
						value={address.city}
						onChange={(e) => setAddress({ ...address, city: e.target.value })}
					/>
					<AddSpaceInput
						type="number"
						label="Tilan koko (m2)"
						value={size}
						onChange={(e) => setSize(e.target.value)}
					/>
					<AddSpaceInput
						type="number"
						label="Hinta €/h"
						value={hourly_price}
						onChange={(e) => setHourlyPrice(e.target.value)}
					/>
					<FormControl
						sx={{
							minWidth: '500px',
							maxWidth: '500px',
							marginTop: '20px',
							marginRight: '50px',
							marginBottom: '20px',
						}}
					>
						<Typography
							variant="h3"
							sx={{
								fontSize: '20px',
								marginTop: '20px',
								marginBottom: '20px',
							}}
						>
							Kategoria
						</Typography>
						<Box sx={{ minWidth: 120 }}>
							<FormControl fullWidth>
								<InputLabel>Kategoria</InputLabel>
								<Select value={category} label="Kategoria" onChange={handleChange}>
									{categories &&
										categories.map((category) => (
											<MenuItem
												key={uuidv4()}
												value={category.category_name}
												onClick={() => handleClick(category)}
											>
												<Typography variant="p">
													{category.category_name}
												</Typography>
											</MenuItem>
										))}
								</Select>
								<Grid
									container
									spacing={3}
									sx={{
										marginTop: '10px',
										marginBottom: '20px',
									}}
								>
									{spaceCategories.length > 0 ? (
										spaceCategories.map((category, index) => (
											<Grid item md={4} key={uuidv4()}>
												<Card
													sx={{
														maxWidth: '140px',
														minWidth: '140px',
														paddingTop: '5px',
														paddingBottom: '5px',
														display: 'inline-flex',
														justifyContent: 'flex-end',
													}}
												>
													<CategoryIcon
														sx={{
															fontSize: '18px',
															marginRight: '15px',
															marginTop: '5px',
														}}
													/>
													<Typography
														variant="h5"
														sx={{
															fontSize: '19px',
															marginRight: '15px',
														}}
													>
														{spaceCategories[index]}
													</Typography>
												</Card>
											</Grid>
										))
									) : (
										<Typography
											variant="h5"
											sx={{
												marginTop: '20px',
												marginBottom: '20px',
												marginLeft: '20px',
											}}
										>
											Ei kategorioita valittu.
										</Typography>
									)}
								</Grid>
							</FormControl>
						</Box>
					</FormControl>
					<Typography
						variant="h3"
						sx={{
							display: 'inline-block',
							fontSize: '20px',
							marginTop: '20px',
							marginBottom: '20px',
						}}
					>
						Kuvaus
					</Typography>
					<Tooltip title={helpText}>
						<HelpOutlineIcon
							sx={{
								display: 'inline-block',
								marginLeft: '50px',
								fontSize: '30px',
							}}
						></HelpOutlineIcon>
					</Tooltip>
					<TextField
						multiline
						rows={6}
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						sx={{
							minWidth: '500px',
							maxWidth: '500px',
							marginTop: '20px',
							marginRight: '50px',
							marginBottom: '20px',
						}}
					/>
					<Typography
						variant="h3"
						sx={{
							display: 'inline-block',
							fontSize: '20px',
							marginTop: '20px',
							marginBottom: '20px',
						}}
					>
						Lataa kuvia
					</Typography>
					<input type="file" name="image" accept="image/*" multiple onChange={(event) => setImage(event.target.files[0])}/>
					<ControlPointIcon
						sx={{
							display: 'inline-block',
							marginTop: '30px',
							marginLeft: '50px',
							fontSize: '30px',
						}}
					></ControlPointIcon>
					<Box
						sx={{
							marginLeft: '-15px',
							marginTop: '30px',
							maxWidth: '550px',
							minWidth: '550px',
						}}
					>
						<Checkbox
							value={termsRead}
							checked={termsRead}
							onChange={() => setTermsRead(!termsRead)}
							sx={{
								display: 'inline-block',
							}}
						/>
						<Typography
							variant="h4"
							sx={{
								display: 'inline-block',
								fontSize: '20px',
							}}
						>
							Olen lukenut käyttöehdot.
						</Typography>
					</Box>
					<Button
						variant="contained"
						onClick={postNewSpace}
						sx={{
							marginTop: '40px',
							marginLeft: '-10px',
							minWidth: '150px',
							minHeight: '50px',
						}}
					>
						Lisää uusi tila
					</Button>
				</Box>
			</Container>
		</Container>
	);
};

export default AddNewSpace;
