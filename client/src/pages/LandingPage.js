import React from 'react';
import BannerContainer from '../components/BannerContainer';
import LandingPageInfo from '../components/LandingPageInfo';
import { Container } from '@mui/material';

const LandingPage = () => {
	return (
		<Container
			disableGutters={true}
			sx={{ minWidth: '100%' }}>
			<BannerContainer />
			<LandingPageInfo />
		</Container>
	);
};

export default LandingPage;
