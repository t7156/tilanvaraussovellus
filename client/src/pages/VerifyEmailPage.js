import React, { useState } from 'react';
import { Container } from '@mui/material';
import PaperCentered from '../components/ui/PaperCentered';
import ReSendConfirmationEmail from '../components/ReSendConfirmationEmail';
import VerifyEmailForm from '../components/VerifyEmailForm';

const VerifyEmailPage = () => {
	const [reSendEmail, setReSendEmail] = useState(false);

	return (
		<Container maxWidth="lg" fixed>
			<PaperCentered>
				{ reSendEmail 
					? <ReSendConfirmationEmail
						setReSendEmail={setReSendEmail}
					/>
					: <VerifyEmailForm
						setReSendEmail={setReSendEmail}/>
				}
			</PaperCentered>
		</Container>
	);
};

export default VerifyEmailPage;