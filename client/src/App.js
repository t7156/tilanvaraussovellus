import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import LandingPage from './pages/LandingPage';
import Login from './pages/Login';
import Register from './pages/Register';
import Spaces from './pages/Spaces';
import ProfilePage from './pages/ProfilePage';
import Navbar  from './components/Navbar';
import { UserProvider } from './contexts/UserContext';
import RequireAuth from './contexts/RequireAuth';
import AddNewSpace from './pages/AddNewSpace';
import SpacePage from './pages/SpacePage';
import VerifyEmailPage from './pages/VerifyEmailPage';
import NotFound from './components/NotFound';

function App() {
	return (
		<BrowserRouter>
			<UserProvider>
				<Navbar />
				<Routes>
					<Route 
						exact path="/" 
						element={
							<LandingPage />
						} 
					/>
					<Route 
						exact path="/landingpage" 
						element={
							<LandingPage />
						} 
					/>
					<Route 
						exact path="/login" 
						element={
							<Login />
						} 
					/>
					<Route 
						exact path="/register" 
						element={
							<Register />
						} 
					/>
					<Route 
						exact path="/spaces" 
						element={
							<Spaces />
						} 
					/>
					<Route 
						exact path="/spaces/:id"
						element={
							<SpacePage/>
						}
					/>
					<Route 
						exact path="/addnewspace" 
						element={
							<RequireAuth>
								<AddNewSpace />
							</RequireAuth>
						} 
					/>
					<Route 
						exact path="/profilepage" 
						element={
							<RequireAuth>
								<ProfilePage />
							</RequireAuth>
						} 
					/>
					<Route 
						path="/verify-email"
						element={
							<VerifyEmailPage/>
						}
					/>
					<Route 
						path="*" 
						element={
							<NotFound/>
						} />
				</Routes>
			</UserProvider>
		</BrowserRouter>
	);
}

export default App;
