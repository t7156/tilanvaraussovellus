import { createTheme } from '@mui/material/styles';

const mainTheme = createTheme({
	palette: {
		primary: {
			main: '#323232'
		},
		secondary: {
			main: '#648DE5'
		},
		background: {
			default: '#f8EDEB'
		}, 
		warning: {
			main: '#E65100'
		},
		calendar: {
			main: '#4261BD',
			light: '#20AAE1',
			selection: '#00B7FF',
		}
	},
});

export default mainTheme;